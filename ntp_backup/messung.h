#ifndef MESSUNG_H_
#define MESSUNG_H_

#define COMP PD6
#define COMPPORT DDRD


extern volatile uint16_t timer;
uint8_t state;
// Messergebnis ADC
volatile uint16_t mval;
volatile uint8_t flag;
volatile uint16_t ergebnis;

uint8_t convert_mt(uint16_t mes);
uint16_t messung(uint8_t channel);
void messung_init();

#endif /* MESSUNG_H_ */
