#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <util/delay.h>
#include <avr/wdt.h>		// Watchdog
#include <avr/pgmspace.h>	// PROGMEM
#include <avr/eeprom.h>		// EEPROM
#include "main.h"
#include "lan.h"
#include "ntp.h"
#include "hd44780.h"
#include "ow.h"
#include "messung.h"
#include "shiftregister.h"

// Timer 0
void timer0_init() {
	// init timer, freq = 1 kHz @ CLK = 16 MHz
	TCCR0 = (1<<WGM01)|(1<<CS01)|(1<<CS00);
	OCR0 = 250;
	TIMSK |= (1<<OCIE0);
}

ISR(TIMER0_COMP_vect)
{
	if(++ms_count == 1000)
	{
		++second_count;
		ms_count = 0;
		OW_timer++;
		HK2_timer++;
		HK1_timer++;
		WW_timer++;
		display_timer++;
	}
}



/* Zeitinterrupt
 * 62,5 x/sec
 * 3750 x/min
 * 16ms
 */
void timer2_init(){
	TCCR2 = ((1 << WGM21) | (1 << CS22) | (1 << CS21) | (1 << CS20));	// CTC mode, prescaler 1024
	OCR2 = 250;				// CTC TOP
	TIMSK = (1 << OCIE2);	// CTC Interrupt aktivieren
}

ISR(TIMER2_COMP_vect)
{
	if(++cycles >= 3750)
	{
		minuten++;
		cycles = 0;
	}
	timer++;
}








void udp_packet(eth_frame_t *frame, uint16_t len)			// udp paketereignis
{
	ip_packet_t *ip = (void*)(frame->data);
	udp_packet_t *udp = (void*)(ip->data);
	uint8_t *data = udp->data;
	uint32_t timestamp;										// zeitvariable

	if(udp->to_port == NTP_LOCAL_PORT)						// eingehende NTP nachricht
	{
		if((timestamp = ntp_parse_reply(udp->data, len)))	// extrahiere zeit, extraktion erfolgreich
		{
			time_offset = timestamp - second_count;			// rechne wirkliche zeit aus
			ntp_next_update = second_count + 12UL*60*60;	// setze n�chste aktualisierung
		}
	}
	else
	{
		uint8_t length = 0;
		// steuerkomando (bulk read)
		if (data[0] == 0x55) {
			// Heizkreis 1
			data[1] = Kessel_ist;
			data[2] = Brenner_status;
			data[3] = HK1_soll;
			data[4] = HK1_ist;
			data[5] = HK1_active;
			data[6] = HK1_diff;
			data[7] = HK1_wait;
			data[8] = HK1_state;
			// Heizkreis 2
			data[9] = HK2_soll;
			data[10] = HK2_ist;
			data[11] = HK2_active;
			data[12] = HK2_diff;
			data[13] = HK2_wait;
			data[14] = HK2_state;
			data[15] = HK2_present;
			// Warmwasser
			data[16] = WW_soll;
			data[17] = WW_ist;
			data[18] = WW_active;
			data[19] = WW_diff;
			data[20] = WW_wait;
			data[21] = WW_state;
			// OneWire Sensoren
			data[22] = Holzkessel;
			data[23] = Speicher0;
			data[24] = Speicher1;
			data[25] = Speicher2;
			data[26] = Speicher3;
			data[27] = Speicher4;
			data[28] = experimentell;
			data[29] = energy_source;
			data[30] = 0xAA;
			length = 31;
			//sende Antwort
			udp_reply(frame, length);
		}
		else{
			// steuerkomando
			//schreiben
			if ((data[0] == 0xAA)&&(data[1] == 0xAA)) {
				hd44780_puts(" schreiben");
				switch (data[2]) {
				// Brenner
				//Heizkreis 1
				case 0x11:
					HK1_soll = data[3];
					eeprom_write_byte((uint8_t *)EEP_HK1_SOLL, HK1_soll);
					break;
				case 0x13:
					HK1_active = data[3];
					eeprom_write_byte((uint8_t *)EEP_HK1_ACTIVE, HK1_active);
					break;
				case 0x14:
					HK1_diff = data[3];
					eeprom_write_byte((uint8_t *)EEP_HK1_DIFF, HK1_diff);
					break;
				case 0x15:
					HK1_wait = data[3];
					eeprom_write_byte((uint8_t *)EEP_HK1_WAIT, HK1_wait);
					break;

				// Heizkreis 2
				case 0x21:
					HK2_soll = data[3];
					eeprom_write_byte((uint8_t *)EEP_HK2_SOLL, HK2_soll);
					break;
				case 0x23:
					HK2_active = data[3];
					eeprom_write_byte((uint8_t *)EEP_HK2_ACTIVE, HK2_active);
					break;
				case 0x24:
					HK2_diff = data[3];
					eeprom_write_byte((uint8_t *)EEP_HK2_DIFF, HK2_diff);
					break;
				case 0x25:
					HK2_wait = data[3];
					eeprom_write_byte((uint8_t *)EEP_HK2_WAIT, HK2_wait);
					break;
				// Warmwasser
				case 0x31:
					WW_soll = data[3];
					eeprom_write_byte((uint8_t *)EEP_WW_SOLL, WW_soll);
					break;
				case 0x33:
					WW_active = data[3];
					eeprom_write_byte((uint8_t *)EEP_WW_ACTIVE, WW_active);
					break;
				case 0x34:
					WW_diff = data[3];
					eeprom_write_byte((uint8_t *)EEP_WW_DIFF, WW_diff);
					break;
				case 0x35:
					WW_wait = data[3];
					eeprom_write_byte((uint8_t *)EEP_WW_WAIT, WW_wait);
					break;
				default: data[3] = 0x00;
				}
			}
			length = 4;
			//sende Antwort
			udp_reply(frame, length);
		}
	}
}


void printT(uint16_t t) {
	char s[10];
	uint16_t nk = 0;
	utoa((t >> 4), s, 10);
	hd44780_puts(s);
	hd44780_puts(".");
	if (t & (1 << 3)) {
		nk = 50;
	}
	if (t & (1 << 2)) {
		nk += 25;
	}
	if (t & (1 << 1)) {
		nk += 12;
	}
	if (t & (1 << 0)) {
		nk += 6;
	}
	if (nk < 10) {
		hd44780_puts("0");
	}
	utoa(nk, s, 10);
	hd44780_puts(s);
}




int main()
{
	// init Watchdog
	wdt_enable(WDTO_2S);

	// init Ports
	DDRA = 0xFF;
	PORTA = 0;
	DDRB = 0xFF;
	PORTB = 0;
	DDRC = 0xFF;
	PORTC = 0;
	DDRD = 1;
	PORTD = 0xFF;

	wdt_reset();

	// init LCD
	hd44780_init();			// initialisierung 4Bit Modus
	hd44780_mode(1,0,0);	// disolay on/off, cursor on/off, cursor blinking on/off
	hd44780_clear();		// display l�schen

	wdt_reset();

	// init Schieberegister
	shift_init();
	hd44780_clear();

	wdt_reset();

	// init Parameter
	HK1_soll = eeprom_read_byte((uint8_t *)EEP_HK1_SOLL);
	HK1_active = eeprom_read_byte((uint8_t *)EEP_HK1_ACTIVE);
	HK1_diff = eeprom_read_byte((uint8_t *)EEP_HK1_DIFF);
	HK1_wait = eeprom_read_byte((uint8_t *)EEP_HK1_WAIT);

	wdt_reset();

	WW_soll = eeprom_read_byte((uint8_t *)EEP_WW_SOLL);
	WW_active = eeprom_read_byte((uint8_t *)EEP_WW_ACTIVE);
	WW_diff = eeprom_read_byte((uint8_t *)EEP_WW_DIFF);
	WW_wait = eeprom_read_byte((uint8_t *)EEP_WW_WAIT);

	wdt_reset();

	HK2_soll = eeprom_read_byte((uint8_t *)EEP_HK2_SOLL);
	HK2_diff = eeprom_read_byte((uint8_t *)EEP_HK2_DIFF);
	ROTATION_TIME = eeprom_read_byte((uint8_t *)EEP_ROTATION_TIME);
	HK2_active = eeprom_read_byte((uint8_t *)EEP_HK2_ACTIVE);
	HK2_wait = eeprom_read_byte((uint8_t *)EEP_HK2_WAIT);

	energy_source = eeprom_read_byte((uint8_t *)EEP_ENERGY_SOURCE);

	wdt_reset();

	// Z�hler f�r Uhr
	timer = 0;
	timer2_init();

	sei();

	// init ICP f�r ADC Messung
	messung_init();

	// init OneWire
	uint8_t ow_state = 0;
	ow_init();

	wdt_reset();


	// Zeitgeber ms slots
	timer0_init();

	uint32_t display_next_update = 0;
	uint32_t loctime;
	uint8_t s = 0, m = 0, h = 0;
	char buf[5];

	// Init LAN
	lan_init();

	wdt_reset();
	lan_poll();

	wdt_reset();

	sei();

	char test[10];

	//shift |= (1 << BRENNER);
	//shift_set(shift);

	//Mischer Energiequelle
//	DDRA |= 0b00000110;
//	PORTA &= 0b11111001;
//	PORTA |= 0b00000010;

	wdt_reset();

	hd44780_clear();		// display l�schen
	hd44780_puts("init done");
	_delay_ms(1000);
	wdt_reset();
	_delay_ms(1000);

	while(1)
	{
		wdt_reset();

		lan_poll();

		// Time to send NTP request?
		if(second_count >= ntp_next_update)
		{
			if(!ntp_request(NTP_SERVER)) {
				ntp_next_update = second_count+2;
			}
			else{
				ntp_next_update = second_count+60;
			}
		}

		// Time to refresh display?
		if((time_offset) && (second_count >= display_next_update))
		{
			display_next_update = second_count+1;

			loctime = time_offset+second_count + 60UL*60*TIMEZONE;
			s = loctime % 60;
			m = (loctime/60)%60;
			h = (loctime/3600)%24;
		}

		if (display_timer) {
			// Zeit ausgeben
			hd44780_clear();
			if (h < 10) {
				hd44780_puts("0");
			}
			itoa(h,buf,10);
			hd44780_puts(buf);
			hd44780_puts(":");
			if (m < 10) {
				hd44780_puts("0");
			}
			itoa(m,buf,10);
			hd44780_puts(buf);
			hd44780_puts(":");
			if (s < 10) {
				hd44780_puts("0");
			}



			itoa(s,buf,10);
			hd44780_puts(buf);

			// Temperaturanzeige
			hd44780_puts(" in:");
			utoa(HK1_ist, test, 10);
			hd44780_puts(test);

			// zweite Zeile
			hd44780_cmd(LCD_SET_CURSOR(41));

			hd44780_puts("K:");
			ultoa(Kessel_ist, test, 10);
			hd44780_puts(test);

			hd44780_puts(" W:");
			ultoa(WW_ist, test, 10);
			hd44780_puts(test);

			hd44780_puts(" FBH:");
			ultoa(HK2_ist, test, 10);
			hd44780_puts(test);
			display_timer = 0;
		}


//-------------------------------------------Sensoren-update-----------------------------------
	// 0 Kessel
	// 1 Au�enf�hler
	// 2 Brauchwasser
	// 3 HK2 anwesend
	// 4 ???
	// 5 Brenner status
	// 6 Vorlauf
	// 7 ???
	switch (messtate) {
	// Kessel
	case 0:
		// messe Kessel
		zz = messung(0);
		if (zz == 0xFFF0) {
			Kessel_ist = 0;
			//Kessel_messung_error = TRUE;
			messtate = 2;
		}else {
			if (!(zz == 0xFFFF)){
				if (energy_source == DIESEL) {
					//Kessel_messung_error = FALSE;
					Kessel_ist = convert_mt(zz);
				} else {
					Kessel_ist = Speicher1;
				}
				messtate = 2;
			}
		}
		break;
	case 2:
		zz = messung(2);
		if (zz == 0xFFF0) {
			WW_ist = 255;
			messtate = 3;
		}else {
			if (!(zz == 0xFFFF)){
				WW_ist = convert_mt(zz);
				messtate = 3;
			}
		}
		break;
	case 3:
		zz = messung(3);
		if (zz == 0xFFF0) {
			HK2_present = 0;
			messtate = 6;
		}else {
			if (!(zz == 0xFFFF)){
				if (zz < 400) {
					HK2_present = 0xFF;
				} else {
					HK2_present = 0;
				}
				messtate = 6;
			}
		}
		break;
	case 6:
		zz = messung(6);
		if (zz == 0xFFF0) {
			HK2_ist = 255;
			messtate = 0;
		}else {
			if (!(zz == 0xFFFF)){
				HK2_ist = convert_mt(zz);
				messtate = 0;
			}
		}
		break;
	default: messtate = 0;
	}

	switch (ow_state) {
	case 0:
		// starte Messung
		ow_start();
		ow_state = 1;
		OW_timer = 0;
		break;
	case 1:
		// Warte bis Messung abgeschkossen ist
		if (OW_timer >=2){
			ow_state = 2;
		}
		break;
	case 2:
		// lese Sensor aus
		Holzkessel = (ow_temp_id(EEP_OW_HOLZ) >> 4);
		ow_state = 3;
		break;
	case 3:
		Speicher0 = (ow_temp_id(EEP_OW_SPEICHER_0) >> 4);
		ow_state = 4;
		break;
	case 4:
		Speicher1 = (ow_temp_id(EEP_OW_SPEICHER_1) >> 4);
		if (energy_source == HOLZ) {
			Kessel_ist = Speicher1;
		}
		ow_state = 5;
		break;
	case 5:
		Speicher2 = (ow_temp_id(EEP_OW_SPEICHER_2) >> 4);
		ow_state = 6;
		break;
	case 6:
		Speicher3 = (ow_temp_id(EEP_OW_SPEICHER_3) >> 4);
		ow_state = 7;
		break;
	case 7:
		Speicher4 = (ow_temp_id(EEP_OW_SPEICHER_4) >> 4);
		ow_state = 8;
		break;
	case 8:
		experimentell = (ow_temp_id(EEP_WO_EXPERIMENT) >> 4);
		HK1_ist = experimentell;
		ow_state = 0;
		break;
	default: ow_state = 0;
	}
//---------------------------------------------------------------------------------------------



//		if (h > 6 && h <= 23){
//			HK1_soll = 21;
//			WW_active = 0xFF;
//		}else{
//			HK1_soll = 19;
//			WW_active = 0;
//		}
//
//	/*	switch (Brenner_state) {
//			case 0:
//					//
//			case 1:
//					//
//			default:
//		}
//	*/

//-----------------state machine Heizkreis 1-----------------------------
		switch (HK1_state) {
		case 0:
			if (HK1_active) {
				HK1_state = 1;
				break;
			}
			break;

		/*
		 *
		 */
		case 1:
			// Heizkreis 1 deaktiviert
			if (!HK1_active){
				HK1_state = 0;
				shift &= ~(1 << HK1);
				shift_set(shift);
				break;
			}

			if (HK1_ist > (HK1_soll + HK1_diff)) {
				shift &= ~(1 << HK1);	// zu warm, Pumpe aus
				shift_set(shift);
			}
			if (HK1_ist < (HK1_soll - HK1_diff)) {
				shift |= (1 << HK1);	// zu kalt, Pumpe an
				shift_set(shift);
			}
			HK1_timer = 0;
			HK1_state = 2;	// warten
		break;

		// Warten
		case 2:
			// Heizkreis 1 deaktiviert
			if (!HK1_active){
				HK1_state = 0;
				shift &= ~(1 << HK1);
				shift_set(shift);
				break;
			}
			// timeout
			if (HK1_timer > HK1_wait) {
				HK1_state = 1;
				break;
			}
		break;
		default: HK1_state = 0;
		}

	//-----------------state machine Warmwasser------------------------------
		switch (WW_state) {
		/*
		 * Startzustand
		 */
		case 0:	//
			if (WW_active) {
				WW_state = 1;
				break;
			}
			break;
		/*
		 * Heizzustand
		 */
		case 1:
			// wenn Warmwasser deaktiviert, gehe in den Startzustand
			if (!WW_active) {
				WW_state = 0;
				shift &= ~(1 << WW);	// Warmwasser Ladepumpe aus
				shift_set(shift);
				break;
			}

			// Warmwasser warm genug, gehe in den Wartezustand
			if (WW_ist >= WW_soll) {
				WW_state = 2;
				shift &= ~(1 << WW);	// Warmwasser Ladepumpe aus
				shift_set(shift);
				break;
			}

			// Kessel Messung durchf�hren
			// WW_ist mit Kesseltemperatur vergleichen und gegebenenfalls in Fehlerzustand oder einen gesonderten wechseln

			if ((Kessel_ist - WW_diff) <= WW_ist) {
				shift &= ~(1 << WW);	// Warmwasser Ladepumpe aus
				shift_set(shift);
				WW_timer = 0;
				WW_state = 3;
				break;
			}
			if (Kessel_ist >= (WW_ist + WW_diff)) {
				shift |= (1 << WW);		// Warmwasser Ladepumpe an
				shift_set(shift);
				WW_timer = 0;
				WW_state = 3;
				break;
			}
			break;
		/*
		 * Wartezustand
		 */
		case 2:
			// Warmwasser inaktiv
			if (!WW_active) {
				WW_state = 0;
				shift &= ~(1 << WW);	// Warmwasser Ladepumpe aus
				shift_set(shift);
				break;
			}

			// Warmwasser zu kalt, heizen
			if (WW_ist < (WW_soll - WW_diff)) {
				WW_state = 1;
				shift |= (1 << WW);	// Warmwasser Ladepumpe an
				shift_set(shift);
				break;
			}
			break;

		/*
		 * Fehlerzustand
		 * noch keine Aktionen definiert
		 * nicht ausreichende Kesseltemperatur als Fehler
		 */
		case 3:
				if (WW_timer >= WW_wait) {
					WW_state = 1;
				}
				break;
		default:	WW_state = 3;
		}

//-----------------state machine Heizkreis 2------------------------------
		switch (HK2_state) {
		/*
		 * Startzustand
		 */
		case 0:
			if (HK2_active) {
				shift |= (1 << HK2);	// Bodenheizung Pumpe an
				shift_set(shift);
				HK2_state = 1;
				HK2_timer = 0;
				break;
			}
			break;

			/*
			 * Wartezustand
			 * hier wird gewartet zwischen den Korrekturvorg�ngen und der Zustand gepr�fft
			 */
		case 1:
			// HK2 deaktiviert
			if (!HK2_active) {
				shift &= ~(1 << HK2);	// Bodenheizung Pumpe aus
				shift_set(shift);
				HK2_state = 0;
				break;
			}

			// FM241 nicht vorhanden
			if (!HK2_present) {
				HK2_state = 4;
				break;
			}

			// timeout erreicht, gehe zur aktuellen Messung
			if (HK2_timer >= HK2_wait){
				HK2_state = 2;
				break;
			}
			break;
		/*
		 * Messung auswerten
		 */
		case 2:
			// falls Temperatur nicht ausreichend Pumpe an lassen, aber Mischer eventuell abschalten

			// Vorlauf OK
			if ((HK2_ist >= (HK2_soll - HK2_diff)) && (HK2_ist <= (HK2_soll + HK2_diff))) {
				HK2_state = 1;
				HK2_timer = 0;
				break;
			}

			// Vorlauf kalt
			if (HK2_ist < (HK2_soll - HK2_diff)) {
				HK2_rotate = (HK2_soll - HK2_ist) * ROTATION_TIME;
				HK2_direction = 0xFF;
				HK2_state = 3;
				HK2_timer = 0;
				break;
			}
			// Vorlauf warm
			if (HK2_ist > (HK2_soll + HK2_diff)) {
				HK2_rotate = (HK2_ist - HK2_soll) * ROTATION_TIME;
				HK2_direction = 0;
				HK2_state = 3;
				HK2_timer = 0;
				break;
			}
			break;

		/*
		 * Drehung wird durchgef�hrt
		 */
		case 3:
			if (HK2_timer >= HK2_rotate) {
				HK2_state = 1;
				HK2_timer = 0;
				shift &= ~((1 << MW) | (1 << MK));
				shift_set(shift);
				break;
			}

			if (HK2_direction) {
				shift |= (1 << MW);
			} else {
				shift |= (1 << MK);
			}
			shift_set(shift);
			break;

		/*
		 * Fehlerzustand
		 */
		case 4:
			if (!HK2_active) {
				HK2_state = 0;
				shift &= ~(1 << HK2);	// Bodenheizung Pumpe aus
				shift_set(shift);
				break;
			}
			//shift &= ~(1 << HK2);	// Bodenheizung Pumpe aus
			//shift_set(shift);
			if (HK2_present) {
				HK2_state = 1;
				HK2_timer = 0;
				break;
			}
			break;
		default:	HK2_state = 4;
		}
	}
	return 0;
}

