#ifndef MAIN_H_
#define MAIN_H_

#define TRUE 0xFF
#define FALSE 0x00

// Schieberegister Pins
#define BRENNER 0
#define HK1 1
#define WW 2
#define PZ 3
#define MW 4
#define MK 5
#define HK2 6

// EEPROM OneWire ROM Adressen
#define EEP_OW_HOLZ			0	//{0x28, 0xFF, 0x8B, 0xAA, 0x03, 0x00, 0x00, 0xA5}
#define EEP_OW_SPEICHER_0	8	//{0x28, 0xFA, 0x70, 0xAA, 0x03, 0x00, 0x00, 0x98}
#define EEP_OW_SPEICHER_1	16	//{0x28, 0x68, 0xAC, 0xAA, 0x03, 0x00, 0x00, 0x38}
#define EEP_OW_SPEICHER_2	24	//{0x28, 0x19, 0x98, 0xAA, 0x03, 0x00, 0x00, 0x0C}
#define EEP_OW_SPEICHER_3	32	//{0x28, 0xF2, 0xAC, 0xAA, 0x03, 0x00, 0x00, 0x46}
#define EEP_OW_SPEICHER_4	40	//{0x28, 0x86, 0x6D, 0xAA, 0x03, 0x00, 0x00, 0xED}
#define EEP_WO_EXPERIMENT	48	//{0x28, 0x94, 0x80, 0xAA, 0x03, 0x00, 0x00, 0xEE}

// Speicherpl�tze f�r Parameter im EEPROM
#define EEP_HK1_SOLL	128
#define EEP_HK1_ACTIVE	129
#define EEP_HK1_DIFF	130
#define EEP_HK1_WAIT	131
#define EEP_WW_SOLL		132
#define EEP_WW_ACTIVE	133
#define EEP_WW_DIFF		134
#define EEP_WW_WAIT		135
#define EEP_HK2_SOLL	136
#define EEP_HK2_DIFF	137
#define EEP_ROTATION_TIME 138
#define EEP_HK2_ACTIVE	139
#define EEP_HK2_WAIT	140
#define EEP_ENERGY_SOURCE 141

//eeprom_write_byte((uint8_t *)128, 0xFF);

//OneWire Sensoren
uint8_t Holzkessel = 0;
uint8_t Speicher0 = 0;
uint8_t Speicher1 = 0;
uint8_t Speicher2 = 0;
uint8_t Speicher3 = 0;
uint8_t Speicher4 = 0;
uint8_t experimentell = 0;


#define DIESEL	0x00
#define HOLZ	0xFF

uint8_t energy_source = 0;

// Brenner arbeitsvariablen
	uint8_t Kessel_ist = 0;
	uint8_t Brenner_status = 0;

// Heizkreis 1 Arbeitsvariablen
	uint8_t HK1_soll = 0;		// read/write
	uint8_t HK1_ist = 0;		// read
	uint8_t HK1_active = 0x00;	// read/write
	uint8_t HK1_diff = 0;		// read:write
	uint8_t HK1_wait = 0;		// read/write
	uint8_t HK1_state = 0;		// read

// Brauchwasser Arbeitsvariablen
	uint8_t WW_soll = 0;		// read/write, Solltemperatur
	uint8_t WW_ist = 0;			// read only, aktuelle Temperatur (nur bei aktiviertem Warmwasser auslesen ansonsten besser gesondert)
	uint8_t WW_state = 0;		// read only, aktueller Zustand
	uint8_t WW_active = 0x00;	// read/write, Aktivierungsflag
	uint8_t WW_diff = 0;		// read/write, offset f�r erneute Aufw�rmung
	uint8_t WW_wait = 0;		// read/write

// Bodenheizung Arbeitsvariablen
	uint8_t HK2_state = 0;		// read only, aktueller Zustand
	uint8_t HK2_soll = 0;		// read/write, Solltemperatur
	uint8_t HK2_ist = 0;		// read only, aktuelle Temperatur (nur der letzte gemessene Wert, nur bei aktivem Heizkreis verwenden. [state 1, 2 oder 3])
	uint8_t HK2_diff = 0;		// read/write, Regelgenauigkeit in Grad
	uint8_t HK2_rotate = 0;
	uint8_t ROTATION_TIME = 0;
	uint8_t HK2_direction = 0;
	uint8_t HK2_active = 0x00;	// read/write, Aktivierungsflag
	uint8_t HK2_wait = 0;		// read/write, Wartezeit zwischen den Messungen
	uint8_t HK2_present = 0;	// read only


// Variable f�r Messung
uint16_t zz = 0;

volatile uint8_t temperatur;
volatile uint8_t messtate = 0;

extern volatile uint16_t shift;

volatile uint8_t debug_timer = 0;
// Variablen f�r Systemzeit
volatile uint8_t minuten = 05;

volatile uint8_t HK1_timer = 0;

// Timer zur Heizkreis 2 Steuerung
volatile uint8_t HK2_timer = 0;
volatile uint8_t WW_timer = 0;
volatile uint8_t OW_timer = 0;
volatile uint8_t display_timer = 0;

uint16_t cycles;
volatile uint16_t timer;

#define NTP_SERVER	inet_addr(62,117,76,142)
#define TIMEZONE	2

static volatile uint16_t ms_count;				// Milisekunden Z�hler f�r ISR
static volatile uint32_t second_count;
static volatile uint32_t ntp_next_update;
static volatile uint32_t time_offset;

#endif /* MAIN_H_ */
