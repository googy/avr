#ifndef OW_H_
#define OW_H_

#define OW_OUT		PORTA
#define OW_DIR		DDRA
#define OW_PIN		PINA
#define OW			PA0

void ow_init();
void ow_reset();
void ow_write(uint8_t data);
uint64_t ow_read(uint8_t len);
void ow_start();
uint16_t ow_temp_id(uint8_t id);

#endif
