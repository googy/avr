#include <avr/io.h>
#include "shiftregister.h"

void shift_init() {
	DDRD |= (1 << STROBE) | (1 << DATA) | (1 << CLOCK);
	PORTD &= ~((1 << STROBE) | (1 << DATA) | (1 << CLOCK));
}

void shift_set(uint16_t v){
	PORTD &= ~((1 << DATA) | (1 << CLOCK) | (1 << STROBE));
	for (uint8_t i = 0; i < 16; i++){
		PORTD &= ~((1 << DATA) | (1 << CLOCK));		// aufr�umen
		if (1 & (v >> (15 - i))) {
			PORTD |= (1 << DATA);					// setze 1
		}
		PORTD |= (1 << CLOCK);						// einshiften
	}
	PORTD &= ~((1 << DATA) | (1 << CLOCK));			// aufr�umen

	PORTD |= (1 << STROBE);
	PORTD &= ~(1 << STROBE);
}
