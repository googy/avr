
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>

#include <util/delay.h>
#include <stdlib.h>

#include "uart.h"
#include "mcp2515.h"
#include "global.h"
#include "defaults.h"


void print_can_message(tCAN *message) {
	char buf[10];

	uart_puts("RTR: ");
	itoa(message->header.rtr, buf, 10);
	uart_puts("    ID: ");
	itoa(message->id, buf, 16);
	uart_puts(buf);
	uart_puts("    DLC: ");
	itoa(message->header.length, buf, 10);
	uart_puts(buf);
	
	if (!message->header.rtr) {
		uart_puts("    DATA: ");
		for (uint8_t i = 0; i < message->header.length; i++) {
			uart_puts("0x");
			itoa(message->data[i], buf, 16);
			uart_puts(buf);
			uart_putc(' ');
		}
		uart_putc(10);
		uart_putc(13);
	}
}

int main(void) {
	uart_init();
	sei();		// Aktiviere Interrupts
	
	// Versuche den MCP2515 zu initilaisieren
	if (!mcp2515_init()) {
		uart_puts("Fehler: kann den MCP2515 nicht ansprechen!\n\r");
		for (;;);
	}
	else {
		uart_puts("MCP2515 is aktiv\n\n\r");
	}
	
	uart_puts("Erzeuge Nachricht\n\r");
	tCAN message;
	
	// einige Testwerte
	message.id = 0x123;
	message.header.rtr = 0;
	message.header.length = 2;
	message.data[0] = 0xab;
	message.data[1] = 0xcd;
	
	print_can_message(&message);
	
	uart_puts("\nwechsle zum Loopback-Modus\n\n\r");
	mcp2515_bit_modify(CANCTRL, (1<<REQOP2)|(1<<REQOP1)|(1<<REQOP0), (1<<REQOP1));
	
	// Sende eine Nachricht
	if (mcp2515_send_message(&message)) {
		uart_puts("Nachricht wurde in die Puffer geschrieben\n\r");
	}
	else {
		uart_puts("Fehler: konnte die Nachricht nicht senden\n\r");
	}
	
	// warte ein bisschen
	_delay_ms(10);
	
	if (mcp2515_check_message()) {
		uart_puts("Nachricht empfangen!\n\r");
		
		// read the message from the buffers
		if (mcp2515_get_message(&message)) {
			print_can_message(&message);
			uart_puts("\n\r");
		}
		else {
			uart_puts("Fehler: konnte die Nachricht nicht auslesen (loop)\n\n\r");
		}
	}
	else {
		uart_puts("Fehler: keine Nachricht empfangen (loop)\n\n\r");
	}
	
	uart_puts("zurueck zum normalen Modus\n\n\r");
	mcp2515_bit_modify(CANCTRL, (1<<REQOP2)|(1<<REQOP1)|(1<<REQOP0), 0);
	
	// wir sind ab hier wieder mit dem CAN Bus verbunden
	
	uart_puts("Versuche die Nachricht per CAN zu verschicken\n\r");

	// Versuche nochmal die Nachricht zu verschicken, diesmal per CAN
	if (mcp2515_send_message(&message)) {
		uart_puts("Nachricht wurde in die Puffer geschrieben\n\n\r");
	}
	else {
		uart_puts("Fehler: konnte die Nachricht nicht senden\n\n\r");
	}
	
	uart_puts("Warte auf den Empfang von Nachrichten\n\n\r");
	
	while (1) {
		// warten bis wir eine Nachricht empfangen
		if (mcp2515_check_message()) {
			uart_puts("Nachricht empfangen!\n\r");
			
			// Lese die Nachricht aus dem Puffern des MCP2515
			if (mcp2515_get_message(&message)) {
				print_can_message(&message);
				uart_puts("\n\r");
			}
			else {
				uart_puts("Kann die Nachricht nicht auslesen\n\n\r");
			}
		}
	}
	return 0;
}
