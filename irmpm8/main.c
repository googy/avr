/*---------------------------------------------------------------------------------------------------------------------------------------------------
 * main.c - demo main module to test irmp decoder
 *
 * Copyright (c) 2009-2013 Frank Meyer - frank(at)fli4l.de
 *
 * $Id: main.c,v 1.17 2013/01/17 07:33:14 fm Exp $
 *
 * This demo module is runnable on AVRs and LM4F120 Launchpad (ARM Cortex M4)
 *
 * ATMEGA88 @ 8 MHz internal RC      Osc with BODLEVEL 4.3V: lfuse: 0xE2 hfuse: 0xDC efuse: 0xF9
 * ATMEGA88 @ 8 MHz external Crystal Osc with BODLEVEL 4.3V: lfuse: 0xFF hfuse: 0xDC efuse: 0xF9
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 */

#define F_CPU 16000000UL

#include <avr/io.h>
#include "irmp.h"
#include <stdlib.h>
#include <util/delay.h>



#define BAUD 9600UL      // Baudrate
// Berechnungen
#define UBRR_VAL ((F_CPU+BAUD*8)/(BAUD*16)-1)   // clever runden
#define BAUD_REAL (F_CPU/(16*(UBRR_VAL+1)))     // Reale Baudrate
#define BAUD_ERROR ((BAUD_REAL*1000)/BAUD) // Fehler in Promille, 1000 = kein Fehler.
#if ((BAUD_ERROR<990) || (BAUD_ERROR>1010))
#error Systematischer Fehler der Baudrate gr�sser 1% und damit zu hoch!
#endif

void uart_init() {
	UBRRH = UBRR_VAL >> 8;
	UBRRL = UBRR_VAL & 0xFF;

	UCSRB |= (1 << TXEN);  // UART TX einschalten
	UCSRC = (1 << URSEL) | (1 << UCSZ1) | (1 << UCSZ0);  // Asynchron 8N1
}

void uart_putc(unsigned char c) {
	while (!(UCSRA & (1 << UDRE))) {
	} /* warten bis Senden moeglich */
	UDR = c; /* sende Zeichen */
}

void uart_puts(char *s) {
	while (*s) { /* so lange *s != '\0' also ungleich dem "String-Endezeichen(Terminator)" */
		uart_putc(*s);
		s++;
	}
}

void timer2_init() {	// IRMP Timer
	TCCR2 = (1 << WGM21) | (0 << CS22) | (1 << CS21) | (0 << CS20);	// Prescaler 8; CTC mode
	OCR2 = (F_CPU / 8 / F_INTERRUPTS) - 1;// compare value: 1/15000 of Timer frequency
	TIMSK |= 1 << OCIE2;					// OCIE2: Interrupt by timer compare
}

ISR(TIMER2_COMP_vect)// Timer2 output compare interrupt service routine, called every 1/15000 sec
{
	(void) irmp_ISR();                                          // call irmp ISR
}

void timer1_init() {
	DDRB |= (1 << PB1); // Port OC1A als Ausgang
	DDRB |= (1 << PB2); // Port OC1B als Ausgang
	TCCR1A = (1 << WGM11) | (1 << WGM10) | (1 << COM1A1) | (1 << COM1B1); // PWM, phase correct, 10 bit.
	TCCR1B = (0 << CS12) | (0 << CS11) | (1 << CS10); // Prescaler 64 = Enable counter
	//OCR1A = 1; // Duty cycle 50% (Anm. ob 128 oder 127 bitte pr�fen)
}

void adc_init() {
	ADMUX = (0 << REFS1) | (1 << REFS0) | (0 << ADLAR) | (0 << MUX3)
			| (0 << MUX2) | (0 << MUX1) | (1 << MUX0);
	ADCSRA = (1 << ADEN) | (1 << ADSC) | (0 << ADFR) | (0 << ADIF) | (0 << ADIE)
			| (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0);
}

void timer0_init() {
	TCCR0 = (1 << CS02) | (0 << CS01) | (1 << CS00);// prescaler 1024, 8MHz -> 32,768ms
	TIMSK |= (1 << TOIE0);
}
volatile uint8_t c = 0;
ISR (TIMER0_OVF_vect) {
	if (!c) {
		//PORTD ^= (1 << PD5);
		//PORTD ^= (1 << PD6);
	}
	c++;
}

const uint16_t pwmtable[18] = { 0, 1, 2, 3, 4, 6, 8, 11, 16, 23, 32, 45, 64, 90,
		127, 255, 511, 1023 };

int main(void) {

	DDRD |= (1 << PD5) | (1 << PD6) | (1 << PD7);
	PORTD &= ~((1 << PD5) | (1 << PD6) | (1 << PD7));

	timer0_init();
	timer1_init();

	uart_init();
	char buf[10];

	IRMP_DATA irmp_data;
	irmp_init();	// initialize irmp
	timer2_init();	// initialize irmp timer2

	sei ();
	// enable interrupts

	uint16_t h = 0;

	for (;;) {

		//
		if (irmp_get_data(&irmp_data)) {
			if (irmp_data.protocol == 2) {	// NEC Protocol
				if (irmp_data.address == 0xef00 && irmp_data.flags == 0) {	// Gainward
					switch (irmp_data.command) {
						case 0xd:
							uart_puts("heller\n\r");
							if (h < 17) {
								h++;
							}
							break;
						case 0x15:
							uart_puts("dunkler\n\r");
							if (h > 0) {
								h--;
							}
							break;
					}
					OCR1A = pwmtable[h];
					OCR1B = pwmtable[h];
				}
			}

			uart_puts(utoa(irmp_data.protocol, buf, 10));
			uart_putc(' ');
			uart_puts(utoa(irmp_data.address, buf, 16));
			uart_putc(' ');
			uart_puts(utoa(irmp_data.command, buf, 16));
			uart_putc(' ');
			uart_puts(utoa(irmp_data.flags, buf, 2));
			uart_putc(' ');
			uart_putc(10);
			uart_putc(13);

			// ir signal decoded, do something here...
			// irmp_data.protocol is the protocol, see irmp.h
			// irmp_data.address is the address/manufacturer code of ir sender
			// irmp_data.command is the command code
			// irmp_protocol_names[irmp_data.protocol] is the protocol name (if enabled, see irmpconfig.h)
		}
	}
}
