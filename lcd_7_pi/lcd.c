#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
#include "lcd.h"
#include "gpio.h"

void LCD_Write_Bus(unsigned short data) {
	// clear data mask
	GPIO_CLR = (1<<DB0)|(1<<DB1)|(1<<DB2)|(1<<DB3)|(1<<DB4)|(1<<DB5)|(1<<DB6)|(1<<DB7)|(1<<DB8)|
			(1<<DB9)|(1<<DB10)|(1<<DB11)|(1<<DB12)|(1<<DB13)|(1<<DB14)|(1<<DB15);
	// set data
	GPIO_SET = ((data & 0b111) << 2) |
		((data & 0b11111000) << 4) |
		((data & 0b1100000000) << 6) |
		((data & 0b110000000000) << 7) |
		((data & 0b1111000000000000) << 10);

	// wait 10ns
	//usleep(1);
	// clear WR
	GPIO_CLR = (1 << WR);
	// wait 20ns
	//usleep(1);
	// set WR
	GPIO_SET = (1 << WR);
	// wait 10ns
	//usleep(1);
}

void LCD_WR_REG(unsigned short data) {
	GPIO_CLR = (1 << RS);		// clear RS
	LCD_Write_Bus(data);		// write data
}

void LCD_WR_DATA(unsigned short data) {
	GPIO_SET = (1 << RS);		// set RS
	LCD_Write_Bus(data);		// write data
}

void address_set(unsigned short x1, unsigned short y1, unsigned short x2, unsigned short y2) {
	LCD_WR_REG(0x02);	// begin y
	LCD_WR_DATA(y1);
	LCD_WR_REG(0x03);	// begin x
	LCD_WR_DATA(x1);
	LCD_WR_REG(0x06);	// stop y
	LCD_WR_DATA(y2);
	LCD_WR_REG(0x07);	// stop x
	LCD_WR_DATA(x2);
	LCD_WR_REG(0x0f);	// set write addres to data channel
}

void LCD_init() {
	// setup GPIOs
	INP_GPIO(DB0);OUT_GPIO(DB0);
	INP_GPIO(DB1);OUT_GPIO(DB1);
	INP_GPIO(DB2);OUT_GPIO(DB2);
	INP_GPIO(DB3);OUT_GPIO(DB3);
	INP_GPIO(DB4);OUT_GPIO(DB4);
	INP_GPIO(DB5);OUT_GPIO(DB5);
	INP_GPIO(DB6);OUT_GPIO(DB6);
	INP_GPIO(DB7);OUT_GPIO(DB7);
	INP_GPIO(DB8);OUT_GPIO(DB8);
	INP_GPIO(DB9);OUT_GPIO(DB9);
	INP_GPIO(DB10);OUT_GPIO(DB10);
	INP_GPIO(DB11);OUT_GPIO(DB11);
	INP_GPIO(DB12);OUT_GPIO(DB12);
	INP_GPIO(DB13);OUT_GPIO(DB13);
	INP_GPIO(DB14);OUT_GPIO(DB14);
	INP_GPIO(DB15);OUT_GPIO(DB15);
	INP_GPIO(REST);OUT_GPIO(REST);
	INP_GPIO(CS);OUT_GPIO(CS);
	INP_GPIO(RD);OUT_GPIO(RD);
	INP_GPIO(WR);OUT_GPIO(WR);
	INP_GPIO(RS);OUT_GPIO(RS);

	GPIO_SET = (1 << RD) | (1 << WR);
	GPIO_CLR = (1 << REST);
	usleep(10000);
	GPIO_SET = (1 << REST);
	usleep(10000);
	GPIO_CLR = (1 << CS);

	// Hintergrundbeleuchtung einschalten
	LCD_WR_REG(0x01);
	LCD_WR_DATA(1);
}

void LCD_Clear(unsigned short  color) {
	unsigned short i,j;
	address_set(0, 0, 799, 479);
	for (i = 0; i < 800; i++) {
		for (j = 0; j < 480; j++) {
			LCD_WR_DATA(color);
		}
	}
}

void WritePage(unsigned char index) {
	LCD_WR_REG(0x05);
	LCD_WR_DATA(index);
}

void ShowPage(unsigned char index) {
	LCD_WR_REG(0x04);
	LCD_WR_DATA(index);
}
