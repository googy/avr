#ifndef LCD_H_
#define LCD_H_

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
#include "gpio.h"

// I/O access
extern volatile unsigned *gpio;

#define DB0	2
#define DB1	3
#define DB2	4
#define DB3	7
#define DB4	8
#define DB5	9
#define DB6	10
#define DB7	11
#define DB8	14
#define DB9	15
#define DB10	17
#define DB11	18
#define DB12	22
#define DB13	23
#define DB14	24
#define DB15	25
#define REST	27
#define CS	28
#define RD	29
#define WR	30
#define RS	31

#define BLACK	0b0000000000000000
#define RED	0b1111100000000000
#define GREEN	0b0000011111100000
#define BLUE	0b0000000000011111
#define YELLOW	0b1111111111100000
#define PINK	0b1111100000011111
#define WHITE	0b1111111111111111

void LCD_Write_Bus(unsigned short data);
void LCD_WR_REG(unsigned short data);
void LCD_WR_DATA(unsigned short data);
void address_set(unsigned short x1, unsigned short y1, unsigned short x2, unsigned short y2);
void LCD_init();
void LCD_Clear(unsigned short  color);
void WritePage(unsigned char index);
void ShowPage(unsigned char index);

#endif /* LCD_H_ */
