#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
#include "lcd.h"

int main(int argc, char **argv) {
	unsigned int i;
	setup_io();
	LCD_init();
	address_set(0,0,799,479);
	for (i = 0; i < (800*480); i++) {
		LCD_WR_DATA(BLACK);
	}
	address_set(0,0,799,479);
	for (i = 0; i < 10000; i++) {
		LCD_WR_DATA(RED);
		usleep(1000);
		LCD_WR_DATA(0xFFFE);
		usleep(1000);
	}
	return 0;
}
