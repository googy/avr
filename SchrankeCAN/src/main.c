/*
 * main.c
 *
 *  Created on: 29.04.2018
 *      Author: googy
 */
#include <avr/io.h>
#include <stdlib.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdio.h>
#include <inttypes.h>

#include "uart.h"
#include "timebase.h"
#include "i2cmaster.h"
#include "ssd1306.h"
#include "mcp2515.h"
#include "button.h"


tCAN message;   // eingehende Nachrichten werden hier abgelegt

char buf[10];       // Zwischenpuffer zur Umwandlung von Zahlen in Strings


void can_puts(char *s) {
    tCAN m;
    m.id = 0x20;
    m.header.rtr = 0;
    uint8_t i = 0;
    while (*s) { /* so lange *s != '\0' also ungleich dem "String-Endezeichen(Terminator)" */
        m.data[i] = *s;
        i++;
        if (i == 8) {
            m.header.length = 8;
            // send message
            if (mcp2515_send_message(&m)) {
                //uart_puts("Nachricht konnte gesendet werden\n\r");
            } else {
                //uart_puts("ERROR Nachricht konnte nicht gesendet werden\n\r");
            }
            i = 0;
        }
        s++;
    }
    if (i > 0) {
        m.header.length = i;
        // send message
        if (mcp2515_send_message(&m)) {
        //uart_puts("Nachricht konnte gesendet werden\n\r");
        } else {
        //uart_puts("ERROR Nachricht konnte nicht gesendet werden\n\r");
        }
    }
}


// PD2     // Taster Schranke auf
// PD3     // Taster Schranke zu
// PD4     // Taster CAN ID
// PD5     // PWM Timer0   OCR0B   links vom Motor gesehen
// PD6     // PWM Timer0   OCR0A   rechts vom Motor gesehen
// PD7     // Lampe
#define LED     PB5     // LED am Arduino

// Verkabelung:
// DB9 Pin2, rot    -> CAN low
// DB9 Pin7, lila   -> CAN high
// DB9 Pin3, orange -> CAN GND

uint8_t active_sender = 2;
// auf dem Bus senden mehrere Sender
// Anweisungen werden nur von einem der Sender ausgeführt.
// acrive_sender speichert die ID, welche angenommen wird

volatile uint32_t testtimer = 0;

int main() {
    sei();  // für timebase und buttons

    // UART
    uart_init();
    uart_puts("UART initialized\n\r");

    // timebase
    // benutzt Timer2 von den Buttons

    // GPIO
    DDRD |= (1 << PD5) | (1 << PD6) | (1 << PD7);       // Motor links | Motor rechts | Lampe
    DDRD &= ~((1 << PD2) | (1 << PD3) | (1 << PD4));    // Taster auf, Taster zu, Taster CAN ID     // FIXME, nicht nötig, wird in init_buttons() gemacht
    PORTD |= ((1 << PD2) | (1 << PD3) | (1 << PD4));    // pullups für alle Taster, active low      // FIXME, nicht nötig, wird in init_buttons() gemacht
    DDRB |= (1 << PB5); // LED

    // Buttons
    init_buttons();

    // I2C OLED
    init_ssd1306();
    clear();
    setCursor(0, 0);
    oled_print("OLED initialized");

    // MCP2515 CAN
     DDRB |= 1 << PB2;		// !!!!!!!!!!!!!!!!!!!!! SS to output, sonst keine SPI Kommunikation, wird hier auch als CS verwendet
    // Versuche den MCP2515 zu initilaisieren
    if (!mcp2515_init()) {
        clear();
        uart_puts("Kommunikation zum CAN Controller fehlgeschlagen\n\r");
        return 0;
    } else {
        uart_puts("MCP2515 OK\n\r");
        setCursor(1,0);
        oled_print("CAN initialized");
    }






    stoptimer_init(&testtimer);


    // setup PWM on Timer0
    TCCR0A = (1 << COM0A1) | (1 << COM0B1) | (1 << WGM01) | (1 << WGM00);   // mode 3, FastPWM
    TCCR0B = (1 << CS02); // prescaler 256
    OCR0A = 0;
    OCR0B = 0;

    uint8_t motor_auf = 0;
    uint8_t motor_zu = 0;
    while (1) {

        // Taster zur Auswahl des aktiven Senders
        if (b_event(3)) {
            active_sender = (active_sender + 1) % 10;
        }

        if (b_event(1)) {
            uart_puts("Schranke zu\n\r");
        }
        if (b_event(2)) {
            uart_puts("Schranke auf\n\r");
        }

        if (stoptimer_expired(&testtimer, 500)) {
            stoptimer_init(&testtimer);
        }

        setCursor(2, 0);
        oled_print(itoa(active_sender, buf, 10));


        // sende Anschlagtaster
        message.id = 0x3;
        message.header.rtr = 0;
        message.header.length = 4;
        message.data[0] = ((~PIND >> 2) & 0b11);  // auf 1, zu 2, kein debouncing
        message.data[1] = 0;
        message.data[2] = 0;
        message.data[3] = 0;
        // send message
        if (mcp2515_send_message(&message)) {
            ;
        } else {
            uart_puts("ERROR Nachricht konnte nicht gesendet werden\n\r");
        }

//////////////////////////////////////////////////////receive_CAM_message/////////////////////////
        // check for incoming message            if (message.id == )s, immer Filter prüfen damit Empfang überhaupt möglich wird
        if (mcp2515_check_message()) {                  // Nachricht vorhanden
            if (mcp2515_get_message(&message)) {		// Nachricht erfolgreich angekommen
                if (message.id == active_sender && message.header.length == 4) {

                    // Lampe
                    if (message.data[0] & (1 << 2)) {
                        PORTD |= (1 << PD7);
                    } else {
                        PORTD &= ~(1 << PD7);
                    }

                    // Fehlerzustand, beide Motoren gleichzeitig eingeschaltet
                    if ((message.data[0] & 0b11) == 0b11) {
                        clear();
                        setCursor(3, 0);
                        oled_print("beide Richtungen!!!");
                        //PORTD &= ~((1 << PD5) | (1 << PD6));
                        OCR0A = 0;
                        OCR0B = 0;
                        motor_auf = 0;
                        motor_zu = 0;
                    } else {
                        if (message.data[0] & (1 << 0)) {
                            //PORTD |= (1 << PD5);    // open
                            OCR0A = 150;
                            motor_auf = 1;
                        } else {
                            //PORTD &= ~(1 << PD5);   // stop open
                            OCR0A = 0;
                            motor_auf = 0;
                        }
                        if (message.data[0] & (1 << 1)) {
                            //PORTD |= (1 << PD6);    //close
                            OCR0B = 150;
                            motor_zu = 1;
                        } else {
                            //PORTD &= ~(1 << PD6);   //stop close
                            OCR0B = 0;
                            motor_zu = 0;
                        }
                    }
                }
            } else {
                uart_puts("FAIL\n\r");
            }
        } // if (mcp2515_check_message())

        if (motor_auf && motor_zu) {
            // error
        } else if (motor_auf) {
            if (PIND & (1 << PD2)) {    // active low
                // oeffne
            } else {
                // stop oeffne
            }
        }

    }
    return 0;
}
