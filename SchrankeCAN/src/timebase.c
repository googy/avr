/*
 * timebase.c
 *
 *  Created on: 16.09.2018
 *      Author: googy
 */
#include <avr/io.h>
#include <avr/interrupt.h>
#include "timebase.h"

volatile uint32_t ticks = 0;

// Timer 1 als Zeitbasis, Frequenz???
//void timer0_init() {
//	TCCR0A = (1 << WGM01);				// kein PWM, CTC mode, bis OCR0A dann reset
//	TCCR0B = (1 << CS02);				// kein PWM, prescaler 256, 31,250kHz bei 8MHz
//	OCR0A = 125;						// bis OCR0A zählen, dann reset
//	TIMSK0 |= (1 << OCIE0A);			// Output Compare A Match Interrupt
//}

// 250Hz Timer Interrupt (ungenau, nicht Uhr-fähig)
//ISR(TIMER0_COMPA_vect) {
//	ticks++;
//}

// speichere aktuelle Zeit in Ticks im Counter
void stoptimer_init(volatile uint32_t* counter) {
    cli();
    *counter = ticks;
    sei();
}

// wenn die aktuelle Zeit den gespeicherten Startwert + offset übersteigt return true
// timer2 wird als Zeitquelle mitbenutzt, aktuell 244Hz, dies ergibt ein Fehler, da hier ein 250Hz Takt angenommen wird
uint8_t stoptimer_expired(volatile uint32_t* counter, uint16_t ms_time) {
    uint8_t ergebnis = 0;
    cli();
    if (ticks > (*counter + (ms_time >> 2))) ergebnis = 1;
    sei();
    return ergebnis;
}
//-----------------------------------------------------------------------------------------------------
