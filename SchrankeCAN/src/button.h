/*
 * button.h
 *
 *  Created on: 10.07.2019
 *      Author: schleica
 */

#ifndef SRC_BUTTON_H_
#define SRC_BUTTON_H_

void init_buttons();
uint8_t b_event(uint8_t b_num);

#endif /* SRC_BUTTON_H_ */
