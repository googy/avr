/*
 * uart.c
 *
 *  Created on: 03.07.2013
 *      Author: googy
 */
#include <avr/io.h>
#include "uart.h"

void uart_init() {	// USART init
	UBRR0H = UBRR_VAL >> 8;
	UBRR0L = UBRR_VAL & 0xFF;

	UCSR0B |= (1 << TXEN0) | (1 << RXEN0); // UART TX einschalten
	UCSR0C = (1 << UCSZ01) | (1 << UCSZ00); // Asynchron 8N1

    // Flush Receive-Buffer (entfernen evtl. vorhandener ungültiger Werte)
//    do
//    {
//        UDR;
//    }
//    while (UCSRA & (1 << RXC));
}

void uart_putc(unsigned char c) {
	while (!(UCSR0A & (1 << UDRE0))) {
	} /* warten bis Senden moeglich */
	UDR0 = c; /* sende Zeichen */
}

void uart_puts(char *s) {
	while (*s) { /* so lange *s != '\0' also ungleich dem "String-Endezeichen(Terminator)" */
		uart_putc(*s);
		s++;
	}
}

uint8_t uart_receive(void) {
	while ( !(UCSR0A & (1 << RXC0)) ) {};
	return UDR0;
}

void uart_flush(void) {
	uint8_t dummy = 0;
	while ( UCSR0A & (1 << RXC0) ) {
		dummy = UDR0;
	}
}
