/*
 * button.c
 *
 *  Created on: 10.07.2019
 *      Author: schleica
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include "button.h"

//-------------mögliche Zustände der Tasten------------
enum buttonstate {
    RELEASED = 1,
    RISING = 2,
    PRESSED = 3,
//    FALLING = 4
};

// Variablen, in denen die Zustände der Tasten verwaltet werden
uint8_t b1 = 0;
enum buttonstate b1_s = RELEASED;
uint8_t b2 = 0;
enum buttonstate b2_s = RELEASED;
uint8_t b3 = 0;
enum buttonstate b3_s = RELEASED;

// es werden nur low-high Events erkannt
void button(enum buttonstate* b_state, uint8_t* b_count, uint8_t b_pin) {
    uint8_t pins = PIND;                                                // !!!!!! PORT an dem die Tasten angeschlossen sind
    switch (*b_state) {
    case RELEASED:
        // auf Grundlage der Frequenz mit der diese Funktion aufgerufen wird, wird gezählt wie oft eine Taste als gedrückt erkannt worden ist.
        // überschreitet dieser Wert eine voreingestellte Schwelle, wird dies als ein Ereignis erkannt
        // diese Schwelle sollte auf basis der Ausführungsfrequenz von button(...) und dem konkreten Einsatzszenario festgelegt werden.
        if (!(pins & (1 << b_pin))) {   // taste gedrückt
            (*b_count)++;
            if (*b_count > 15) *b_state = RISING;    // Schwelle zum Erkennen einer Betätigung überschritten
        } else {
            if (*b_count) (*b_count)--; // taste nicht gedrückt
        }
        break;
    case RISING:
        // hier geht es nicht weiter, damit das RISING event extern abgeholt werden kann
        break;
    case PRESSED:
        // ähnlich wie im RELEASED State, jedoch wird das losslassen erkannt
        if (!(pins & (1 << b_pin))) {
            if (*b_count < 15) (*b_count)++;
            // Taste immernoch gedrückt, inkrementiere bis Schwelle. Damit wir schnell innerhalb der 7 Zyklen das loslassen erkennen.
            // Wenn höher inkrementiert werden würde, würde eine Verzögerung entstehen bis das Loslassen erkannt worden ist.
        } else {
            if (*b_count) (*b_count)--;     // falls 0 noch nicht erreicht, dekrementiere
            else *b_state = RELEASED;       // 0 wurde erreicht, die Taste als nicht gedrückt erkannt
        }
        break;
    }
}

// damit werden events abgeholt
uint8_t b_event(uint8_t b_num) {
    switch (b_num) {
    case 1:
        if (b1_s == RISING) {
            b1_s = PRESSED;
            return 1;
        }
        break;
    case 2:
        if (b2_s == RISING) {
            b2_s = PRESSED;
            return 1;
        }
        break;
    case 3:
        if (b3_s == RISING) {
            b3_s = PRESSED;
            return 1;
        }
        break;
    }
    return 0;
}

//-----------------------------timebase----------------------------------------------------------------
void init_button_timer() {
    // normal mode, overflow bei TOP
//    TCCR2A = 0;     // normal mode, no output compare output
//    TCCR2B = (1 << CS22) | (1 << CS21) | (1 << CS20);   // 1024 prescaler
//    TIMSK2 = 1 << TOIE2;                                // enable Timer2 Overflow Interrupt

    // normal mode
    TCCR2A = 0;
    TCCR2B = (1 << CS22) | (1 << CS20);
    TIMSK2 = 1 << TOIE2;
}

extern volatile uint32_t ticks; // stammt aus timebase.c
// 8000000Hz / 128 / 256 = 244Hz 244,140625
ISR(TIMER2_OVF_vect) {
    // von der timebase mitbenutzt
    cli();
    ticks++;
    sei();

    // Tasten
    button(&b1_s, &b1, PD2);
    button(&b2_s, &b2, PD3);
    button(&b3_s, &b3, PD4);
}

//-----------------------------------------------------------------------------------------------------
void init_buttons() {
    // initialisiere buttons GPIO
    DDRD &= ~(1 << PD2);    // button1
    PORTD |= (1 << PD2);    // pullup, active low

    DDRD &= ~(1 << PD3);    // button2
    PORTD |= (1 << PD3);

    DDRD &= ~(1 << PD4);    // button3
    PORTD |= (1 << PD4);

    init_button_timer();
}
