/*
 * main.c
 *
 *  Created on: 23.01.2015
 *      Author: googy_000
 */
#include <avr/io.h>
#include <util/delay.h>
#include "uart.h"
#include <stdlib.h>
#include "MAX31855.h"

int main() {
	max31855initSW();
	//max31855initHW();
	uart_init();
	char buf[40];

	while (1) {
		uart_putc(10);
		uart_putc(12);

		readTempSW();
		//readTempHW();

		uart_puts("Intern: ");
		itoa(max31855.intern, buf, 10);
		uart_puts(buf);
		uart_putc(10);
		uart_putc(13);

		uart_puts("Extern: ");
		itoa(max31855.sensor, buf, 10);
		uart_puts(buf);
		uart_putc(10);
		uart_putc(13);

		_delay_ms(1000);
	}
	return 1;
}
