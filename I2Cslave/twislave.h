#ifndef _TWISLAVE_H
#define _TWISLAVE_H

//Bei zu alten AVR-GCC-Versionen werden die Interrupts anders genutzt, in diesem Fall Fehlermeldung
#if (__GNUC__ * 100 + __GNUC_MINOR__) < 304
	#error "This library requires AVR-GCC 3.4.5 or later, update to newer AVR-GCC compiler !"
#endif

#define buffer_size	20

volatile uint8_t twibuffer[buffer_size];				// "Register"
volatile uint8_t twibuffer_ptr; 						// buffer pointer

void init_twi_slave(uint8_t adr);

#endif //#ifdef _TWISLAVE_H
