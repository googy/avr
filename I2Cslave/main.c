#include <avr/io.h>
#include <avr/interrupt.h>

#include <avr/pgmspace.h>
#include <stdlib.h>

#include "uart.h"
#include "twislave.h"

#define SLAVE_ADRESSE 0x50 					// Slave-Adresse, wird vom Raspberry als 0x28 angezeigt

int main(void) {
	uart_init();
	cli();
	init_twi_slave(SLAVE_ADRESSE);			//TWI als Slave mit Adresse slaveadr starten 
	sei();

	twibuffer[0] = 0xAA;
	twibuffer[1] = 0xBB;
	twibuffer[2] = 0xCC;
	twibuffer[3] = 0xDD;
	twibuffer[4] = 0xEE;
	twibuffer[5] = 0xFF;

	while(1) {
		;
	}
}
