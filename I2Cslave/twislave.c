/* getestet Arduino pro mini, ATmega328p, 8MHz, 3,3V zu Raspberry Pi 1B
 * es waren keine pull-ups nötig
 * >>> i2cdetect -y 1
 * zeigt 0x28 als Adresse obwohl 0x50 eingestellt
 * >>> sudo i2cset -y 1 0x28 0xYY 0xZZ
 * 0x28 -> Adresse
 * 0xYY -> Adresse des zu schreibenden Registers
 * 0xZZ -> Wert, welcher in das Register 0xYY geschrieben wird
 * >>> sudo i2cget -y 1 0x28 0xYY
 * 0xYY Adresse, die gelesen werden soll
 * >>> sudo i2cget -y 1 0x28
 * gibt den Wert am pointer zurück beginnend mit 0 und inkrementiert diesen
 */

#include <util/twi.h>
#include <avr/interrupt.h>
#include <stdint.h>
#include "twislave.h"

//ACK nach empfangenen Daten senden/ ACK nach gesendeten Daten erwarten
#define TWCR_ACK 	TWCR = (1<<TWEN)|(1<<TWIE)|(1<<TWINT)|(1<<TWEA)|(0<<TWSTA)|(0<<TWSTO)|(0<<TWWC);  

//NACK nach empfangenen Daten senden/ NACK nach gesendeten Daten erwarten     
#define TWCR_NACK 	TWCR = (1<<TWEN)|(1<<TWIE)|(1<<TWINT)|(0<<TWEA)|(0<<TWSTA)|(0<<TWSTO)|(0<<TWWC);

//switched to the non adressed slave mode...
#define TWCR_RESET 	TWCR = (1<<TWEN)|(1<<TWIE)|(1<<TWINT)|(1<<TWEA)|(0<<TWSTA)|(0<<TWSTO)|(0<<TWWC);  

void init_twi_slave(uint8_t adr) {
	TWAR = adr; //Adresse setzen
	TWCR &= ~(1 << TWSTA) | (1 << TWSTO);
	TWCR |= (1 << TWEA) | (1 << TWEN) | (1 << TWIE);
	twibuffer_ptr = 0xFF;		// buffer pointer mit ungültigem Wert initialisieren, um korrekte Adressen zu erkennen
	sei();
}

//ISR, die bei einem Ereignis auf dem Bus ausgelöst wird.
// Im Register TWSR befindet sich dann ein Statuscode, anhand dessen die Situation festgestellt werden kann.
ISR (TWI_vect) {
	uint8_t data = 0;

	switch (TW_STATUS) {							// TWI-Statusregister
	case TW_SR_SLA_ACK:								// 0x60 Slave Receiver, schreibender Zugriff
		TWCR_ACK;									// FIXME nächstes Datenbyte empfangen, ACK danach
		twibuffer_ptr = 0xFF;							// Übertragung wurde begonnen, initialisiere buffer pointer
		break;
	case TW_SR_DATA_ACK:							// 0x80 Slave Receiver, Daten empfangen
		data = TWDR;								// empfangenen Byte auslesen
		if (twibuffer_ptr == 0xFF) {					// erster Zugriff, zu schreibende Adresse noch nicht empfangen
			if (data <= buffer_size) {				// Kontrolle ob zu schreibende Adresse im erlaubten Bereich
				twibuffer_ptr = data;					// buffer pointer setzen, das nächste Byte wird an diese Stelle geschrieben
			} else {
				twibuffer_ptr = 0;						// Adresse auf Null setzen. Ist das sinnvoll?
				// TODO Adresse außerhalb des gültigen Bereichs, starte Fehlerbehandlung
			}
			TWCR_ACK;								// nächstes Datenbyte empfangen, ACK danach, um nächstes Byte anzufordern
		} else {									// zu schreibende Adresse bereits empfangen, aktueller Byte soll in den Puffer geschrieben werden
			twibuffer[twibuffer_ptr] = data; 			// Daten in Buffer schreiben
			twibuffer_ptr++;							// Buffer-Adresse weiterzählen für nächsten Schreibzugriff
			if (twibuffer_ptr < (buffer_size - 1)) {	// im Buffer ist noch Platz für mehr als ein Byte
				TWCR_ACK;							// nächstes Datenbyte empfangen, ACK danach, um nächstes Byte anzufordern
			} else {								// es kann nur noch ein Byte kommen, dann ist der Buffer voll
				TWCR_NACK;							// letztes Byte lesen, dann NACK, um vollen Buffer zu signaliseren
			}
		}
		break;
	case TW_ST_SLA_ACK:								//
	case TW_ST_DATA_ACK:							// 0xB8 Slave Transmitter, weitere Daten wurden angefordert
		if (twibuffer_ptr == 0xFF) {					// zuvor keine Leseadresse angegeben!
			twibuffer_ptr = 0;
		}
		TWDR = twibuffer[twibuffer_ptr];				// Datenbyte senden
		twibuffer_ptr++;								// bufferadresse für nächstes Byte weiterzählen
		if (twibuffer_ptr < (buffer_size - 1)) {		// im Buffer ist mehr als ein Byte, das gesendet werden kann
			TWCR_ACK;								// nächstes Byte senden, danach ACK erwarten
		} else {
			TWCR_NACK;								// letztes Byte senden, danach NACK erwarten
		}
		break;

	case TW_ST_DATA_NACK:							// 0xC0 Keine Daten mehr gefordert
	case TW_SR_DATA_NACK:							// 0x88
	case TW_ST_LAST_DATA:							// 0xC8  Last data byte in TWDR has been transmitted (TWEA = �0�); ACK has been received
	case TW_SR_STOP:								// 0xA0 STOP empfangen
	default:
		TWCR_RESET;									// Übertragung beenden, warten bis zur nächsten Adressierung
		break;
	}
}
