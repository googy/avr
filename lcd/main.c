#include <avr/io.h>
#include <util/delay.h>
#include <stdlib.h>
#include <avr/interrupt.h>
#include "hd44780.h"
#include "ow.h"

#define STROBE PD3
#define DATA PD4
#define CLOCK PD5

#define BRENNER 0
#define HK1 1
#define WW 2
#define PZ 3
#define MW 4
#define MK 5
#define HK2 6
#define S0 8
#define S1 9
#define S2 10
#define T5 11
#define COMP PD6

uint16_t shift = 0;

// Messergebnis ADC
volatile uint16_t mval = 1;

volatile uint8_t debug_timer = 0;
// Variablen f�r Systemzeit
volatile uint8_t stunden = 23;
volatile uint8_t minuten = 05;
volatile uint8_t sekunden = 0;

volatile uint8_t HK1_timer = 0;
// Timer zur Heizkreis 2 Steuerung
volatile uint8_t HK2_timer = 0;
volatile uint8_t OW_timer = 0;

void shift_init() {
	DDRD |= (1 << STROBE) | (1 << DATA) | (1 << CLOCK);
	PORTD &= ~((1 << STROBE) | (1 << DATA) | (1 << CLOCK));
}

void shift_set(uint16_t v){
	PORTD &= ~((1 << DATA) | (1 << CLOCK) | (1 << STROBE));
	for (uint8_t i = 0; i < 16; i++){
		PORTD &= ~((1 << DATA) | (1 << CLOCK));		// aufr�umen
		if (1 & (v >> (15 - i))) {
			PORTD |= (1 << DATA);					// setze 1
		}
		PORTD |= (1 << CLOCK);						// einshiften
	}
	PORTD &= ~((1 << DATA) | (1 << CLOCK));			// aufr�umen

	PORTD |= (1 << STROBE);
	PORTD &= ~(1 << STROBE);
}

// Timer f�r ADC Messung
void timer0_init_int_128us(){
	TCCR0 = 0;
	TCCR0 = (1 << CS01);	// prescaler 8 >> 0,5us
	TCNT0 = 0;				// Timer zur�cksetzen
	TIMSK |= (1 << TOIE0);	// Overflow Interrupt enable	128us Overflow
}

// ADC Messung
void meassure(){
	shift |= (1 << T5);
	shift_set(shift);		// Kondensator Entladetransistor T5 einschalten
	mval = 0;				// Z�hler initialisieren
	_delay_ms(100);			// warten bis Kondensator weitgehend entladen ist
	shift &= ~(1 << T5);
	timer0_init_int_128us();
	shift_set(shift);		// Kondensator laden
	_delay_ms(100);			// warte auf Messung, feste Wartezeit, damit bei einer endlosen Messung kein deadlock
	TCCR0 = 0;				// Timer anhalten
	TIMSK &= ~(TOIE0);		// overflow Interupt deaktivieren
}

// ADC Messergebnis in Temperaturwert umwandeln
uint8_t convert_mt(uint16_t mes){
	uint32_t h = 6950000/mes;		// Vergleichswert Messung
	uint32_t j = 1;					// Vergleichswert Berechnung
	uint8_t temper = 0;
	while (h > j){					// N�herung des Logarithmus
		j *= 103;
		if (j > 1000000){
			j /= 100;
		}
		temper++;
	}
	return temper;
}

// 1 Sekunde Timer f�r Uhr und state machine
void timer1_init_1s(){
	TCCR1A = 0;
	TCCR1B = (1 << CS12) | (0 << CS11) | (1 << CS10) | (1 << WGM12);	// prescaler 1024, CTC mode
	OCR1A = 15624;
	TIMSK |= (1 << OCIE1A);								// channel A output compare interrupt
}

// �berpr�fe ob FM241 vorhanden ist
uint8_t check_FM241(){
	shift |= (1 << S0) | (1 << S1);
	shift &= ~(1 << S2);
	shift_set(shift);
	meassure();
	if (mval > 300) {
		return 0;
	} else {
		return 0xFF;
	}
}

int main() {
//	uint8_t id1[8] = { 0x28, 0x94, 0x80, 0xAA, 0x03, 0x00, 0x00, 0xEE }; //ROM ID 1 experimentell
//	uint8_t id2[8] = { 0x28, 0xFF, 0x8B, 0xAA, 0x03, 0x00, 0x00, 0xA5 }; //ROM ID 2 Wohnzimmer

	ow_init();
	timer1_init_1s();

	shift_init();
	// ADC eingang
	DDRD &= ~(1 << COMP);

	// Init LCD
	_delay_ms(30);
	hd44780_init();
	hd44780_mode(1,1,0);
	hd44780_clear();

	sei();

	// Brenner arbeitsvariablen
	uint8_t Kessel_ist = 0;

	// Heizkreis 1 Arbeitsvariablen
	uint8_t HK1_soll = 0;	// wird nicht hier gesetzt
	uint8_t HK1_ist = 0;
	uint8_t HK1_active = 1;
	uint8_t HK1_diff = 0;
	uint8_t HK1_state = 0;

	// Brauchwasser Arbeitsvariablen
	uint8_t WW_soll = 60;		// read/write, Solltemperatur
	uint8_t WW_ist = 0;			// read only, aktuelle Temperatur (nur bei aktiviertem Warmwasser auslesen ansonsten besser gesondert)
	uint8_t WW_state = 0;		// read only, aktueller Zustand
	uint8_t WW_active = 0xFF;	// read/write, Aktivierungsflag
	uint8_t WW_diff = 5;		// read/write, offset f�r erneute Aufw�rmung

	// Bodenheizung Arbeitsvariablen
	uint8_t HK2_state = 0;		// read only, aktueller Zustand
	uint8_t HK2_soll = 42;		// read/write, Solltemperatur
	uint8_t HK2_ist = 0;		// read only, aktuelle Temperatur (nur der letzte gemessene Wert, nur bei aktivem Heizkreis verwenden. [state 1, 2 oder 3])
	uint8_t HK2_diff = 1;		// read/write, Regelgenauigkeit in Grad
	uint8_t HK2_rotate = 0;
	uint8_t ROTATION_TIME = 1;	// read/write, Bet�tigungszeit in Sekunden pro 1 Grad Temperaturdifferenz
	uint8_t HK2_direction = 0;
	uint8_t HK2_active = 0xFF;	// read/write, Aktivierungsflag
	uint8_t HK2_wait = 20;		// read/write, Wartezeit zwischen den Messungen

	while (1) {

		if (stunden > 6 && stunden <= 23){
			HK1_soll = 21;
			WW_active = 0xFF;
		}else{
			HK1_soll = 19;
			WW_active = 0;
		}

	/*	switch (Brenner_state) {
			case 0:
					//
			case 1:
					//
			default:
		}
	*/

	//-----------------state machine Heizkreis 1-----------------------------
		switch (HK1_state) {
		case 0:
				if (HK1_active) {
					HK1_state = 1;
					OW_timer = 0;				// Timer reset
					break;
				}
				break;

		/*
		 * Messung
		 */
		case 1:
				// Heizkreis 1 deaktiviert
				if (!HK1_active){
					HK1_state = 0;

					shift &= ~(1 << HK1);
					shift_set(shift);
					break;
				}
				// starte Messung
				if (OW_timer < 10) {
					ow_reset();				//reset
					ow_write(0xCC);			//SKIP ROM [CCh]
					ow_write(0x44);			//CONVERT T [44h]
					OW_timer = 10;
					break;
				}
				// Messung durchgef�hrt, lese Ergebnis
				if (OW_timer >= 12) {
					ow_reset();				//reset
					//ow_write(0x55);			//MATCH ROM [55h]
					//send_id(id1);			//send ROM id
					ow_write(0xCC);		//SKIP ROM [CCh]
					ow_write(0xBE);			//READ SCRATCHPAD [BEh]
					HK1_ist = ow_read(16) >> 4;		//read 2 bytes

					if (HK1_ist > (HK1_soll + HK1_diff)) {
						shift &= ~(1 << HK1);	// zu warm, Pumpe aus
						shift_set(shift);
					}
					if (HK1_ist < (HK1_soll - HK1_diff)) {
						shift |= (1 << HK1);	// zu kalt, Pumpe an
						shift_set(shift);
					}
					OW_timer = 0;
					HK1_state = 2;	// warten
					break;
				}
				break;

		// Warten
		case 2:
				// Heizkreis 1 deaktiviert
				if (!HK1_active){
					HK1_state = 0;
					shift &= ~(1 << HK1);
					shift_set(shift);
					break;
				}
				// timeout
				if (OW_timer > 30) {
					OW_timer = 0;
					HK1_state = 1;
					break;
				}
				break;
		//
		default: HK1_state = 0;
		}

	//-----------------state machine Warmwasser------------------------------
		switch (WW_state) {
		/*
		 * Startzustand
		 */
		case 0:	//
				if (WW_active) {
					WW_state = 1;
					shift |= (1 << WW);		// Warmwasser Ladepumpe an
					shift_set(shift);
					break;
				}
				break;
		/*
		 * Heizzustand
		 */
		case 1:
				// wenn Warmwasser deaktiviert, gehe in den Startzustand
				if (!WW_active) {
					WW_state = 0;
					shift &= ~(1 << WW);	// Warmwasser Ladepumpe aus
					shift_set(shift);
					break;
				}

				// Warmwasser Messung durchf�hren
				shift |= (1 << S1);
				shift &= ~((1 << S0) | (1 << S2));
				shift_set(shift);
				meassure();
				WW_ist = convert_mt(mval);

				// Warmwasser warm genug, gehe in den Wartezustand
				if (WW_ist >= WW_soll) {
					WW_state = 2;
					shift &= ~(1 << WW);	// Warmwasser Ladepumpe aus
					shift_set(shift);
					break;
				}

				// Kessel Messung durchf�hren
				// WW_ist mit Kesseltemperatur vergleichen und gegebenenfalls in Fehlerzustand oder einen gesonderten wechseln
				shift &= ~((1 << S0) | (1 << S1) | (1 << S2));
				shift_set(shift);
				meassure();
				Kessel_ist = convert_mt(mval);

				if ((Kessel_ist - WW_diff) <= WW_ist) {
					shift &= ~(1 << WW);	// Warmwasser Ladepumpe aus
				} else {
					shift |= (1 << WW);		// Warmwasser Ladepumpe an
				}
				shift_set(shift);
				break;
		/*
		 * Wartezustand
		 */
		case 2:
				// Warmwasser inaktiv
				if (!WW_active) {
					WW_state = 0;
					shift &= ~(1 << WW);	// Warmwasser Ladepumpe aus
					shift_set(shift);
					break;
				}

				// Warmwassertemperatur messen
				shift |= (1 << S1);
				shift &= ~((1 << S0) | (1 << S2));
				shift_set(shift);
				meassure();
				WW_ist = convert_mt(mval);

				// Warmwasser zu kalt, heizen
					if (WW_ist < (WW_soll - WW_diff)) {
						WW_state = 1;
						shift |= (1 << WW);	// Warmwasser Ladepumpe an
						shift_set(shift);
						break;
					}
					break;

			/*
			 * Fehlerzustand
			 * noch keine Aktionen definiert
			 * nicht ausreichende Kesseltemperatur als Fehler
			 */
			case 3:
					shift &= ~(1 << WW);	// Warmwasser Ladepumpe aus
					shift_set(shift);
			default:	WW_state = 3;
			}

	//-----------------state machine Heizkreis 2------------------------------
			switch (HK2_state) {
			/*
			 * Startzustand
			 */
			case 0:	//USART_SendString("_init");
					if (HK2_active) {
						shift |= (1 << HK2);	// Bodenheizung Pumpe an
						shift_set(shift);
						HK2_state = 1;
						HK2_timer = 0;
						break;
					}
					break;
			/*
			 * Wartezustand
			 * hier wird gewartet zwischen den Korrekturvorg�ngen und der Zustand gepr�fft
			 */
			case 1:
					// HK2 deaktiviert
					if (!HK2_active) {
						shift &= ~(1 << HK2);	// Bodenheizung Pumpe aus
						shift_set(shift);
						HK2_state = 0;
						break;
					}

					// FM241 nicht vorhanden
					if (!check_FM241()) {
						HK2_state = 4;
						break;
					}

					// timeout erreicht, gehe zur aktuellen Messung
					if (HK2_timer >= HK2_wait){
						HK2_state = 2;
						break;
					}
					break;
			/*
			 * Messung durchf�hren und auswerten
			 */
			case 2:	//USART_SendString("_meassure");
					// Multiplexer IC10 Port A6 messen
					shift |= (1 << S1) | (1 << S2);
					shift &= ~(1 << S0);
					shift_set(shift);
					meassure();
					HK2_ist = convert_mt(mval);

					// Kesseltemperatur abfragen
					// falls Temperatur nicht Ausreichend Pumpe an lassen, aber Mischer eventuell abschalten

					// Vorlauf OK
					if ((HK2_ist >= (HK2_soll - HK2_diff)) && (HK2_ist <= (HK2_soll + HK2_diff))) {
						HK2_state = 1;
						HK2_timer = 0;
						break;
					}

					// Vorlauf kalt
					if (HK2_ist < (HK2_soll - HK2_diff)) {
						HK2_rotate = (HK2_soll - HK2_ist) * ROTATION_TIME;
						HK2_direction = 0xFF;
						HK2_state = 3;
						HK2_timer = 0;
						break;
					}
					// Vorlauf warm
					if (HK2_ist > (HK2_soll + HK2_diff)) {
						HK2_rotate = (HK2_ist - HK2_soll) * ROTATION_TIME;
						HK2_direction = 0;
						HK2_state = 3;
						HK2_timer = 0;
						break;
					}
				break;

			/*
			 * Drehung wird durchgef�hrt
			 */
			case 3:	if (HK2_timer >= HK2_rotate) {
						HK2_state = 1;
						HK2_timer = 0;
						shift &= ~((1 << MW) | (1 << MK));
						shift_set(shift);
						break;
					}

					if (HK2_direction) {
						shift |= (1 << MW);
					} else {
						shift |= (1 << MK);
					}
					shift_set(shift);
					break;

			/*
			 * Fehlerzustand
			 */
			case 4:	//USART_SendString("_error");
					if (!HK2_active) {
						HK2_state = 0;
						shift &= ~(1 << HK2);	// Bodenheizung Pumpe aus
						shift_set(shift);
						break;
					}
					shift &= ~(1 << HK2);	// Bodenheizung Pumpe aus
					shift_set(shift);
					if (check_FM241()) {
						HK2_state = 1;
						HK2_timer = 0;
						break;
					}
					break;
			default:	HK2_state = 4;
			}


	//-----------------------------------------------------------------------


			char tr[10];


			shift |= (1 << S1) | (1 << S2);
			shift &= ~(1 << S0);
			shift_set(shift);
			meassure();
			ultoa(mval, tr, 10);
			hd44780_clear();
			hd44780_puts(tr);
			hd44780_puts("-");
			ultoa(convert_mt(mval), tr, 10);
			hd44780_puts(tr);

			hd44780_puts(" ");
			utoa(WW_ist, tr, 10);
			hd44780_puts(tr);

//		// HK1 Parameter
//		if ((debug_timer < 5)){
//			char test[8];
//			hd44780_clear();	// clear screen
//
//			hd44780_puts("HK1: ");
//			ltoa(HK1_soll, test, 10);
//			hd44780_puts(test);
//
//			hd44780_puts(" ");
//			utoa(HK1_ist, test, 10);
//			hd44780_puts(test);
//
//			hd44780_puts(" ");
//			ultoa(HK1_state, test, 10);
//			hd44780_puts(test);
//			debug_timer = 5;
//		}
//		// HK2 Parameter
//		if ((debug_timer > 10) && (debug_timer < 15)){
//			char test[8];
//			hd44780_clear();	// clear screen
//
//			hd44780_puts("HK2: ");
//			ltoa(HK2_soll, test, 10);
//			hd44780_puts(test);
//
//			hd44780_puts(" ");
//			utoa(HK2_ist, test, 10);
//			hd44780_puts(test);
//
//			hd44780_puts(" ");
//			ultoa(HK2_state, test, 10);
//			hd44780_puts(test);
//
//			debug_timer = 15;
//		}
//
//		if (debug_timer > 20) {
//			debug_timer = 0;
//		}
	}
}

// Interrupt f�r ADC Messung
ISR (TIMER0_OVF_vect){							// Timer 0 Overflow interrupt
	if (PIND & (1 << COMP)){
		mval++;
	} else {
		TCCR0 = 0;					// timer anhalten
		TIMSK &= ~(1 << TOIE0);		// overflow interrupt deaktivieren
	}
}

// 1sec Interrupt
ISR (TIMER1_COMPA_vect){
	sekunden++;
	if (sekunden >= 60){
		sekunden = 0;
		minuten++;
		if (minuten >= 60){
			minuten = 0;
			stunden++;
			if (stunden >= 24){
				stunden = 0;
			}
		}
	}
	OW_timer++;
	HK2_timer++;
	debug_timer++;
}
