/*
 Copyright:      Radig Ulrich  mailto: mail@ulrichradig.de
 Author:         Radig Ulrich
 Remarks:        
 known Problems: none
 Version:        28.05.2004
 Description:    Dieses Programm dient als Beispiel zur Ansteuerung einer MMC/SD-Memory-Card.
				 Zum Zugriff auf eine MMC/SD-Karte, mu� man nur die Datei mmc.c
				 in sein eigenes Programm einf�gen.
*/

#include <stdio.h>
#include <string.h>	
#include <avr/io.h>	
#include <avr/eeprom.h>	
#include <stdlib.h>			// itoa
#include <util/delay.h>
	
#include "mmc.h"
#include "fat.h"
#include "uart.h"

//Hauptprogramm
int main (void) {
	uart_init();

	uart_puts("\n\n\r");
	//Initialisierung der MMC/SD-Karte
	uart_puts("System Ready!\r\n");
	while (mmc_init() != 0) {	//ist der R�ckgabewert ungleich NULL ist ein Fehler aufgetreten
		uart_puts("** Keine MMC/SD Karte gefunden!! **\n\r");
	}
	uart_puts("Karte gefunden!!\n\r");
	
	uint8_t Buffer[512];

	mmc_read_sector (0,Buffer); //Read Master Boot Record

	char buf[20];
	
	uint16_t i;
	uint16_t j;
	for (j = 0; ; j++) {
		uart_puts("read sector ");
		itoa(j,buf,10);
		uart_puts(buf);
		uart_puts(":\n\r");
		mmc_read_sector (j,Buffer);
		for (i = 0; i < 512; i++) {
			itoa(Buffer[i], buf, 16);
			uart_puts(buf);
			uart_putc(' ');
		}
		uart_puts("\n\n\r");
		//_delay_ms(2000);
	}

//	uart_puts("\n\n\r write sector 0:\n\r");
//
//	for (i = 0; i < 512; i++) {
//		Buffer[i] = 0xAA;
//	}
//
//	mmc_write_sector(0,Buffer);
//
//	uart_puts("\n\n\n\rread sector 0:\n\r");
//	for (i = 0;i < 512;i++) {
//		itoa(Buffer[i], buf, 16);
//		uart_puts(buf);
//		uart_putc(' ');
//	}

	uart_puts("\n\rFERTIG!!\r\n");
	while (1) {}
return (1);
}
