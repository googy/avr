#include <avr/io.h>
#include <util/delay.h>
#define TRUE 1

// LCD
#define LCD_PORT	PORTD
#define LCD_DDR		DDRD
#define LCD_DB		PD0

#define LCD_RS		PD4
#define LCD_RW		PD5
#define LCD_EN		PD6


int main() {

	DDRD = 0xFF;	// alles Ausg�nge
	PORTD = 0;	// alles low

	_delay_ms(15);	// warten auf die Bereitschaft des LCD

	// 3 mal 0x30 senden
	PORTD &= ~(0xF0 >> 4);	// Datenpins l�schen
	PORTD |= (0x30 >> 4);	// Bits setzen

	// Enable puls
    PORTD |= (1 << LCD_EN);     // Enable auf 1 setzen
    _delay_us( 20 );  // kurze Pause
    PORTD &= ~(1 << LCD_EN);    // Enable auf 0 setzen

    // warte
    _delay_ms( 5 );

    // Enable puls
    LCD_PORT |= (1 << LCD_EN);     // Enable auf 1 setzen
    _delay_us(20);  // kurze Pause
    LCD_PORT &= ~(1 << LCD_EN);    // Enable auf 0 setzen

    _delay_ms( 1 );

    // Enable puls
    LCD_PORT |= (1<<LCD_EN);     // Enable auf 1 setzen
    _delay_us( 20 );  // kurze Pause
    LCD_PORT &= ~(1<<LCD_EN);    // Enable auf 0 setzen

    _delay_ms( 1 );

    // 4-bit Modus aktivieren
	PORTD &= ~(0xF0 >> 4);	// Datenpins l�schen
	PORTD |= (0x20 >> 4);	// Bits setzen

    _delay_ms( 5 );


    // command--0x28----------------------------------------------
    // 4-bit Modus / 2 Zeilen / 5x7
    LCD_PORT &= ~(1 << LCD_RS);    // RS auf 0 setzen


    // 0x28 -> 0x2 schreiben
    PORTD &= ~(0xF0 >> 4);	// Datenpins l�schen
	PORTD |= (0x28 >> 4);	// Bits setzen

	// Enable puls
    LCD_PORT |= (1<<LCD_EN);     // Enable auf 1 setzen
    _delay_us( 20 );  // kurze Pause
    LCD_PORT &= ~(1<<LCD_EN);    // Enable auf 0 setzen

    // 0x28 -> 0x8 schreiben
    PORTD &= ~(0xF0 >> 4);	// Datenpins l�schen
	PORTD |= ((0x28 << 4) >> 4);	// Bits setzen

    // Enable puls
    LCD_PORT |= (1<<LCD_EN);     // Enable auf 1 setzen
    _delay_us( 20 );  // kurze Pause
    LCD_PORT &= ~(1<<LCD_EN);    // Enable auf 0 setzen

    _delay_us( 42 );





    // command--0x0F----------------------------------------------
    LCD_PORT &= ~(1 << LCD_RS);    // RS auf 0 setzen


    // 0x0F -> 0x2 schreiben
    PORTD &= ~(0xF0 >> 4);	// Datenpins l�schen
	PORTD |= (0x0F >> 4);	// Bits setzen

	// Enable puls
    LCD_PORT |= (1<<LCD_EN);     // Enable auf 1 setzen
    _delay_us( 20 );  // kurze Pause
    LCD_PORT &= ~(1<<LCD_EN);    // Enable auf 0 setzen

    // 0x28 -> 0x8 schreiben
    PORTD &= ~(0xF0 >> 4);	// Datenpins l�schen
	PORTD |= ((0x0F << 4) >> 4);	// Bits setzen

    // Enable puls
    LCD_PORT |= (1<<LCD_EN);     // Enable auf 1 setzen
    _delay_us( 20 );  // kurze Pause
    LCD_PORT &= ~(1<<LCD_EN);    // Enable auf 0 setzen

    _delay_us( 42 );



    // Cursor inkrement / kein Scrollen
//    lcd_command( LCD_SET_ENTRY |
//                 LCD_ENTRY_INCREASE |
//                 LCD_ENTRY_NOSHIFT );

//    lcd_clear();


	while (TRUE) {

	}
	return 1;
}
