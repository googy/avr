
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>

#include <util/delay.h>
#include <stdio.h>
#include <inttypes.h>

#include <stdlib.h>	// itoa

#include "mcp2515.h"
#include "global.h"
#include "defaults.h"

#include "uart.h"

int main(void) {
	char buf[10];
	uart_init();

	DDRB |= (1 << PB0);
	PORTB &= ~(1 << PB0);
	_delay_ms(500);
	PORTB |= (1 << PB0);

	sei();	// f�r MCP Interrupts

	// Versuche den MCP2515 zu initilaisieren
	if (!mcp2515_init()) {
		uart_puts("MCP2515 FAIL\n\r");
		return 0;
	}
	else {
		uart_puts("MCP2515 OK\n\r");
	}
	
	uart_puts("Erzeuge Nachricht\n\r");
	tCAN message;
	
	message.id = 0x123;		// kommt nicht am Filter vorbei
	message.header.rtr = 0;
	message.header.length = 2;
	message.data[0] = 0xab;
	message.data[1] = 0xcd;
	
	//PRINT("\nwechsle zum Loopback-Modus\n\n");
	mcp2515_bit_modify(CANCTRL, (1<<REQOP2)|(1<<REQOP1)|(1<<REQOP0), (1<<REQOP1));
	
	// Sende eine Nachricht
	if (mcp2515_send_message(&message)) {
		uart_puts("Nachricht wurde in die Puffer geschrieben\n\r");
	}
	else {
		uart_puts("Fehler: konnte die Nachricht nicht senden\n\r");
	}
	
	// warte ein bisschen
	_delay_ms(10);
	
	if (mcp2515_check_message()) {
		uart_puts("Nachricht empfangen!\n\r");
		
		// read the message from the buffers
		if (mcp2515_get_message(&message)) {
			uart_puts("message received\n\r");
		}
		else {
			uart_puts("Nachricht konnte nicht gelesen werden\n\r");
		}
	}
	else {
		uart_puts("Fehler: keine Nachricht empfangen\n\r");
	}
	
	uart_puts("zurueck zum normalen Modus\n\r");
	mcp2515_bit_modify(CANCTRL, (1<<REQOP2)|(1<<REQOP1)|(1<<REQOP0), 0);
	
	// wir sind ab hier wieder mit dem CAN Bus verbunden
	
	uart_puts("Versuche die Nachricht per CAN zu verschicken\n\r");
	
	// Versuche nochmal die Nachricht zu verschicken, diesmal per CAN
	if (mcp2515_send_message(&message)) {
		uart_puts("Nachricht wurde in die Puffer geschrieben\n\r");
	}
	else {
		uart_puts("Fehler: konnte die Nachricht nicht senden\n\r");
	}
	
	uart_puts("Warte auf den Empfang von Nachrichten\n\r");
	
	while (1) {
		if (mcp2515_check_message()) {
			if (mcp2515_get_message(&message)) {
				uart_puts("ID: ");
				itoa((message.id), buf, 16);
				uart_puts(buf);
				uart_puts("   DLC: ");
				itoa((message.header.length), buf, 10);
				uart_puts(buf);
				uart_puts("   Data: ");
				for (uint8_t i = 0; i < message.header.length; i++) {
					itoa((message.data[i]), buf, 16);
					uart_puts(buf);
					uart_putc(' ');
				}
				uart_puts("\n\r");
			}
			else {
				uart_puts("FAIL\n\r");
			}
		}
	}
	
	return 0;
}
