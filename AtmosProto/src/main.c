/*
 * main.c
 *
 *  Created on: 29.04.2018
 *      Author: googy
 */
#include <avr/io.h>         // AVR Register, GPIO
#include <avr/interrupt.h>  // sei() Interrupt enable
#include <util/delay.h>     // _delay_ms()
#include <stdlib.h>         // itoa()   int to ascii

#include "uart.h"
#include "mcp2515.h"
#include "timebase.h"
#include "buderus/messung.h"
#include "buderus/sensoren.h"


uint8_t broadcast = 1;		// broadcast
tCAN message, rMessage;

char buf[10];

#define BRENNER PD0
#define INT     PD2
#define HK1     PD3
#define WW      PD4
#define PZ      PD5
#define MW      PD6
#define MK      PD7
#define ICP     PB0
#define HK2     PB1
#define S0      PC0
#define S1      PC1
#define S2      PC2
#define T5      PC3



struct hk2_opt {
    uint8_t state;
    uint8_t active : 1;
    uint8_t present : 1;
    uint16_t wait;
    uint8_t ist;
    uint8_t soll;
    uint8_t diff;
    uint16_t rotate;
    uint8_t ROTATION_TIME;
};

struct {
    struct hk2_opt hk2;
}hkopt;


static uint32_t hk2_timecounter = 0;

void hk2_state_machine() {
    if (values.vorlauf_hk2 > 0)
        hkopt.hk2.ist = values.vorlauf_hk2;
    _delay_ms(500);
    uart_puts("IST: ");
    uart_puts(itoa(hkopt.hk2.ist, buf, 10));
    uart_puts("     State: ");
    uart_puts(itoa(hkopt.hk2.state, buf, 10));
    uart_puts("     rotate: ");
    uart_puts(itoa(hkopt.hk2.rotate, buf, 10));
    uart_puts("\n\r");
    switch (hkopt.hk2.state) {
    case 0:
        if (hkopt.hk2.active) {
            PORTB |= (1 << HK2); // Bodenheizung Pumpe an
            hkopt.hk2.state = 1;
            stoptimer_init(&hk2_timecounter);
            break;
        }
        break;

        //           * Wartezustand
        //           * hier wird gewartet zwischen den Korrekturvorg�ngen und der Zustand gepr�fft

    case 1:
        // HK2 deaktiviert
        if (!hkopt.hk2.active) {
            PORTB &= ~(1 << HK2); // Bodenheizung Pumpe aus
            hkopt.hk2.state = 0; // gehe in default Ruhezustand
            break;
        }

        // FM241 nicht vorhanden
        if (!hkopt.hk2.present) {
            hkopt.hk2.state = 4; // Mudul nicht erkannt, Fehlerzustand
            break;
        }

        // timeout erreicht, gehe zur aktuellen Messung
        if (stoptimer_expired(&hk2_timecounter, hkopt.hk2.wait)) {
            hkopt.hk2.state = 2;
        }
        break;

        //       * Messung auswerten

    case 2:
        // falls Temperatur nicht ausreichend Pumpe an lassen, aber Mischer eventuell abschalten

        // Vorlauf OK, ist liegt im Sollbereich
        if ((hkopt.hk2.ist >= (hkopt.hk2.soll - hkopt.hk2.diff)) && (hkopt.hk2.ist <= (hkopt.hk2.soll
                + hkopt.hk2.diff))) {
            hkopt.hk2.state = 1;
            stoptimer_init(&hk2_timecounter);
            uart_puts("OK\n\r");
            break;
        }
        if (hkopt.hk2.ist < (hkopt.hk2.soll - hkopt.hk2.diff)) { // Vorlauf kalt
            hkopt.hk2.rotate = (hkopt.hk2.soll - hkopt.hk2.ist) * hkopt.hk2.ROTATION_TIME * 1000;
            PORTD |= (1 << MW); // drehe Mischer auf wärmer
            hkopt.hk2.state = 3;
            stoptimer_init(&hk2_timecounter);
            uart_puts("warmer\n\r");
            break;

            // falls Temperatur im Speicher und Kessel zu niedrig ist, Messung verz�gern (to-do)
        }
        if (hkopt.hk2.ist > (hkopt.hk2.soll + hkopt.hk2.diff)) { // Vorlauf warm
            hkopt.hk2.rotate = (hkopt.hk2.ist - hkopt.hk2.soll) * hkopt.hk2.ROTATION_TIME * 1000;
            PORTD |= (1 << MK);
            uart_puts("colder\n\r");
            hkopt.hk2.state = 3;
            stoptimer_init(&hk2_timecounter);
            break;
        }
        break;

        //       * Drehung wird durchgef�hrt
    case 3:
        if (stoptimer_expired(&hk2_timecounter, hkopt.hk2.rotate)) {
            hkopt.hk2.state = 1;
            stoptimer_init(&hk2_timecounter);
            PORTD &= ~((1 << MW) | (1 << MK)); // Mischer aus
            uart_puts("alles AUS\n\r");
        }
        break;

        //       * Fehlerzustand

    case 4:
        if (!hkopt.hk2.active) {
            hkopt.hk2.state = 0;
            PORTD &= ~(1 << HK2); // Bodenheizung Pumpe aus
            break;
        }
        //shift &= ~(1 << HK2); // Bodenheizung Pumpe aus
        //shift_set(shift);
        if (hkopt.hk2.present) {
            hkopt.hk2.state = 1;
            stoptimer_init(&hk2_timecounter);
            break;
        }
        break;
    default:
        hkopt.hk2.state = 4;
    }
}






int main() {

    uart_init();

    while (1) {
            uart_puts("test\n\r");
        }

    sei();	// Timer, ICP
    timer0_init();  // Zeitbasis

    // Versuche den MCP2515 zu initilaisieren
    if (!mcp2515_init()) {
        uart_puts("MCP2515 FAIL\n\r");
        return 0;
    } else {
        uart_puts("MCP2515 OK\n\r");
    }
    // WARMWASSER
    DDRD |= (1 << WW);
    PORTD |= (1 << WW);
    // ----------

    hkopt.hk2.state = 0;
    hkopt.hk2.active = 1;
    hkopt.hk2.present = 1;
    hkopt.hk2.wait = 30000;
    hkopt.hk2.soll = 32;
    hkopt.hk2.diff = 0;
    hkopt.hk2.ROTATION_TIME = 1;
    hkopt.hk2.ist = hkopt.hk2.soll;
    DDRB |= (1 << HK2);
    DDRD |= (1 << MW) | (1 << MK);
    while (1) {
        buderus_sensoren();
        hk2_state_machine();

//        message.id = 0x20;
//        message.header.rtr = 0;
//        message.header.length = 1;
//        message.data[0] = digit;
//        // send message
//        if (mcp2515_send_message(&message)) {
//            //uart_puts("Nachricht konnte gesendet werden\n\r");
//        } else {
//            //uart_puts("ERROR Nachricht konnte nicht gesendet werden\n\r");
//        }

//            can_puts((char*)&sentence[0]); //TODO differ in signedness
//            can_puts("\n\r");

//////////////////////////////////////////////////////receive_CAM_message/////////////////////////
/*		// check for incoming messages
		while (mcp2515_check_message()) {		// Nachricht vorhanden
			if (mcp2515_get_message(&message)) {		// Nachricht erfolgreich angekommen
				if (rMessage.header.rtr) {
					// send data
				}

//				if (id = x) {		// set broadcast
//					broadcast = data;
//				}
				uart_puts("ID: ");
				itoa((message.id), buf, 16);
				uart_puts(buf);
				uart_puts("   DLC: ");
				itoa((message.header.length), buf, 10);
				uart_puts(buf);
				uart_puts("   Data: ");
				for (uint8_t i = 0; i < message.header.length; i++) {
					itoa((message.data[i]), buf, 10);
					uart_puts(buf);
					uart_putc(' ');
				}
				uart_puts("\n\r");
			}
			else {
				uart_puts("FAIL\n\r");
			}
		}*/
    }
    return 0;
}
