/*
 * ssd1306.c
 *
 *  Created on: 10.05.2015
 *      Author: googy_000
 */

#include "i2cmaster.h"
#include <avr/pgmspace.h>
#include "ssd1306.h"


void sendCommand(uint8_t command) {
	i2c_start(DevSSD1306);
	i2c_write(0x00);
	i2c_write(command);
	i2c_stop();
}

void init_ssd1306() {
	i2c_init();

	i2c_start(DevSSD1306 + I2C_WRITE);	// set device address and write mode

	// Init sequence for 128x64 OLED module
//	i2c_write(0xAE);                    // Display Off
//
//    i2c_write(0xAE);                    // Display
//	i2c_write(0x00 | 0x0);            // low col = 0
//	i2c_write(0x10 | 0x0);           // hi col = 0
//	i2c_write(0x40 | 0x0);            // line #0
//
//	i2c_write(0x81);                   // Set Contrast 0x81
//	i2c_write(0xCF);
//	// flips display
//	i2c_write(0xA1);                    // Segremap - 0xA1
//	i2c_write(0xC8);                    // COMSCAN DEC 0xC8 C0
//	i2c_write(0xA6);                    // Normal Display 0xA6 (Invert A7)
//
//	i2c_write(0xA4);                // DISPLAY ALL ON RESUME - 0xA4
//	i2c_write(0xA8);                    // Set Multiplex 0xA8
//	i2c_write(0x3F);                    // 1/64 Duty Cycle
//
//	i2c_write(0xD3);                    // Set Display Offset 0xD3
//	i2c_write(0x0);                     // no offset
//
//	i2c_write(0xD5);                    // Set Display Clk Div 0xD5
//	i2c_write(0x80);                    // Recommnedi2c_write(pgm_read_byte(&(font[((*s - 25) * 5) + c])));ed resistor ratio 0x80
//
//	i2c_write(0xD9);                  // Set Precharge 0xd9
//	i2c_write(0xF1);
//
//	i2c_write(0xDA);                    // Set COM Pins0xDA
//	i2c_write(0x12);
//
//	i2c_write(0xDB);                 // Set VCOM Detect - 0xDB
//	i2c_write(0x40);
//
//	i2c_write(0x20);                    // Set Memory Addressing Mode
//	i2c_write(0x00);                    // 0x00 - Horizontal
//
//	i2c_write(0x40 | 0x0);              // Set start line at line 0 - 0x40
//
//	i2c_write(0x8D);                    // Charge Pump -0x8D
//	i2c_write(0x14);
//
//	i2c_write(0xA4);              //--turn on all pixels - A5. Regular mode A4
//	i2c_write(0xAF);                //--turn on oled panel - AF
//
//	i2c_write(0x21);        // column address
//	i2c_write(0);           // column start address (0 = reset)
//	i2c_write(127);         // column end addres (127 = reset)
//
//	i2c_write(0x22);        // page address
//	i2c_write(0);           // page start address (0 = reset);
//	i2c_write(3);           // page end address



	// Init sequence
	sendCommand(SSD1306_DISPLAYOFF);                    // 0xAE
	sendCommand(SSD1306_SETDISPLAYCLOCKDIV);            // 0xD5
	sendCommand(0x80); // the suggested ratio 0x80

	sendCommand(SSD1306_SETMULTIPLEX);                  // 0xA8
	sendCommand(SSD1306_LCDHEIGHT - 1);

	sendCommand(SSD1306_SETDISPLAYOFFSET);              // 0xD3
	sendCommand(0x0);                                   // no offset
	sendCommand(SSD1306_SETSTARTLINE | 0x0);            // line #0
	sendCommand(SSD1306_CHARGEPUMP);                    // 0x8D

	sendCommand(0x14);
	sendCommand(SSD1306_MEMORYMODE);                    // 0x20
	sendCommand(0x00);                                  // 0x0 act like ks0108
	sendCommand(SSD1306_SEGREMAP | 0x1);
	sendCommand(SSD1306_COMSCANDEC);


	sendCommand(SSD1306_SETCOMPINS);                    // 0xDA
	sendCommand(0x02);
	sendCommand(SSD1306_SETCONTRAST);                   // 0x81
	sendCommand(0x8F);


	sendCommand(SSD1306_SETPRECHARGE);                  // 0xd9

	sendCommand(0xF1);
	sendCommand(SSD1306_SETVCOMDETECT);                 // 0xDB
	sendCommand(0x40);
	sendCommand(SSD1306_DISPLAYALLON_RESUME);           // 0xA4
	sendCommand(SSD1306_NORMALDISPLAY);                 // 0xA6

	sendCommand(SSD1306_DEACTIVATE_SCROLL);

	sendCommand(SSD1306_DISPLAYON);//--turn on oled panel

	i2c_stop();
}

void clear() {
    // write whole display
    sendCommand(SSD1306_COLUMNADDR);
    sendCommand(0);   // Column start address (0 = reset)
    sendCommand(SSD1306_LCDWIDTH-1); // Column end address (127 = reset)

    sendCommand(SSD1306_PAGEADDR);
    sendCommand(0); // Page start address (0 = reset)
    sendCommand(3); // Page end address
	uint8_t twbrbackup = TWBR;
	TWBR = 12;
	for (uint16_t i = 0; i < ((128 * 32) / 8); i++) {
		unsigned char ret = i2c_start(DevSSD1306 + I2C_WRITE); // set device address and write mode
		if (ret) {
			/* failed to issue start condition, possibly no device found */
			i2c_stop();
		} else {
			i2c_write(0x40);      // set display RAM display start line register
			for (uint8_t x = 0; x < 16; x++) {
				i2c_write(0x00);
				i++;
			}
			i--;
			i2c_stop();
			TWBR = twbrbackup;
		}
	}
}

void setCursor(uint8_t page, uint8_t column) {
    sendCommand(SSD1306_COLUMNADDR);
    sendCommand(column);   // Column start address (0 = reset)
    sendCommand(SSD1306_LCDWIDTH-1); // Column end address (127 = reset)

    sendCommand(SSD1306_PAGEADDR);
    sendCommand(page); // Page start address (0 = reset)
    sendCommand(3); // Page end address
}

void write_bytes(uint8_t* data, uint8_t bytes) {
	i2c_start(DevSSD1306);
	i2c_write(0x40);		//
	for (uint8_t packet_byte = 0; packet_byte < bytes; packet_byte++) {
		if (packet_byte % 5 == 0) {
			i2c_write(0x00);
		}
		i2c_write(pgm_read_byte(&(font[packet_byte + 495])));
	}
	i2c_stop();
}

void oled_print(char *s) {
	while (*s) { /* so lange *s != '\0' also ungleich dem "String-Endezeichen(Terminator)" */
		i2c_start(DevSSD1306);
		i2c_write(0x40);
		for (uint8_t c = 0; c < 5; c++) {
			i2c_write(pgm_read_byte(&(font[((*s - 25) * 5) + c])));
		}
		i2c_write(0x00);
		s++;
		i2c_stop();
	}
}
