################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/main.c \
../src/mcp2515.c \
../src/ssd1306.c \
../src/timebase.c \
../src/twimaster.c \
../src/uart.c 

OBJS += \
./src/main.o \
./src/mcp2515.o \
./src/ssd1306.o \
./src/timebase.o \
./src/twimaster.o \
./src/uart.o 

C_DEPS += \
./src/main.d \
./src/mcp2515.d \
./src/ssd1306.d \
./src/timebase.d \
./src/twimaster.d \
./src/uart.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -Os -fpack-struct -fshort-enums -ffunction-sections -fdata-sections -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega8 -DF_CPU=1000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


