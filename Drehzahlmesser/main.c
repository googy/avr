/*
 * main.c
 *
 *  Created on: 31.08.2014
 *      Author: googy_000
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdlib.h>
#include "uart.h"

char buf[10];

int main() {
	DDRB &= ~(1 << PB1);	// AIN1 als Eingang
	DDRB &= ~(1 << PB0);	// AIN0 als Eingang
	DDRD |= (1 << PD5);		// LED1 als Ausgang
	uart_init();

	uint8_t f = 0;
	uint32_t x = 0;

	// Analog Comparator
	ACSR = 0
//		| (1 << ACD)	// disable Comparator
//		| (1 << ACBG)	// use internal 1.1V BandGapReference instead of AIN0
//		| (1 << ACIE)	// enable Comparator Interrupt
		| (1 << ACIC)	// connect Comparator output to Timer1 InputCapture unit
//		| (1 << ACIS1)	// select falling edge to fire comparator interrupt
//		| (0 << ACIS0)	//
		;
	//DIDR = (1 << AIN1D) | (AIN0D);	// disable digital input buffers on AIN0/1

	// Timer1 init
	TCCR1A = 0;
	TCNT1 = 0;				// reset timer value
	ICR1 = 0;				// reset input capture value
	TCCR1B = (1 << ICNC1)	// enable noise canceler
		| (0 << ICES1)		// use fallung edge
		| (0 << CS12)		// prescaler to 8
		| (1 << CS11)		//
		| (0 << CS10)		//
		;
	TIMSK = (1 << 3);	// input capture interrupt enable [ICIE1]


	sei();

	while (1) {

	}
}

ISR(TIMER1_CAPT_vect) {
	TCCR1B &= ~((1 << CS12) | (1 << CS11) | (1 << CS10));	// stop timer
	uint16_t val = ICR1;									// save value
	TCNT1 = 0;												// reset timer
	TCCR1B |= (0 << CS12) | (1 << CS11) | (0 << CS10);		// start timer
	ltoa(val, buf, 10);
	uart_puts(buf);
	uart_putc(10);
	uart_putc(13);
	// 8000000 clock, prescaler 8 -> 1000000 timer clock -> 0.000001s per tick
	// 30000 ticks per run -> 0,03s per run -> 33,3 runs per second
	// 2000 runs per min.
}
