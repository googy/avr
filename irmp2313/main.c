/*---------------------------------------------------------------------------------------------------------------------------------------------------
 * main.c - demo main module to test irmp decoder
 *
 * Copyright (c) 2009-2013 Frank Meyer - frank(at)fli4l.de
 *
 * $Id: main.c,v 1.17 2013/01/17 07:33:14 fm Exp $
 *
 * This demo module is runnable on AVRs and LM4F120 Launchpad (ARM Cortex M4)
 *
 * ATMEGA88 @ 8 MHz internal RC      Osc with BODLEVEL 4.3V: lfuse: 0xE2 hfuse: 0xDC efuse: 0xF9
 * ATMEGA88 @ 8 MHz external Crystal Osc with BODLEVEL 4.3V: lfuse: 0xFF hfuse: 0xDC efuse: 0xF9
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 */
#include <avr/io.h>
#include "irmp.h"
#include <stdlib.h>
#include <avr/delay.h>

#ifndef F_CPU
#error F_CPU unkown
#endif

#define BAUD 9600UL      // Baudrate
// Berechnungen
#define UBRR_VAL ((F_CPU+BAUD*8)/(BAUD*16)-1)   // clever runden
#define BAUD_REAL (F_CPU/(16*(UBRR_VAL+1)))     // Reale Baudrate
#define BAUD_ERROR ((BAUD_REAL*1000)/BAUD) // Fehler in Promille, 1000 = kein Fehler.
#if ((BAUD_ERROR<990) || (BAUD_ERROR>1010))
#error Systematischer Fehler der Baudrate gr�sser 1% und damit zu hoch!
#endif

//#define USART

#ifdef USART
void uart_init() {
	UBRRH = UBRR_VAL >> 8;
	UBRRL = UBRR_VAL & 0xFF;
	UCSRB |= (1 << TXEN);
	UCSRC = (1 << UCSZ1) | (1 << UCSZ0);	//8bit, rest default
}

void uart_putc(unsigned char c) {
	while (!(UCSRA & (1 << UDRE))) {
	}
	UDR = c;
}

void uart_puts(char *s) {
	while (*s) {
		uart_putc(*s);
		s++;
	}
}
#endif

void timer0_init() {									// IRMP Timer
	TCCR0A = (1 << WGM01);								// CTC mode
	TCCR0B = (0 << CS02) | (1 << CS01) | (0 << CS00);	// Prescaler 8
	OCR0A = (F_CPU / 8 / F_INTERRUPTS) - 1;	// compare value: 1/15000 of Timer frequency
	TIMSK = (1 << OCIE0A);					// OCIE2: Interrupt by timer compare
}

ISR(TIMER0_COMPA_vect) {// Timer2 output compare interrupt service routine, called every 1/15000 sec
	(void) irmp_ISR();									// call irmp ISR
}

void timer1_10bit_pwm() {
	TCCR1A |= (1 << COM1A1) | (1 << WGM10) | (1 << WGM11);
	TCCR1B = (0 << CS12) | (0 << CS11) | (1 << CS10);
}

int main(void) {
	DDRB |= (1 << PB3);
	timer1_10bit_pwm();

	uint8_t h = 0;
	IRMP_DATA irmp_data;

	irmp_init();                                              // initialize irmp
	timer0_init();                                          // initialize timer1
	sei ();
	// enable interrupts

	const uint16_t pwmtable[9] = { 0, 1, 2, 5, 15, 50, 100, 255, 1023 };

#ifdef USART
	char buf[10];
	uart_init();
#endif
	for (;;) {
		if (irmp_get_data(&irmp_data) && irmp_data.protocol == 2
				&& irmp_data.address == 0xef00) {
			if (irmp_data.command == 0x3 && irmp_data.flags == 0) {
				OCR1A = 0;
			}
			if (irmp_data.command == 0xd && irmp_data.flags == 0 && h < 8) {
				h++;
				OCR1A = pwmtable[h];
			}
			if (irmp_data.command == 0x15 && irmp_data.flags == 0 && h > 0) {
				h--;
				OCR1A = pwmtable[h];
			}

#ifdef USART
			//uart_puts(utoa(irmp_data.protocol, buf, 10));
			//uart_putc(' ');
			//uart_puts(utoa(irmp_data.address, buf, 16));
			//uart_putc(' ');
			uart_puts(utoa(irmp_data.command, buf, 16));
			uart_putc(' ');
			uart_puts(utoa(irmp_data.flags, buf, 2));
			uart_putc(' ');
			uart_puts(utoa(OCR1A, buf, 10));
			uart_putc(10);
			uart_putc(13);
#endif
			// ir signal decoded, do something here...
			// irmp_data.protocol is the protocol, see irmp.h
			// irmp_data.address is the address/manufacturer code of ir sender
			// irmp_data.command is the command code
			// irmp_protocol_names[irmp_data.protocol] is the protocol name (if enabled, see irmpconfig.h)
		}
	}
}
