/*
 * shift.c
 *
 *  Created on: 09.10.2014
 *      Author: googy_000
 */

#include <avr/io.h>

#define	STROBE	PC3
#define	DATA	PC1
#define	CLOCK	PC2

void shift_set(uint8_t command, uint16_t data) {
	PORTC &= ~((1 << DATA) | (1 << CLOCK) | (1 << STROBE));		// (init) Data, Clock, Strobe -> low

	for (uint8_t i = 0; i < 16; i++){
		PORTC &= ~((1 << DATA) | (1 << CLOCK));		// (cycle init) Data, Clock -> low
		if (1 & (data >> (15 - i))) {
			PORTC |= (1 << DATA);
		}
		PORTC |= (1 << CLOCK);						// einshiften
	}

	for (uint8_t i = 0; i < 8; i++){
		PORTC &= ~((1 << DATA) | (1 << CLOCK));		// (cycle init) Data, Clock -> low
		if (1 & (command >> (7 - i))) {
			PORTC |= (1 << DATA);
		}
		PORTC |= (1 << CLOCK);						// einshiften
	}

	PORTC &= ~((1 << DATA) | (1 << CLOCK));			// aufr�umen
	PORTC |= (1 << STROBE);							// auf den Ausgang legen
	PORTC &= ~(1 << STROBE);
}

void shift_init() {
	DDRC |= (1 << STROBE) | (1 << DATA) | (1 << CLOCK);
	PORTC &= ~((1 << STROBE) | (1 << DATA) | (1 << CLOCK));
	shift_set(0,0);
}
