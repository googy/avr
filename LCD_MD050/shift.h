/*
 * shift.h
 *
 *  Created on: 09.10.2014
 *      Author: googy_000
 */

#ifndef SHIFT_H_
#define SHIFT_H_

void shift_set(uint8_t command, uint16_t data);
void shift_init();

#endif /* SHIFT_H_ */
