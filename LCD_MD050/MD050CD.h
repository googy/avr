/*
 * MD050CD.h
 *
 *  Created on: 09.10.2014
 *      Author: googy_000
 */

#ifndef MD050CD_H_
#define MD050CD_H_

#define REST	0
#define CS		1
#define RD		2
#define WR		3
#define RS		4

void LCD_init();
void LCD_Clear(uint16_t color);
void WritePage(uint8_t index);
void ShowPage(uint8_t index);
void address_set(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2);
void LCD_WR_DATA(uint16_t data);

#endif /* MD050CD_H_ */
