/*
 * main.c
 *
 *  Created on: 08.10.2014
 *      Author: googy_000
 * An 3.3V und GND 2200uF Cap
 * für daten wurden 2 portexpander benutzt
 */
#include <avr/io.h>
#include <util/delay.h>
#include "MD050CD.h"

//void line(int x0, int y0, int x1, int y1) {
//	int dx =  abs(x1-x0), sx = x0<x1 ? 1 : -1;
//	int dy = -abs(y1-y0), sy = y0<y1 ? 1 : -1;
//	int err = dx+dy, e2; /* error value e_xy */
//
//	for(;;){  /* loop */
//		address_set(x0, y0, x0, y0);
//		LCD_WR_DATA(0xF800);
//		if (x0==x1 && y0==y1) break;
//		e2 = 2*err;
//		if (e2 > dy) { err += dy; x0 += sx; } /* e_xy+e_x > 0 */
//		if (e2 < dx) { err += dx; y0 += sy; } /* e_xy+e_y < 0 */
//	}
//}

#define BLACK	0b0000000000000000
#define RED		0b1111100000000000
#define GREEN	0b0000011111100000
#define BLUE	0b0000000000011111
#define YELLOW	0b1111111111100000
#define PINK	0b1111100000011111
#define WHITE	0b1111111111111111

uint8_t zero[] =	{0b00111011, 0b11101011, 0b10111101, 0b1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

uint16_t rgb2color(uint8_t r, uint8_t g, uint8_t b) {
	return 0;
}

int main() {
	LCD_init();

	address_set(0, 0, 100, 100);
	for (uint16_t i = 0; i <= 1000; i++) {
		LCD_WR_DATA(0xFFFF);
	}
	address_set(0, 0, 4, 8);
	for (uint32_t z = 0; z <= (4 * 7); z++) {
		if (zero[z / 8] & (1 << z % 8)) {
			LCD_WR_DATA(0xFFFF);
		} else {
			LCD_WR_DATA(0x0);
		}
	}

//	ShowPage(0);
//	WritePage(0);
//	LCD_Clear(RED);
//
//	ShowPage(1);
//	WritePage(1);
//	LCD_Clear(GREEN);
//
//	ShowPage(2);
//	WritePage(2);
//	LCD_Clear(BLUE);
//
//	ShowPage(3);
//	WritePage(3);
//	LCD_Clear(WHITE);
//
//	ShowPage(4);
//	WritePage(4);
//	LCD_Clear(BLACK);
//
//	ShowPage(5);
//	WritePage(5);
//	LCD_Clear(LILA);
//
//	ShowPage(6);
//	WritePage(6);
//	LCD_Clear(BLUE | GREEN);
//
//	ShowPage(7);
//	WritePage(7);
//	LCD_Clear(YELLOW);

//	ShowPage(0);
//	LCD_Clear(0xFFFF);

//	uint16_t color = 0;

	while(1) {
		//LCD_Clear(color);
		//color++;
//
//		x++;
//		if (x > 7) {
//			x = 0;
//			y++;
//			if (y >= 5) {
//				x = 0;
//				y = 0;
//			}
//		}
//		line(10, 10, 100, 100);

//		LCD_Clear(0x0000);
	}
	return 0;
}
