/*
 * MD050SD.c
 *
 *  Created on: 09.10.2014
 *      Author: googy_000
 */
#include <avr/io.h>
#include <util/delay.h>
#include "MD050CD.h"
#include "shift.h"

uint8_t command = 0;

void LCD_Write_Bus(uint16_t data) {
	shift_set(command, data);
	command &= ~(1 << WR);
	shift_set(command, data);
	command |= (1 << WR);
	shift_set(command, data);
}

void LCD_WR_REG(uint16_t data) {
	command &= ~(1 << RS);
	LCD_Write_Bus(data);
}

void LCD_WR_DATA(uint16_t data) {
	command |= (1 << RS);
	LCD_Write_Bus(data);
}

void address_set(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2) {
	LCD_WR_REG(0x02);	// begin y
	LCD_WR_DATA(y1);
	LCD_WR_REG(0x03);	// begin x
	LCD_WR_DATA(x1);
	LCD_WR_REG(0x06);	// stop y
	LCD_WR_DATA(y2);
	LCD_WR_REG(0x07);	// stop x
	LCD_WR_DATA(x2);
	LCD_WR_REG(0x0f);
}

void LCD_init() {
	shift_init();
	command |= (1 << RD) | (1 << WR);
	command &= ~(1 << REST);
	shift_set(command, 0);
	_delay_ms(10);
	command |= (1 << REST);
	shift_set(command, 0);
	_delay_ms(10);
	command &= ~(1 << CS);
	shift_set(command, 0);

	// Hintergrundbeleuchtung einschalten
	LCD_WR_REG(0x01);
	LCD_WR_DATA(3);
}

void LCD_Clear(uint16_t color) {
	uint16_t i,j;
	address_set(0, 0, 799, 479);
	for (i = 0; i < 800; i++) {
		for (j = 0; j < 480; j++) {
			LCD_WR_DATA(color);
		}
	}
}

void WritePage(uint8_t index) {
	LCD_WR_REG(0x05);
	LCD_WR_DATA(index);
}

void ShowPage(uint8_t index) {
	LCD_WR_REG(0x04);
	LCD_WR_DATA(index);
}
