import socket
import struct
import sys

# CAN frame packing/unpacking (see `struct can_frame` in <linux/can.h>)
can_frame_fmt = "<IB3x8s"
# dieser String wird dazu verwendet die CAN Nachricht in ein struct zu packen, bin mir nicht sicher inwiefern man dies benötigt. Ich gehe davon aus,
# dass dies nicht nötig ist. Dies gilt nich aus zu probieren

#def build_can_frame(can_id, data):
#        can_dlc = len(data)
#        data = data.ljust(8, b'\x00')
#        return struct.pack(can_frame_fmt, can_id, can_dlc, data)

def dissect_can_frame(frame):
        can_id, can_dlc, data = struct.unpack(can_frame_fmt, frame)
        return (can_id, can_dlc, data[:can_dlc])
        # [_can_dlc]: der puffer fuer die Daten ist 8 byte lang,
        # da je nach DLC nicht alle diese bytes Daten darstellen,
        # werden diese damit abgeschnitten

# create a raw socket and bind it to the given CAN interface
sock = socket.socket(socket.AF_CAN, socket.SOCK_RAW, socket.CAN_RAW)

try:
    sock.bind(("can0",))	# muss ein tupel sein
except OSError:
    sys.stderr.write("Could not bind to interface '%s'\n" % "can0")
    # do something about the error...


while True:
        canframe, _ = sock.recvfrom(16)
# blockiert bis Nachricht empfangen

        (can_id, can_dlc, data) = dissect_can_frame(canframe)
#        print('Message received, ID:', hex(can_id), '  DLC:', can_dlc, '   len:', len(data))
        if can_id == 0x23:
            print(''.join(chr(i) for i in data), end='', flush=True)
        else:
            print("record")

#        print(hex(can_id), can_dlc, [hex(i) for i in data])
#        print('Received: can_id=%x, can_dlc=%x, data=%s' % dissect_can_frame(canframe))

#        try:
#                sock.send(canframe)
#        except socket.error:
#                print('Error sending CAN frame')

#        try:
#                sock.send(build_can_frame(0x01, b'\x01\x02\x03'))
#        except socket.error:
#                print('Error sending CAN frame')


# receive keyboard interrupt and do sock.shutdown()  sock.close()
