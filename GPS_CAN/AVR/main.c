/*
 * main.c
 *
 *  Created on: 29.04.2018
 *      Author: googy
 */
#include <avr/io.h>
#include <stdlib.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdio.h>
#include <inttypes.h>

#include "uart.h"
#include "mcp2515.h"


//$GPVTG,127.78,T,,M,1.967,N,3.642,K,A*3C
//$GPGGA,065518.00,5012.84016,N,00658.29032,E,1,06,1.80,437.5,M,47.0,M,,*56
//$GPSA,A,3,30,07,05,28,13,08,,,,,,,2.75,1.80,2.07*0F
//$GPGSV,3,1,10,05,41,200,24,07,18,065,19,08,11,037,24,13,72,300,16*72
//$GPSV,3,2,10,15,37,295,18,20,22,313,15,21,09,311,,24,04,249,*7D
//$GPGSV,3,3,10,28,55,106,11,30,45,065,34*73
//$GPGLL,5012.84016,N,00658.29032,E,065518.00,A,A*6A
//$GPRMC,065519.00,A,5012.84015,N,00658.29048,E,1.478,,010518,,,A*7B
//$GPVTG,,T,,M,1.478,N,2.737,K,A*28
//$GPGGA,065519.00,5012.84015,N,00658.29048,E,1,06,1.80,437.3,M,47.0,M,,*5F
//$GPSA,A,3,30,07,05,28,13,08,,,,,,,2.75,1.80,2.07*0F

//$GPRMC,200345.00,A,5012.85128,N,00658.28202,E,0.286,,300418,,,A*73
//$GPRMC,200346.00,A,5012.85231,N,00658.28192,E,0.554,,300418,,,A*79
//$GPRMC,200347.00,A,5012.85355,N,00658.28184,E,1.213,,300418,,,A*79
//$GPRMC,200348.00,A,5012.85415,N,00658.28158,E,1.381,,300418,,,A*7E


//$GPRMC,,V,,,,,,,,,,N*53



enum direction{
    INVALID,
    N,
    S,
    E,
    W
};

struct time {
    uint8_t hh;
    uint8_t mm;
    uint8_t ss;
};

struct date {
    uint8_t day;
    uint8_t month;
    uint8_t year;
};


struct coordinate {
    enum direction himmelsrichtung;
    uint8_t deg;
    uint8_t min;
    uint32_t dezimin;
};

struct nmea{
    struct time zeit;
    struct coordinate lat;
    struct coordinate lon;
    uint32_t speed; // m/h
    struct date datum;
    uint8_t valid;
} data;

uint8_t sentence[120];
uint8_t length = 0;
uint8_t pointer = 0;

uint8_t broadcast = 1;		// broadcast
tCAN message, rMessage;

char buf[10];

uint8_t checksum() {
    uint8_t p = 1;
    uint8_t check = 0;
    while (sentence[p] != '*' && p < 120) {
        check ^= sentence[p];
        p++;
    }
    return check;
}

uint8_t parse() {
    uint8_t i = 0;
    uint8_t delimiter[12];

    // delimiter suchen
    for (uint8_t k = 0; k < 12; k++) {
        delimiter[k] = 0;
        while (sentence[i] != ',' && i < 120) {i++;}
        delimiter[k] = i;
        i++;
    }

    // check prüfsumme
    itoa(checksum(), buf, 16);
    if (buf[0] >= 97) buf[0] -= 32;
    if (buf[1] >= 97) buf[1] -= 32;
    if (buf[0] != sentence[delimiter[11] + 3] || buf[1] != sentence[delimiter[11] + 4]) {
        return 0;
    }

    // Zeit parsen, nicht immer verfügbar
    if ((delimiter[1] - delimiter[0]) == 10) {  // Zeit wird gesendet
        data.zeit.hh = (sentence[delimiter[0] + 1] - 48) * 10;
        data.zeit.hh += sentence[delimiter[0] + 2] - 48;
        data.zeit.mm = (sentence[delimiter[0] + 3] - 48) * 10;
        data.zeit.mm += sentence[delimiter[0] + 4] - 48;
        data.zeit.ss = (sentence[delimiter[0] + 5] - 48) * 10;
        data.zeit.ss += sentence[delimiter[0] + 6] - 48;
    } else {
        data.zeit.hh = 0;
        data.zeit.mm = 0;
        data.zeit.ss = 0;
    }

    if (sentence[delimiter[1] + 1] == 'A') {
        data.valid = 1;
        // latitude
        data.lat.deg = (sentence[delimiter[2] + 1] - 48) * 10;
        data.lat.deg += sentence[delimiter[2] + 2] - 48;

        data.lat.min = (sentence[delimiter[2] + 4] - 48);
        data.lat.min += (sentence[delimiter[2] + 3] - 48) * 10;

        data.lat.dezimin = (sentence[delimiter[2] + 6] - 48) * 10000ul;
        data.lat.dezimin += (sentence[delimiter[2] + 7] - 48) * 1000;
        data.lat.dezimin += (sentence[delimiter[2] + 8] - 48) * 100;
        data.lat.dezimin += (sentence[delimiter[2] + 9] - 48) * 10;
        data.lat.dezimin += sentence[delimiter[2] + 10] - 48;

        if (sentence[delimiter[3] + 1] == 'N') {
            data.lat.himmelsrichtung = N;
        } else {
            data.lat.himmelsrichtung = S;
        }

        // longitude
        data.lon.deg = (sentence[delimiter[4] + 1] - 48) * 100;
        data.lon.deg += (sentence[delimiter[4] + 2] - 48) * 10;
        data.lon.deg += (sentence[delimiter[4] + 3] - 48);

        data.lon.min = (sentence[delimiter[4] + 4] - 48) * 10;
        data.lon.min += (sentence[delimiter[4] + 5] - 48);

        data.lon.dezimin = (sentence[delimiter[4] + 7] - 48) * 10000ul;
        data.lon.dezimin += (sentence[delimiter[4] + 8] - 48) * 1000;
        data.lon.dezimin += (sentence[delimiter[4] + 9] - 48) * 100;
        data.lon.dezimin += (sentence[delimiter[4] + 10] - 48) * 10;
        data.lon.dezimin += sentence[delimiter[4] + 11] - 48;

        if (sentence[delimiter[5] + 1] == 'E') {
            data.lon.himmelsrichtung = E;
        } else {
            data.lon.himmelsrichtung = W;
        }

        // geschwindigkeit
        if (sentence[delimiter[6] + 1] != ',') {
            data.speed = sentence[delimiter[7] - 1] - 48;
            data.speed += (sentence[delimiter[7] - 2] - 48) * 10;
            data.speed += (sentence[delimiter[7] - 3] - 48) * 100;
            if (sentence[delimiter[6] + 2] == '.') {
                data.speed += (sentence[delimiter[6] + 1] - 48) * 1000;
            } else if (sentence[delimiter[6] + 3] == '.') {
                data.speed += (sentence[delimiter[6] + 2] - 48) * 1000;
                data.speed += (sentence[delimiter[6] + 1] - 48) * 10000ul;
            } else if (sentence[delimiter[6] + 4] == '.') {
                data.speed += (sentence[delimiter[6] + 3] - 48) * 1000;
                data.speed += (sentence[delimiter[6] + 2] - 48) * 10000ul;
                data.speed += (sentence[delimiter[6] + 1] - 48) * 100000ul;
            } else {
                uart_puts("ERROR parsing knots\n\r");
            }
            data.speed = (data.speed * 1852ul) / 1000;
        }

        //direction delimiter[7] + 1

        data.datum.day = sentence[delimiter[8] + 2] - 48;
        data.datum.day += (sentence[delimiter[8] + 1] - 48) * 10;

        data.datum.month = sentence[delimiter[8] + 4] - 48;
        data.datum.month += (sentence[delimiter[8] + 3] - 48) * 10;

        data.datum.year = sentence[delimiter[8] + 6] - 48;
        data.datum.year += (sentence[delimiter[8] + 5] - 48) * 10;

        // error

        // error direction

        // type
        return 1;
    } else {
        data.valid = 0;
    }
    return 0;
}

//    $GPRMC,134719.00,A,5012.86418,N,00658.27676,E,2.147,,010518,,,A*78
//    78
//    $GPRMC,134720.00,A,5012.86277,N,00658.27568,E,2.505,,010518,,,A*73
//    73
//    $GPRMC,134729.00,A,5012.84239,N,00658.27758,E,3.723,169.54,010518,,,A*65
//    65
//    $GPRMC,134730.00,A,5012.83718,N,00658.27807,E,4.716,151.57,010518,,,A*60
//    60


void can_puts(char *s) {
    tCAN m;
    m.id = 0x20;
    m.header.rtr = 0;
    uint8_t i = 0;
    while (*s) { /* so lange *s != '\0' also ungleich dem "String-Endezeichen(Terminator)" */
        m.data[i] = *s;
        i++;
        if (i == 8) {
            m.header.length = 8;
            // send message
            if (mcp2515_send_message(&m)) {
                //uart_puts("Nachricht konnte gesendet werden\n\r");
            } else {
                //uart_puts("ERROR Nachricht konnte nicht gesendet werden\n\r");
            }
            i = 0;
        }
        s++;
    }
    if (i > 0) {
        m.header.length = i;
        // send message
        if (mcp2515_send_message(&m)) {
        //uart_puts("Nachricht konnte gesendet werden\n\r");
        } else {
        //uart_puts("ERROR Nachricht konnte nicht gesendet werden\n\r");
        }
    }
}

int main() {
    uart_init();
    sei();	// für MCP Interrupts
    DDRB |= 1 << PB2;		// !!!!!!!!!!!!!!!!!!!!! SS to output, sonst keine SPI Kommunikation

    /* NiRen MCP2515_CAN modul von alice aus China
     * Reset per pullup an VCC
     * 5V
     * CS an B1
     * INT an D2
     * 8MHz
     * 125kbps
     */

    // Versuche den MCP2515 zu initilaisieren
    if (!mcp2515_init()) {
        uart_puts("MCP2515 FAIL\n\r");
        return 0;
    } else {
        uart_puts("MCP2515 OK\n\r");
    }

    // init broadcast message
    message.id = 0x23;
    message.header.rtr = 0;
    message.header.length = 8;
    message.data[0] = '0';
    message.data[1] = 'x';
    message.data[2] = 0x0;
    message.data[3] = 0x0;
    message.data[4] = 0x0;
    message.data[5] = 0x0;
    message.data[6] = 0x0;
    message.data[7] = 0x0;


    while (1) {
        uint8_t digit = uart_receive();

//        // time/date
//        message.id = 0x20;
//        message.header.rtr = 0;
//        message.header.length = 1;
//        message.data[0] = digit;
//        // send message
//        if (mcp2515_send_message(&message)) {
//            //uart_puts("Nachricht konnte gesendet werden\n\r");
//        } else {
//            //uart_puts("ERROR Nachricht konnte nicht gesendet werden\n\r");
//        }

		if (digit == '$') {
			pointer = 0;
            can_puts((char*)&sentence[0]); //TODO differ in signedness
            can_puts("\n\r");
		} else {
			if (digit == '\r' && sentence[3] == 'R' && sentence[4] == 'M' && sentence[5] == 'C') {
				sentence[pointer] = 0;
				//uart_puts((char*)&sentence[0]); //TODO differ in signedness
				//uart_puts("\n\r");



/*				parse();

                // time/date
                message.id = 0x10;
                message.header.rtr = 0;
                message.header.length = 8;
                message.data[0] = data.zeit.ss;
                message.data[1] = data.zeit.mm;
                message.data[2] = data.zeit.hh;
                message.data[3] = data.datum.day;
                message.data[4] = data.datum.month;
                message.data[5] = data.datum.year;
                message.data[6] = 0x0;
                message.data[7] = 0x0;
                // send message
                if (mcp2515_send_message(&message)) {
                    //uart_puts("Nachricht konnte gesendet werden\n\r");
                } else {
                    //uart_puts("ERROR Nachricht konnte nicht gesendet werden\n\r");
                }

                // latitude
                message.id = 0x11;
                message.header.rtr = 0;
                message.header.length = 8;
                message.data[0] = data.lat.deg;
                message.data[1] = data.lat.min;
                message.data[2] = data.lat.dezimin & 0xFF;
                message.data[3] = (data.lat.dezimin >> 8) & 0xFF;
                message.data[4] = (data.lat.dezimin >> 16) & 0xFF;
                message.data[5] = (data.lat.dezimin >> 24) & 0xFF;
                message.data[6] = data.lat.himmelsrichtung;
                message.data[7] = data.valid;;
                //////////////////////////////////////////////////////send_CAN_message////////////////////////////
                // send message
                if (mcp2515_send_message(&message)) {
                    //uart_puts("Nachricht konnte gesendet werden\n\r");
                } else {
                    //uart_puts("ERROR Nachricht konnte nicht gesendet werden\n\r");
                }

                // longitude
                message.id = 0x12;
                message.header.rtr = 0;
                message.header.length = 8;
                message.data[0] = data.lon.deg;
                message.data[1] = data.lon.min;
                message.data[2] = data.lon.dezimin & 0xFF;
                message.data[3] = (data.lon.dezimin >> 8) & 0xFF;
                message.data[4] = (data.lon.dezimin >> 16) & 0xFF;
                message.data[5] = (data.lon.dezimin >> 24) & 0xFF;
                message.data[6] = data.lon.himmelsrichtung;
                message.data[7] = data.valid;
                //////////////////////////////////////////////////////send_CAN_message////////////////////////////
                // send message
                if (mcp2515_send_message(&message)) {
                    //uart_puts("Nachricht konnte gesendet werden\n\r");
                } else {
                    //uart_puts("ERROR Nachricht konnte nicht gesendet werden\n\r");
                }

                // speed
                message.id = 0x13;
                message.header.rtr = 0;
                message.header.length = 8;
                message.data[0] = data.speed & 0xFF;
                message.data[1] = (data.speed >> 8) & 0xFF;
                message.data[2] = (data.speed >> 16) & 0xFF;
                message.data[3] = (data.speed >> 24) & 0xFF;
                message.data[4] = 0x0;
                message.data[5] = 0x0;
                message.data[6] = 0x0;
                message.data[7] = data.valid;
                if (mcp2515_send_message(&message)) {} else {}
*/
            }
        }
		sentence[pointer] = digit;
		pointer++;


//////////////////////////////////////////////////////send_CAN_message////////////////////////////
/*		// send message
		if (mcp2515_send_message(&message)) {
			//uart_puts("Nachricht konnte gesendet werden\n\r");
		} else {
			//uart_puts("ERROR Nachricht konnte nicht gesendet werden\n\r");
		}
*/

//////////////////////////////////////////////////////receive_CAM_message/////////////////////////
/*		// check for incoming messages
		if (mcp2515_check_message()) {		// Nachricht vorhanden
			if (mcp2515_get_message(&rMessage)) {		// Nachricht erfolgreich angekommen
				if (rMessage.header.rtr) {
					// send data
				}

//				if (id = x) {		// set broadcast
//					broadcast = data;
//				}
				uart_puts("ID: ");
				itoa((message.id), buf, 16);
				uart_puts(buf);
				uart_puts("   DLC: ");
				itoa((message.header.length), buf, 10);
				uart_puts(buf);
				uart_puts("   Data: ");
				for (uint8_t i = 0; i < message.header.length; i++) {
					itoa((message.data[i]), buf, 16);
					uart_puts(buf);
					uart_putc(' ');
				}
				uart_puts("\n\r");
			}
			else {
				uart_puts("FAIL\n\r");
			}
		}*/
	}

	return 0;
}


////////////////////////////////////////////////////////////////////minimal/Parser/////////////////////////
/*http://www.technoblogy.com/show?SJ0
// Example: $GPRMC,194509.000,A,4042.6142,N,07400.4168,W,2.03,5.84,160412,,,A*77
char fmt[]="$GPRMC,dddtdd.ddm,A,eeae.eeee,l,eeeae.eeee,o,djdk,djdc,dddy??,,,?*??";

int state = 0;
unsigned int temp;
long ltmp;

// GPS variables
volatile unsigned int Time, Msecs, Knots, Course, Date;
volatile long Lat, Long;
volatile boolean Fix;

void ParseGPS (char c) {
  if (c == '$') { state = 0; temp = 0; }
  char mode = fmt[state++];
  // If received character matches format string, or format is '?' - return
  if ((mode == c) || (mode == '?')) return;
  // d=decimal digit
  char d = c - '0';
  if (mode == 'd') temp = temp*10 + d;
  // e=long decimal digit
  else if (mode == 'e') ltmp = (ltmp<<3) + (ltmp<<1) + d; // ltmp = ltmp*10 + d;
  // a=angular measure
  else if (mode == 'a') ltmp = (ltmp<<2) + (ltmp<<1) + d; // ltmp = ltmp*6 + d;
  // t=Time - hhmm
  else if (mode == 't') { Time = temp*10 + d; temp = 0; }
  // m=Millisecs
  else if (mode == 'm') { Msecs = temp*10 + d; ltmp=0; }
  // l=Latitude - in minutes*1000
  else if (mode == 'l') { if (c == 'N') Lat = ltmp; else Lat = -ltmp; ltmp = 0; }
  // o=Longitude - in minutes*1000
  else if (mode == 'o') { if (c == 'E') Long = ltmp; else Long = -ltmp; temp = 0; }
   // j/k=Speed - in knots*100
  else if (mode == 'j') { if (c != '.') { temp = temp*10 + d; state--; } }
  else if (mode == 'k') { Knots = temp*10 + d; temp = 0; }
  // c=Course (Track) - in degrees*100
  else if (mode == 'c') { Course = temp*10 + d; temp = 0; }
  // y=Date - ddmm
  else if (mode == 'y') { Date = temp*10 + d ; Fix = 1; }
  else state = 0;
}
*/
