/*
 * Copyright (c) 2006-2012 by Roland Riegel <feedback@roland-riegel.de>
 *
 * This file is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include <string.h>
#include <avr/pgmspace.h>
#include <avr/sleep.h>
#include "sd_raw.h"
#include "sd_raw_config.h"
#include "uart.h"
#include <stdlib.h>
#include <util/delay.h>

uint8_t bu[512];

int main() {
	uart_init();

	while (1) {
		/* setup sd card slot */
		if (!sd_raw_init()) {
			uart_puts("\n\n\rMMC/SD initialization failed\n\r");
			continue;
		}
		uart_puts("MMC/SD initialized\n\r");

		char buf[20];

		uint16_t i;
		uint64_t j;
		for (j = 0;; j+=512) {
			uart_puts("read sector ");
			ltoa(j / 512, buf, 10);
			uart_puts(buf);
			uart_puts(":\n\r");
			sd_raw_read(j, bu, 512);
			for (i = 0; i < 512; i++) {
				itoa(bu[i], buf, 16);
				uart_puts(buf);
				uart_putc(' ');
			}
			uart_puts("\n\n\r");
		}
	}
	return 0;
}
