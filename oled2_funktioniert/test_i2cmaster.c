/****************************************************************************
Title:    Access serial EEPROM 24C02 using I2C interace
Author:   Peter Fleury <pfleury@gmx.ch> http://jump.to/fleury
File:     $Id: test_i2cmaster.c,v 1.2 2003/12/06 17:07:18 peter Exp $
Software: AVR-GCC 3.3
Hardware: AT90S8515 at 4 Mhz, any AVR device can be used

Description:
    This example shows how the I2C library i2cmaster.S can be used to 
    access a serial eeprom.
    Based on Atmel Application Note AVR300, adapted to AVR-GCC C interface

*****************************************************************************/
#include <avr/io.h>
#include <avr/delay.h>
#include "i2cmaster.h"

//#define DevSSD1306  0xA2      // device address of EEPROM 24C02, see datasheet
#define DevSSD1306  0x78      // device address of SSD1306 OLED, uses 8-bit address (0x3c)!!! 


void setup_i2c(){
    // Init sequence for 128x64 OLED module
    i2c_write(0xAE);                    // Display Off
    
    i2c_write(0x00 | 0x0);            // low col = 0
    i2c_write(0x10 | 0x0);           // hi col = 0
    i2c_write(0x40 | 0x0);            // line #0

    i2c_write(0x81);                   // Set Contrast 0x81
    i2c_write(0xCF);
                                        // flips display
    i2c_write(0xA1);                    // Segremap - 0xA1
    i2c_write(0xC8);                    // COMSCAN DEC 0xC8 C0
    i2c_write(0xA6);                    // Normal Display 0xA6 (Invert A7)
    
    i2c_write(0xA4);                // DISPLAY ALL ON RESUME - 0xA4
    i2c_write(0xA8);                    // Set Multiplex 0xA8
    i2c_write(0x3F);                    // 1/64 Duty Cycle 

    i2c_write(0xD3);                    // Set Display Offset 0xD3
    i2c_write(0x0);                     // no offset

    i2c_write(0xD5);                    // Set Display Clk Div 0xD5
    i2c_write(0x80);                    // Recommneded resistor ratio 0x80

    i2c_write(0xD9);                  // Set Precharge 0xd9
    i2c_write(0xF1);

    i2c_write(0xDA);                    // Set COM Pins0xDA
    i2c_write(0x12);

    i2c_write(0xDB);                 // Set VCOM Detect - 0xDB
    i2c_write(0x40);

    i2c_write(0x20);                    // Set Memory Addressing Mode
    i2c_write(0x00);                    // 0x00 - Horizontal

    i2c_write(0x40 | 0x0);              // Set start line at line 0 - 0x40 

    i2c_write(0x8D);                    // Charge Pump -0x8D
    i2c_write(0x14); 


    i2c_write(0xA4);              //--turn on all pixels - A5. Regular mode A4
    i2c_write(0xAF);                //--turn on oled panel - AF
}


void drawBuffer(){
    i2c_write(0x21);        // column address
    i2c_write(0);           // column start address (0 = reset)
    i2c_write(127);         // column end addres (127 = reset)

    i2c_write(0x22);        // page address
    i2c_write(0);           // page start address (0 = reset);
    i2c_write(7);           // page end address

    uint8_t twbrbackup = TWBR;
    TWBR = 12;

    for (uint16_t i=0; i<((128*64)/8); i++){
        unsigned char ret = i2c_start(DevSSD1306+I2C_WRITE);   // set device address and write mode
        if ( ret ) {
            /* failed to issue start condition, possibly no device found */
            i2c_stop();
            PORTB=0xff;                            // activate all 8 LED to show error */
        }
        else {
            i2c_write(0x40);                        // set display RAM display start line register
            for (uint8_t x=0; x<16; x++){
                i2c_write(0xAA);
                i++;
            }
            i--;
            i2c_stop();
            TWBR = twbrbackup;
        }
    }
}

int main(void)
{
    unsigned char ret;

    DDRB  = 0xff;                              // use all pins on port B for output
    PORTB = 0x00;                              // (LED's low & off)

    i2c_init();                                // init I2C interface

    /* write 0x75 to eeprom address 0x05 (Byte Write) */
    ret = i2c_start(DevSSD1306+I2C_WRITE);       // set device address and write mode
    if ( ret ) {
        /* failed to issue start condition, possibly no device found */
        i2c_stop();
        PORTB=0xff;                            // activate all 8 LED to show error */
    }
    else {
    setup_i2c();
    drawBuffer();
    i2c_stop();
    }
    for(;;){
        ADCSRA |= (1 << ADSC); // Start a new conversion,
        if(ADCH < 128){
            PORTB=0x00;
        }
        else{
            PORTB=0xFF;
        }
    }
}
