/*
 * main.c
 *
 *  Created on: 02.02.2019
 *      Author: googy
 */

#include "avr/io.h"
#include "util/delay.h"

int main() {
    DDRB |= 1 << PB0;
    DDRD |= 1 << PD5;

    while (1) {
        PORTB ^= (1 << PB0);
        _delay_ms(500);
    }
    return 0;
}
