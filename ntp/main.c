#include <avr/io.h>			// Ports
#include <avr/interrupt.h>	// ISR, sli(), cli()
#include <stdlib.h>
#include <util/delay.h>
#include <avr/wdt.h>		// Watchdog
#include <avr/pgmspace.h>	// PROGMEM
#include <avr/eeprom.h>		// EEPROM
#include "main.h"
#include "lan.h"
#include "ntp.h"
#include "ow.h"
#include "messung.h"
#include "shiftregister.h"
#include "timer0.h"
#include "timer2.h"
#include "netcom.h"
#include "hk2_state_machine.h"
#include "ww_state_machine.h"
#include "uart.h"
#include "types.h"

void initialize() {
	// init Watchdog
	//wdt_enable(WDTO_2S);
	//wdt_reset();

	// init Ports
	DDRA = 0xFF;
	PORTA = 0;
	DDRB = 0xFF;
	PORTB = 0;
	DDRC = 0xFF;
	PORTC = 0;
	DDRD = 1;
	PORTD = 0xFF;

	uart_init();
	shift_init();

	// init Parameter
	HK1_soll = eeprom_read_byte((uint8_t *) EEP_HK1_SOLL);
	HK1_active = eeprom_read_byte((uint8_t *) EEP_HK1_ACTIVE);
	HK1_diff = eeprom_read_byte((uint8_t *) EEP_HK1_DIFF);
	HK1_wait = eeprom_read_byte((uint8_t *) EEP_HK1_WAIT);

	WW_soll = eeprom_read_byte((uint8_t *) EEP_WW_SOLL);
	WW_active = eeprom_read_byte((uint8_t *) EEP_WW_ACTIVE);
	WW_diff = eeprom_read_byte((uint8_t *) EEP_WW_DIFF);
	WW_wait = eeprom_read_byte((uint8_t *) EEP_WW_WAIT);

	HK2_soll = eeprom_read_byte((uint8_t *) EEP_HK2_SOLL);
	HK2_diff = eeprom_read_byte((uint8_t *) EEP_HK2_DIFF);
	ROTATION_TIME = eeprom_read_byte((uint8_t *) EEP_ROTATION_TIME);
	HK2_active = eeprom_read_byte((uint8_t *) EEP_HK2_ACTIVE);
	HK2_wait = eeprom_read_byte((uint8_t *) EEP_HK2_WAIT);

	source_soll = eeprom_read_byte((uint8_t *) EEP_ENERGY_SOURCE);


	// Z�hler f�r Uhr
	timer = 0;
	timer2_init();

	sei();

	// init ICP f�r ADC Messung
	messung_init();

	// Zeitgeber ms slots
	timer0_init();

	// Init LAN
	lan_init();
	lan_poll();

	// init OneWire
	ow_init();
	ow_init2();
}

void prog(){
	initialize();

	char buf[20];

	uint8_t ow_state = 0;

	uint32_t display_next_update = 0;
	uint32_t loctime;
	uint8_t s = 0, m = 0, h = 0;

	uint16_t erg;

	while (1) {
		//itoa(Holzkessel,buf,10);
		//uart_puts(buf);

		lan_poll();

		// Time to send NTP request?
		if (second_count >= ntp_next_update) {
			if (!ntp_request(NTP_SERVER)) {
				ntp_next_update = second_count + 2;
			} else {
				ntp_next_update = second_count + 60;
			}
		}

		// Time to refresh display?
		if ((time_offset) && (second_count >= display_next_update)) {
			display_next_update = second_count + 1;

			loctime = time_offset + second_count + 60UL * 60 * TIMEZONE;
			s = loctime % 60;
			m = (loctime / 60) % 60;
			h = (loctime / 3600) % 24;
		}

		//-------------------------------------------Sensoren-update-----------------------------------
		// 0 Kessel
		// 1 Au�enf�hler
		// 2 Brauchwasser
		// 3 HK2 anwesend
		// 4 ???
		// 5 Brenner status
		// 6 Vorlauf
		// 7 ???
		erg = 0;
		switch (messtate) {
		// Diesel-Kessel
		case 0:
			erg = messung(0);
			if (!(erg == 0xFFFF)) { // Messung abgeschlossen
				if (erg == 0xFFF0) { // Messung fehlgeschlagen, Timmer overflow
					diesel_t = 0;
					errors.diesel_t_error = TRUE;
					messtate = 2; // probiere (�ber)n�chsten Sensor
				} else { // Messung erfolgreich
					errors.diesel_t_error = FALSE;
					diesel_t = convert_mt(erg);
					messtate = 2;
				}
			}
			break;

			// Au�enf�hler (abgeschaltet)
		case 1:
			messtate = 2;
			break;

			// Warmwasser
		case 2:
			erg = messung(2);
			if (!(erg == 0xFFFF)) {
				if (erg == 0xFFF0) {
					WW_ist = 255;
					errors.wwasser_t_error = TRUE;
					messtate = 3;
				} else {
					WW_ist = convert_mt(erg);
					errors.wwasser_t_error = FALSE;
					messtate = 3;
				}
			}
			break;

			// HK2 FM241 Modul anwesend
		case 3:
			erg = messung(3);
			if (!(erg == 0xFFFF)) {
				if (erg == 0xFFF0) {
					HK2_present = FALSE;
					errors.FM241_error = TRUE;
					messtate = 5;
				} else {
					errors.FM241_error = FALSE;
					if (erg < 400) {
						HK2_present = TRUE;
					} else {
						HK2_present = FALSE;
					}
					messtate = 5;
				}
			}
			break;

			// Brenner Status
		case 5:
			erg = messung(5);
			if (!(erg == 0xFFFF)) { // Messung abgeschlossen
				if (erg == 0xFFF0) { // Messung fehlgeschlagen, Timmer overflow
					brenner_status = 0;
					messtate = 6; // probiere n�chsten Sensor
				} else { // Messung erfolgreich
					brenner_status = erg;
					messtate = 6;
				}
			}
			break;

			// HK2 Temperatur
		case 6:
			erg = messung(6);
			if (!(erg == 0xFFFF)) {// Messung abgeschlossen
				if (erg == 0xFFF0) {// Messung fehlgeschlagen, Timmer overflow
					HK2_ist = 0xFF;
					errors.hk2_t_error = TRUE;
					messtate = 0; // probiere n�chsten Sensor
				} else {// Messung erfolgreich
					errors.hk2_t_error = FALSE;
					HK2_ist = convert_mt(erg);
					messtate = 0;
				}
			}
			break;
		default:
			messtate = 0;
		}

		//-----------------OneWire-Messung----------------------------------------------
		switch (ow_state) {
		case 0: // starte Messung
			ow_start();
			ow_start2();
			ow_state = 1;
			OW_timer = 0;
			break;
		case 1: // warte bis Messung abgeschlossen ist
			if (OW_timer >= 2) {
				ow_state = 2;
			}
			break;
		case 2: // lese Sensor aus
			Holzkessel = (ow_temp_id(EEP_OW_HOLZ) >> 4);
			ow_state = 3;
			break;
		case 3:
			Speicher0 = (ow_temp_id(EEP_OW_SPEICHER_0) >> 4);
			ow_state = 4;
			break;
		case 4:
			Speicher1 = (ow_temp_id(EEP_OW_SPEICHER_1) >> 4);
			ow_state = 5;
			break;
		case 5:
			Speicher2 = (ow_temp_id(EEP_OW_SPEICHER_2) >> 4);
			ow_state = 6;
			break;
		case 6:
			Speicher3 = (ow_temp_id(EEP_OW_SPEICHER_3) >> 4);
			ow_state = 7;
			break;
		case 7:
			Speicher4 = (ow_temp_id(EEP_OW_SPEICHER_4) >> 4);
			ow_state = 8;
			break;
		case 8:
			read_ow_scratch_id2(EEP_OW_EXPERIMENT);
			arbeitsZimmer.degree = get_temp_deg2();
			arbeitsZimmer.millis = get_temp_millis2();
			ow_state = 9;
			break;
		case 9:
			read_ow_scratch_id2(EEP_OW_SCHLAFZIMMER);
			schlafZimmer.degree = get_temp_deg2();
			schlafZimmer.millis = get_temp_millis2();
			ow_state = 0;
			break;
		default:
			ow_state = 0;
		}
		//---------------------------------------------------------------------------------------------
		//-----------------state machine Heizkreis 1-----------------------------
		/*		switch (HK1_state) {
		 case 0:
		 if (HK1_active) {
		 HK1_state = 1;
		 break;
		 }
		 break;

		 case 1:
		 // Heizkreis 1 deaktiviert
		 if (!HK1_active){
		 HK1_state = 0;
		 shift &= ~(1 << HK1);
		 shift_set(shift);
		 break;
		 }

		 if (HK1_ist > (HK1_soll + HK1_diff)) {
		 shift &= ~(1 << HK1);	// zu warm, Pumpe aus
		 shift_set(shift);
		 }
		 if (HK1_ist < (HK1_soll - HK1_diff)) {
		 shift |= (1 << HK1);	// zu kalt, Pumpe an
		 shift_set(shift);
		 }
		 HK1_timer = 0;
		 HK1_state = 2;	// warten
		 break;

		 // Warten
		 case 2:
		 // Heizkreis 1 deaktiviert
		 if (!HK1_active){
		 HK1_state = 0;
		 shift &= ~(1 << HK1);
		 shift_set(shift);
		 break;
		 }
		 // timeout
		 if (HK1_timer > HK1_wait) {
		 HK1_state = 1;
		 break;
		 }
		 break;
		 default: HK1_state = 0;
		 }
		 */
		ww_state_machine();
		hk2_state_machine();

		// Energiequelle
		if ((source_ist != source_soll) || (source_turn != OFF)) {
			if (source_turn != source_soll) {
				source_turn = OFF;
			}
			if (source_turn == OFF) {
				source_turn = source_soll;
				source_timer = 0;
				PORTA &= ~((1 << PA3) | (1 << PA2));
				if (source_turn == HOLZ) {
					PORTA |= (1 << PA3);
				} else if (source_turn == HEIZOEL) {
					PORTA |= (1 << PA2);
				}
			}
			// Motor dreht, warte
			if (source_timer >= 130) {
				PORTA &= ~((1 << PA3) | (1 << PA2));
				source_ist = source_turn;
				source_turn = OFF;
			}
		}
	}
}

int main() {
	prog();
	return 0;
}
