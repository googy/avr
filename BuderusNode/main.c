/*
 * main.c
 *
 *  Created on: 29.04.2018
 *      Author: googy
 */
#include <avr/io.h>
#include <stdlib.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdio.h>
#include <inttypes.h>

#include "uart.h"
#include "mcp2515.h"




uint8_t broadcast = 1;		// broadcast
tCAN message, rMessage;

char buf[10];


void can_puts(char *s) {
    tCAN m;
    m.id = 0x20;
    m.header.rtr = 0;
    uint8_t i = 0;
    while (*s) { /* so lange *s != '\0' also ungleich dem "String-Endezeichen(Terminator)" */
        m.data[i] = *s;
        i++;
        if (i == 8) {
            m.header.length = 8;
            // send message
            if (mcp2515_send_message(&m)) {
                //uart_puts("Nachricht konnte gesendet werden\n\r");
            } else {
                //uart_puts("ERROR Nachricht konnte nicht gesendet werden\n\r");
            }
            i = 0;
        }
        s++;
    }
    if (i > 0) {
        m.header.length = i;
        // send message
        if (mcp2515_send_message(&m)) {
        //uart_puts("Nachricht konnte gesendet werden\n\r");
        } else {
        //uart_puts("ERROR Nachricht konnte nicht gesendet werden\n\r");
        }
    }
}


#define BRENNER	PD0
#define INT		PD2
#define HK1		PD3
#define WW		PD4
#define PZ		PD5
#define MW		PD6
#define MK		PD7
#define ICP		PB0
#define HK2		PB1
#define S0		PC0
#define S1		PC1
#define S2		PC2
#define T5		PC3

int main() {
    DDRD |= 1 << WW;
    while (1) {
        PORTD ^= (1 << WW);
        _delay_ms(1);
    }
    uart_init();
    sei();	// für MCP Interrupts
    DDRB |= 1 << PB2;		// !!!!!!!!!!!!!!!!!!!!! SS to output, sonst keine SPI Kommunikation

    /* NiRen MCP2515_CAN modul von alice aus China
     * Reset per pullup an VCC
     * 5V
     * CS an B1		// FIXME CS an CC
     * INT an D2
     * 8MHz
     * 125kbps
     */

    // Versuche den MCP2515 zu initilaisieren
    if (!mcp2515_init()) {
        uart_puts("MCP2515 FAIL\n\r");
        return 0;
    } else {
        uart_puts("MCP2515 OK\n\r");
    }

    // init broadcast message
    message.id = 0x23;
    message.header.rtr = 0;
    message.header.length = 8;
    message.data[0] = '0';
    message.data[1] = 'x';
    message.data[2] = 0x0;
    message.data[3] = 0x0;
    message.data[4] = 0x0;
    message.data[5] = 0x0;
    message.data[6] = 0x0;
    message.data[7] = 0x0;

    while (1) {
        uint8_t digit = uart_receive();

//        message.id = 0x20;
//        message.header.rtr = 0;
//        message.header.length = 1;
//        message.data[0] = digit;
//        // send message
//        if (mcp2515_send_message(&message)) {
//            //uart_puts("Nachricht konnte gesendet werden\n\r");
//        } else {
//            //uart_puts("ERROR Nachricht konnte nicht gesendet werden\n\r");
//        }

//            can_puts((char*)&sentence[0]); //TODO differ in signedness
//            can_puts("\n\r");

//////////////////////////////////////////////////////receive_CAM_message/////////////////////////
/*		// check for incoming messages
		if (mcp2515_check_message()) {		// Nachricht vorhanden
			if (mcp2515_get_message(&rMessage)) {		// Nachricht erfolgreich angekommen
				if (rMessage.header.rtr) {
					// send data
				}

//				if (id = x) {		// set broadcast
//					broadcast = data;
//				}
				uart_puts("ID: ");
				itoa((message.id), buf, 16);
				uart_puts(buf);
				uart_puts("   DLC: ");
				itoa((message.header.length), buf, 10);
				uart_puts(buf);
				uart_puts("   Data: ");
				for (uint8_t i = 0; i < message.header.length; i++) {
					itoa((message.data[i]), buf, 16);
					uart_puts(buf);
					uart_putc(' ');
				}
				uart_puts("\n\r");
			}
			else {
				uart_puts("FAIL\n\r");
			}
		}*/
	}
	return 0;
}
