/*
 * main.c
 *
 *  Created on: 14.10.2015
 *      Author: googy
 */

#include <avr/io.h>
#include <util/delay.h>	// delay
#include "usart.h"		// UART
#include <stdlib.h>		// itoa
#include "i2cmaster.h"	// I2C

// funktioniert
// WICHTIG pullup an SCL und SDA nicht vergessen
// erstellt f�r ATmega328P

int main() {
	i2c_init();
	USART_init();

	while(1) {
		// turn amp on
		i2c_start_wait(0xD8+I2C_WRITE);	// set device address and write mode
		i2c_write(0b00000000);				// send address to write
		i2c_write(0b00010000);					// send value
		i2c_stop();						// set stop conditon = release bus

		_delay_ms(500);

		// unmute
		i2c_start_wait(0xD8+I2C_WRITE);	// set device address and write mode
		i2c_write(0b00000100);				// send address to write
		i2c_write(0b00010000);					// send value
		i2c_stop();						// set stop conditon = release bus

		_delay_ms(2000);

		// mute
		i2c_start_wait(0xD8+I2C_WRITE);	// set device address and write mode
		i2c_write(0b00000000);				// send address to write
		i2c_write(0b00010000);					// send value
		i2c_stop();						// set stop conditon = release bus

		_delay_ms(100);

		// turn amp off
		i2c_start_wait(0xD8+I2C_WRITE);	// set device address and write mode
		i2c_write(0b00000000);				// send address to write
		i2c_write(0b00000000);					// send value
		i2c_stop();						// set stop conditon = release bus

		_delay_ms(5000);
	}
	return 0;
}
