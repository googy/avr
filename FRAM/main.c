/*
 * main.c
 *
 *  Created on: 14.10.2015
 *      Author: googy
 */

#include <avr/io.h>
#include <util/delay.h>	// delay
#include "usart.h"		// UART
#include <stdlib.h>		// itoa
#include "eeprom.h"		// eeprom

// funktioniert
// WICHTIG pullup an SCL und SDA nicht vergessen
// erstellt f�r ATmega328P
// identische Ansteuerung wie normale EEPROMS

int main() {

	eeprom_init();
	USART_init();

	char buf[10];
	uint8_t i = 0;
	while(1) {
		itoa(i, buf, 10);
		USART_SendString(buf);
		USART_SendString("  _  ");

		uint8_t v = eeprom_read(i);
		itoa(v, buf, 16);
		USART_SendString(buf);
		USART_SendByte(10);
		USART_SendByte(13);
		i++;
	}
	return 0;
}
