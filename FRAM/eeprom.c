/*
 * eeprom.c
 *
 *  Created on: 14.10.2015
 *      Author: googy
 */

#include "i2cmaster.h"

#define ADDR	0b10100000

void eeprom_init() {
	i2c_init();
}

void eeprom_write(uint8_t addr, uint8_t val) {
	i2c_start_wait(ADDR+I2C_WRITE);	// set device address and write mode
	i2c_write(addr);				// send address to write
	i2c_write(val);					// send value
	i2c_stop();						// set stop conditon = release bus
}

uint8_t eeprom_read(uint8_t addr) {
	i2c_start_wait(ADDR+I2C_WRITE);	// set device address and write mode
	i2c_write(addr);				// send address to read
	i2c_rep_start(ADDR+I2C_READ);	// set device address and read mode
	uint8_t erg = i2c_readNak();	// read one byte from EEPROM
	i2c_stop();						// set stop condition = release bus
	return erg;
}
