#ifndef USART_H_
#define USART_H_

// USART Baudrate berechnen
#define BAUD 9600UL	//Baudrate festlegen
#define UBRR_VAL ((F_CPU+BAUD*8)/(BAUD*16)-1)   // Baudratenregister berechnen
#define BAUD_REAL (F_CPU/(16*(UBRR_VAL+1)))     // Reale Baudrate
#define BAUD_ERROR ((BAUD_REAL*1000)/BAUD) // Fehler in Promille, 1000 = kein Fehler.
#if ((BAUD_ERROR<990) || (BAUD_ERROR>1010))	//Warnung falls Fehler zu gro�
#error Systematischer Fehler der Baudrate gr�sser 1% und damit zu hoch!	//Warnung
#endif

void USART_init();
void USART_SendByte(uint8_t Data);
void USART_SendString(char *s);

#endif /* USART_H_ */
