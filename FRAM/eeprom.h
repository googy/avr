/*
 * eeprom.h
 *
 *  Created on: 14.10.2015
 *      Author: googy
 */

#ifndef EEPROM_H_
#define EEPROM_H_

void eeprom_init();
void eeprom_write(uint8_t addr, uint8_t val);
uint8_t eeprom_read(uint8_t addr);

#endif /* EEPROM_H_ */
