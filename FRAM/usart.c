#include <avr/io.h>
#include "usart.h"

void USART_init() {
	//Baudrate konfigurieren
	UBRR0H = UBRR_VAL >> 8;
	UBRR0L = UBRR_VAL;
	//Senden einschalten
	UCSR0B |= (1<<TXEN0);
}

void USART_SendByte(uint8_t Data) {
	while (!(UCSR0A & (1 << UDRE0))) { //warte bis vorheriges byte gesendet ist
	}
	UDR0 = Data; //sende
}

void USART_SendString(char *s) {
	while (*s) { //mache weiter solange kein 0-byte als Abschluss der Zeichenkette
		USART_SendByte(*s); //sende aktuelle Array Zelle
		s++; //schiebe den Zeiger eine Zelle weiter
	}
}
