
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>

#include <util/delay.h>
#include <stdio.h>
#include <inttypes.h>

#include <stdlib.h>	// itoa

#include "mcp2515.h"
#include "global.h"
#include "defaults.h"

#include "uart.h"

#include "MAX31855.h"
#include "DS18B20/ow.h"


// EEPROM OneWire ROM Adressen jeweils 8 Byte
//#define EEP_OW_HOLZ			0	//{0x28, 0xFF, 0x8B, 0xAA, 0x03, 0x00, 0x00, 0xA5}	// Ausgang Holkessel
//#define EEP_OW_SPEICHER_0	8	//{0x28, 0xFA, 0x70, 0xAA, 0x03, 0x00, 0x00, 0x98}	// Pufferspeicher oben
//#define EEP_OW_SPEICHER_1	16	//{0x28, 0x68, 0xAC, 0xAA, 0x03, 0x00, 0x00, 0x38}	// Pufferspeicher 1. Anschluss (oben)
//#define EEP_OW_SPEICHER_2	24	//{0x28, 0x19, 0x98, 0xAA, 0x03, 0x00, 0x00, 0x0C}	// Pufferspeicher 2. Anschluss
//#define EEP_OW_SPEICHER_3	32	//{0x28, 0xF2, 0xAC, 0xAA, 0x03, 0x00, 0x00, 0x46}	// Pufferspeicher 3. Anschluss
//#define EEP_OW_SPEICHER_4	40	//{0x28, 0x86, 0x6D, 0xAA, 0x03, 0x00, 0x00, 0xED}	// Pufferspeicher 4. Anschluss (unten)
//#define EEP_OW_EXPERIMENT	48	//{0x28, 0x6A, 0x63, 0xAA, 0x03, 0x00, 0x00, 0x1B}	// Testsensor Zimmer
//#define EEP_OW_SCHLAFZIMMER	56	//{0x28, 0x6D, 0x5F, 0xAA, 0x03, 0x00, 0x00, 0x3B}	// Schlafzimmer
//#define EEP_OW_NOT_IN_EEP2	64	//{0x28, 0x8F, 0x87, 0xAA, 0x03, 0x00, 0x00, 0x1C}	// nicht angeschlossen
//#define EEP_OW_NOT_IN_EEP3	72
//{0x28, 0x94, 0x80, 0xAA, 0x03, 0x00, 0x00, 0xEE defekt ausgebaut}


uint8_t broadcast = 1;		// broadcast
tCAN message, rMessage;

char buf[10];

int main(void) {
	uart_init();
	ow_init();

	sei();	// f�r MCP Interrupts

	DDRB |= 1 << PB2;		// !!!!!!!!!!!!!!!!!!!!! SS to output, sonst keine SPI Kommunikation

	/* NiRen MCP2515_CAN modul von alice aus China
	 * Reset per pullup an VCC
	 * 5V
	 * CS an B1
	 * INT an D2
	 * 8MHz
	 * 125kbps
	 */



	// Versuche den MCP2515 zu initilaisieren
	if (!mcp2515_init()) {
		uart_puts("MCP2515 FAIL\n\r");
		return 0;
	} else {
		uart_puts("MCP2515 OK\n\r");
	}

	// init broadcast message
	message.id = 0x22;
	message.header.rtr = 0;
	message.header.length = 8;
	message.data[0] = 0x0;
	message.data[1] = 0x0;
	message.data[2] = 0x0;
	message.data[3] = 0x0;
	message.data[4] = 0x0;
	message.data[5] = 0x0;
	message.data[6] = 0x0;
	message.data[7] = 0x0;

	max31855initSW();		// init Thermocouple
	_delay_ms(500);

	while (1) {
		_delay_ms(1000);


		//ow_read_single_id(id);
		uint8_t kessel_id[8] = {0x28, 0xae, 0x67, 0xaa, 0x3, 0x0, 0x0, 0x47};
		uint16_t kessel = ow_temp_id(kessel_id);
		uint8_t speicher0_id[8] = {0x28, 0xFA, 0x70, 0xAA, 0x03, 0x00, 0x00, 0x98};
		uint16_t speicher0 = ow_temp_id(speicher0_id);
		uint8_t speicher1_id[8] = {0x28, 0x68, 0xAC, 0xAA, 0x03, 0x00, 0x00, 0x38};
		uint16_t speicher1 = ow_temp_id(speicher1_id);

		uint8_t speicher2_id[8] = {0x28, 0x19, 0x98, 0xAA, 0x03, 0x00, 0x00, 0x0C};
		uint16_t speicher2 = ow_temp_id(speicher2_id);
		uint8_t speicher3_id[8] = {0x28, 0xF2, 0xAC, 0xAA, 0x03, 0x00, 0x00, 0x46};
		uint16_t speicher3 = ow_temp_id(speicher3_id);
		uint8_t speicher4_id[8] = {0x28, 0x86, 0x6D, 0xAA, 0x03, 0x00, 0x00, 0xED};
		uint16_t speicher4 = ow_temp_id(speicher4_id);

		ow_start();

		readTempSW();		// Thermocouple using SoftwareSPI

		itoa(max31855.intern, buf, 10);
		uart_puts("    intern: ");
		uart_puts(buf);
		uart_puts("    Abgastemperatur: ");
		itoa(max31855.sensor, buf, 10);
		uart_puts(buf);
		uart_puts("\n\r");

		// aktives Versenden der Werte aktiviert
		if (broadcast) {

			// prepare message - Abgastemperatur
			message.id = 0x22;
			message.header.rtr = 0;
			message.header.length = 8;
			message.data[0] = max31855.intern;
			message.data[1] = (uint8_t) (max31855.sensor & 0xFF);
			message.data[2] = max31855.sensor >> 8;
			message.data[3] = max31855.fault;
			message.data[4] = max31855.err;
			message.data[5] = 0;
			message.data[6] = 0;
			message.data[7] = 0;

			// send message
			if (mcp2515_send_message(&message)) {
				//uart_puts("Nachricht konnte gesendet werden\n\r");
			} else {
				//uart_puts("ERROR Nachricht konnte nicht gesendet werden\n\r");
			}

			_delay_ms(500);

			// Kesseltemperatur
			message.id = 0x23;
			message.header.rtr = 0;
			message.header.length = 8;
			message.data[0] = kessel >> 4;
			message.data[1] = 0;
			message.data[2] = 0;
			message.data[3] = 0;
			message.data[4] = 0;
			message.data[5] = 0;
			message.data[6] = 0;
			message.data[7] = 0;

			uart_puts("Kessel: ");
			itoa(message.data[0], buf, 10);
			uart_puts(buf);

			// send message
			if (mcp2515_send_message(&message)) {
				//uart_puts("Nachricht konnte gesendet werden\n\r");
				uart_puts("OK");
				uart_puts("\n\r");
			} else {
				//uart_puts("ERROR Nachricht konnte nicht gesendet werden\n\r");
				uart_puts("NOPE");
				uart_puts("\n\r");
			}


			_delay_ms(500);
			// Speicher0
			message.id = 0x24;
			message.header.rtr = 0;
			message.header.length = 8;
			message.data[0] = speicher0 >> 4;
			message.data[1] = 0;
			message.data[2] = 0;
			message.data[3] = 0;
			message.data[4] = 0;
			message.data[5] = 0;
			message.data[6] = 0;
			message.data[7] = 0;

			uart_puts("Speicher0: ");
			itoa(message.data[0], buf, 10);
			uart_puts(buf);

			// send message
			if (mcp2515_send_message(&message)) {
				//uart_puts("Nachricht konnte gesendet werden\n\r");
				uart_puts("OK");
				uart_puts("\n\r");
			} else {
				//uart_puts("ERROR Nachricht konnte nicht gesendet werden\n\r");
				uart_puts("NOPE");
				uart_puts("\n\r");
			}


			_delay_ms(500);
			// Speicher1
			message.id = 0x25;
			message.header.rtr = 0;
			message.header.length = 8;
			message.data[0] = speicher1 >> 4;
			message.data[1] = 0;
			message.data[2] = 0;
			message.data[3] = 0;
			message.data[4] = 0;
			message.data[5] = 0;
			message.data[6] = 0;
			message.data[7] = 0;

			uart_puts("Speicher1: ");
			itoa(message.data[0], buf, 10);
			uart_puts(buf);

			// send message
			if (mcp2515_send_message(&message)) {
				//uart_puts("Nachricht konnte gesendet werden\n\r");
				uart_puts("OK");
				uart_puts("\n\r");
			} else {
				//uart_puts("ERROR Nachricht konnte nicht gesendet werden\n\r");
				uart_puts("NOPE");
				uart_puts("\n\r");
			}


			_delay_ms(500);
			// Speicher2
			message.id = 0x26;
			message.header.rtr = 0;
			message.header.length = 8;
			message.data[0] = speicher2 >> 4;
			message.data[1] = 0;
			message.data[2] = 0;
			message.data[3] = 0;
			message.data[4] = 0;
			message.data[5] = 0;
			message.data[6] = 0;
			message.data[7] = 0;

			uart_puts("Speicher2: ");
			itoa(message.data[0], buf, 10);
			uart_puts(buf);

			// send message
			if (mcp2515_send_message(&message)) {
				//uart_puts("Nachricht konnte gesendet werden\n\r");
				uart_puts("OK");
				uart_puts("\n\r");
			} else {
				//uart_puts("ERROR Nachricht konnte nicht gesendet werden\n\r");
				uart_puts("NOPE");
				uart_puts("\n\r");
			}


			_delay_ms(500);
			// Speicher3
			message.id = 0x27;
			message.header.rtr = 0;
			message.header.length = 8;
			message.data[0] = speicher3 >> 4;
			message.data[1] = 0;
			message.data[2] = 0;
			message.data[3] = 0;
			message.data[4] = 0;
			message.data[5] = 0;
			message.data[6] = 0;
			message.data[7] = 0;

			uart_puts("Speicher3: ");
			itoa(message.data[0], buf, 10);
			uart_puts(buf);

			// send message
			if (mcp2515_send_message(&message)) {
				//uart_puts("Nachricht konnte gesendet werden\n\r");
				uart_puts("OK");
				uart_puts("\n\r");
			} else {
				//uart_puts("ERROR Nachricht konnte nicht gesendet werden\n\r");
				uart_puts("NOPE");
				uart_puts("\n\r");
			}


			_delay_ms(500);
			// Speicher4
			message.id = 0x28;
			message.header.rtr = 0;
			message.header.length = 8;
			message.data[0] = speicher4 >> 4;
			message.data[1] = 0;
			message.data[2] = 0;
			message.data[3] = 0;
			message.data[4] = 0;
			message.data[5] = 0;
			message.data[6] = 0;
			message.data[7] = 0;

			uart_puts("Speicher4: ");
			itoa(message.data[0], buf, 10);
			uart_puts(buf);

			// send message
			if (mcp2515_send_message(&message)) {
				//uart_puts("Nachricht konnte gesendet werden\n\r");
				uart_puts("OK");
				uart_puts("\n\r");
			} else {
				//uart_puts("ERROR Nachricht konnte nicht gesendet werden\n\r");
				uart_puts("NOPE");
				uart_puts("\n\r");
			}
		}

		// check for incoming messages
		if (mcp2515_check_message()) {		// Nachricht vorhanden
			if (mcp2515_get_message(&rMessage)) {		// Nachricht erfolgreich angekommen
				if (rMessage.header.rtr) {
					// send data
				}

//				if (id = x) {		// set broadcast
//					broadcast = data;
//				}
				uart_puts("ID: ");
				itoa((message.id), buf, 16);
				uart_puts(buf);
				uart_puts("   DLC: ");
				itoa((message.header.length), buf, 10);
				uart_puts(buf);
				uart_puts("   Data: ");
				for (uint8_t i = 0; i < message.header.length; i++) {
					itoa((message.data[i]), buf, 16);
					uart_puts(buf);
					uart_putc(' ');
				}
				uart_puts("\n\r");
			}
			else {
				uart_puts("FAIL\n\r");
			}
		}
	}

	return 0;
}
