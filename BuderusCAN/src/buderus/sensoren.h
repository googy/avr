/*
 * sensoren.h
 *
 *  Created on: 14.04.2019
 *      Author: googy
 */

#ifndef SRC_BUDERUS_SENSOREN_H_
#define SRC_BUDERUS_SENSOREN_H_

struct {
    uint8_t kesseltemperatur;   // channel 0
    uint8_t aussenfuehler;      // channel 1
    uint8_t brauchwasser;       // channel 2
    uint8_t HK2modul;           // channel 3
    // channel 4
    uint8_t brenner;            // channel 5
    uint8_t vorlauf_hk2;        // channel 6
    // channel 7
} values;

void buderus_sensoren();

#endif /* SRC_BUDERUS_SENSOREN_H_ */
