/*
 * messung.h
 *
 *  Created on: 13.04.2019
 *      Author: googy
 */

#ifndef SRC_BUDERUS_MESSUNG_H_
#define SRC_BUDERUS_MESSUNG_H_

#include <avr/io.h>

#define ICP     PB0
#define S0      PC0
#define S1      PC1
#define S2      PC2
#define T5      PC3

// TODO convert to enum
#define INIT    0x00
#define OK      0xFF
#define ERROR   0x0F


uint8_t messung_done();
uint8_t messung_t_erg();
void messung_init(uint8_t channel);
void messung_compute();

#endif /* SRC_BUDERUS_MESSUNG_H_ */
