/*
 * main.c
 *
 *  Created on: 29.04.2018
 *      Author: googy
 */
#include <avr/io.h>         // AVR Register, GPIO
#include <avr/interrupt.h>  // sei() Interrupt enable
#include <util/delay.h>     // _delay_ms()
#include <stdlib.h>         // itoa()   int to ascii

#include "uart.h"
#include "mcp2515.h"
#include "timebase.h"
#include "buderus/messung.h"
#include "buderus/sensoren.h"


uint8_t broadcast = 1;		// broadcast
ttCAN message, rMessage;

char buf[10];

#define BRENNER PD0
#define INT     PD2
#define HK1     PD3
#define WW      PD4
#define PZ      PD5
#define MW      PD6
#define MK      PD7
#define ICP     PB0
#define HK2     PB1
#define S0      PC0
#define S1      PC1
#define S2      PC2
#define T5      PC3


//------------------------------------------------------------------------------------------------------------------------------
static uint8_t ww_state = 0;
//uint8_t ww_on = 1;  // eingeschaltet, erlaubnis aktiv zu werden
uint8_t ww_active = 0;  // aktiv, wenn vorbedingungen erfüllt
uint8_t ww_soll = 60;
uint8_t ww_diff = 3;
uint8_t speicher1 = 70;
uint32_t ww_counter = 0;

uint8_t buderus_fire = 0;       // zeigt die Wärmeanfrage

enum sources_t{
    HOLZ,               // Atmos Holzkessel
    HEIZOEL,            // Buderus Heiz�lkessel
    UNBEKANNT,          // aktuelle Position nicht bekannt
    TURNING             // Ventil befindet sich in Bewegung
};
enum sources_t source_ist = HEIZOEL;

void ww_state_machine() {
    //-----------------state machine Warmwasser------------------------------
    switch (ww_state) {
    // Idle state, Warmwasser deaktiviert
    case 0:
        if (ww_active) {
            ww_state = 1;   // Warmwasser wurde aktiviert
            uart_puts("state 0,  WW aktiviert\n\r");
        }
        break;
    //
    case 1:
        if (!ww_active) {     // Warmwasser wurde deaktiviert, gehe in den idle Zustand
            ww_state = 0;
            // TODO Pumpe sollte hier nicht an sein, aber reset Pumpe
            uart_puts("State 1 WW deaktiviert\n\r");
            break;
        }
        if (source_ist == HOLZ) {
            ww_state = 2;
            uart_puts("State 1 Quelle ist HOLZ\n\r");
            break;
        }
        if (source_ist == HEIZOEL) {
            uart_puts("State 1 Quelle ist HEIZOEL\n\r");
            ww_state = 5;
            break;
        }
        break;
    // State behandelt die Wärmequelle Holz
    case 2:
        if (!ww_active) {
            ww_state = 0;
            uart_puts("State 2 WW deaktiviert\n\r");
            break;
        }
        if (source_ist != HOLZ) {  // Quelle nicht mehr Holz
            ww_state = 1;
            uart_puts("State 2 Quelle nicht mehr HOLZ\n\r");
            break;
        }
        if (values.brauchwasser < (ww_soll - 3)) {   // nicht auf Maximaltemperatur
            ww_state = 3;
            uart_puts("State 2 WW nicht soll, mehr als 3 Grad weniger\n\r");
            break;
        }
        break;
    // nutze Wärmequelle Holz
    case 3:
        if (!ww_active) { // Warmwasser deaktiviert
            PORTD &= ~(1 << WW);
            ww_state = 0;
            uart_puts("state 3 WW deaktiviert\n\r");
            break;
        }
        if (source_ist != HOLZ) {  // Quelle nicht mehr Holz
            PORTD &= ~(1 << WW);
            ww_state = 1;
            uart_puts("state 3 Quelle nicht mehr HOLZ\n\r");
            break;
        }
        if (values.brauchwasser >= ww_soll) {    // Solltemperatur erreicht
            PORTD &= ~(1 << WW);
            ww_state = 2;
            uart_puts("state 3 WW fertig aufgewaermt\n\r");
            break;
        }
        if ((speicher1 - 6) > values.brauchwasser) { // genug W�rme vorhanden
            PORTD |= (1 << WW);
            ww_state = 4;
            stoptimer_init(&ww_counter);
            uart_puts("state 3 genug im Speicher\n\r");
            break;
        } else {
            PORTD &= ~(1 << WW);
            ww_state = 4;
            stoptimer_init(&ww_counter);
            uart_puts("state 3 Speicher zu kalt\n\r");
            break;
        }
        break;
    // waiting state
    case 4:
        if (stoptimer_expired(&ww_counter, 5000)) {
            ww_state = 3;
            uart_puts("State 4 fertig gewartet 5s\n\r");
            break;
        }
        break;
    case 5: // Heizoel
        if (!ww_active) {
            ww_state = 0;
            uart_puts("state 5 WW deaktiviert\n\r");
            break;
        }
        if (source_ist != HEIZOEL) {
            ww_state = 1;
            uart_puts("state 5 Quelle nicht mehr HEIZOEL\n\r");
            break;
        }
        if (values.brauchwasser < (ww_soll - ww_diff)) {   // nicht auf Maximaltemperatur
            ww_state = 6;
            buderus_fire = 1;  // schalte Brenner hinzu
            uart_puts("state 5 soll nicht erreicht\n\r");
            break;
        }
        break;
    case 6:
        if (!ww_active) {
            ww_state = 0;
            PORTD &= ~(1 << WW);                // Pumpe abschalten
            buderus_fire = 0;      // Brenner abschalten
            uart_puts("State 6 WW deaktiviert, schalte Brenner ab\n\r");
            break;
        }
        if (source_ist != HEIZOEL) {
            ww_state = 1;
            PORTD &= ~(1 << WW);                // Pumpe abschalten
            buderus_fire = 0;      // Brenner abschalten
            uart_puts("State 6 Quelle nicht mehr HEIZOEL\n\r");
            break;
        }
        if ((values.brauchwasser + 5) < values.kesseltemperatur) {
            PORTD |= (1 << WW);                 // Pumpe einschalten
        }
        if (values.brauchwasser >= ww_soll) {
            buderus_fire = 0;
            ww_state = 5;
            PORTD &= ~(1 << WW);                // Pumpe abschalten
            uart_puts("State 6 Soll erreicht\n\r");
        }
        break;
    default:
        ww_state = 0;
    }

    if (buderus_fire) {
        PORTD |= (1 << PD0);
    } else {
        PORTD &= ~(1 << PD0);
    }
}

//------------------------------------------------------------------------------------------------------------------------------


// stoptimer_init(&hk2_timecounter);
// stoptimer_expired(&hk2_timecounter, hkopt.hk2.rotate);

uint32_t send_counter = 0;
int main() {
    uart_init();    // initialize UART for debug output
    sei();	// Timer for timebase, ICP for ADC measurement
    timer0_init();  // Zeitbasis

    // Versuche den MCP2515 zu initilaisieren
    if (!mcp2515_init()) {
        uart_puts("MCP2515 FAIL\n\r");
        return 0;
    } else {
        uart_puts("MCP2515 OK\n\r");
    }

    stoptimer_init(&send_counter);      // starte Timer für das periodische Senden von CAN Nachrichten

    while (1) {
        buderus_sensoren();     // führe Zustandsautomaten, welche die Sensorwerte an der Buderus Steuerung ausliest
        ww_state_machine();

        // alle 2s sende aktuelle Werte
        if (stoptimer_expired(&send_counter, 1000)) {
            message.id = 0x71;
            message.header.rtr = 0;
            message.header.length = 8;
            message.data[0] = values.kesseltemperatur;
            message.data[1] = values.aussenfuehler;
            message.data[2] = values.brauchwasser;
            message.data[3] = values.HK2modul;
            message.data[4] = values.brenner;
            message.data[5] = values.vorlauf_hk2;
            message.data[6] = ww_state;
            message.data[7] = PORTD & ((1 << PD0) | (1 << PD3) | (1 << PD4) | (1 << PD5));

            // send message
            if (mcp2515_send_message(&message)) {
                //uart_puts("\n\rNachricht konnte gesendet werden\n\r");
            } else {
                //uart_puts("\n\rERROR Nachricht konnte nicht gesendet werden\n\r");
            }
            stoptimer_init(&send_counter);  // starte Timer neu, damit nach weiteren 2s wieder eine Nachricht gesendet wird
        }


//////////////////////////////////////////////////////receive_CAM_message/////////////////////////
        // check for incoming messages
        while (mcp2515_check_message()) {		// Nachricht vorhanden
            if (mcp2515_get_message(&message)) {		// Nachricht erfolgreich angekommen

                // Steuermessage
                if (message.id == 0x72 && message.header.length == 8) {
                    uart_puts("control\n\r");
                    if (message.data[0] == 1) { // [0] = 1 -> set message
                        uart_puts("set\n\r");
                        if (message.data[1] == 1) {
                            ww_active = 1;
                            uart_puts("active\n\r");
                        }
                        if (message.data[1] == 2) {
                            ww_soll = message.data[2];
                        }
                        if (message.data[1] == 3) {
                            ww_diff = message.data[2];
                        }
                        if (message.data[1] == 4) {
                            speicher1 = message.data[2];
                        }
                        if (message.data[1] == 5) {
                            buderus_fire = 1;
                        }
                        if (message.data[1] == 6) {
                            source_ist = message.data[2];
                        }
                    } else if (message.data[0] == 0) {  // [0] = 0 -> clear message
                        if (message.data[1] == 1) {
                            ww_active = 0;
                        }
                        if (message.data[1] == 2) {
                            ww_soll = message.data[2];
                        }
                        if (message.data[1] == 3) {
                            ww_diff = message.data[2];
                        }
                        if (message.data[1] == 4) {
                            speicher1 = message.data[2];
                        }
                        if (message.data[1] == 5) {
                            buderus_fire = 0;
                        }
                        if (message.data[1] == 6) {
                            source_ist = message.data[2];
                        }
                    }
                }

                // alte set Nachricht
                if (message.id == 0x77) {
                    if (message.data[0] == 1) {
                        // set
                        if (message.data[1] == 1) {
                            // BRENNER PD0
                            //PORTD |= 1 << PD0;
                            buderus_fire = 1;
                        }
                        if (message.data[2] == 1) {
                            // HK1     PD3
                            PORTD |= 1 << PD3;
                        }
                        if (message.data[3] == 1) {
                            // WW      PD4
                            PORTD |= 1 << PD4;
                        }
                        if (message.data[4] == 1) {
                            // PZ      PD5
                            PORTD |= 1 << PD5;
                        }
                    } else
                    if (message.data[0] == 0) {
                        //clear
                        if (message.data[1] == 1) {
                            // BRENNER PD0
                            //PORTD &= ~(1 << PD0);
                            buderus_fire = 0;
                        }
                        if (message.data[2] == 1) {
                            // HK1     PD3
                            PORTD &= ~(1 << PD3);
                        }
                        if (message.data[3] == 1) {
                            // WW      PD4
                            PORTD &= ~(1 << PD4);
                        }
                        if (message.data[4] == 1) {
                            // PZ      PD5
                            PORTD &= ~(1 << PD5);
                        }
                    }
                }

//                uart_puts("\n\rID: ");
//                itoa((message.id), buf, 16);
//                uart_puts(buf);
//                uart_puts("   DLC: ");
//                itoa((message.header.length), buf, 10);
//                uart_puts(buf);
//                uart_puts("   Data: ");
//                for (uint8_t i = 0; i < message.header.length; i++) {
//                    itoa((message.data[i]), buf, 10);
//                    uart_puts(buf);
//                    uart_putc(' ');
//                }
//                uart_puts("\n\r");
            }
            else {
                uart_puts("FAIL\n\r");
            }
        }
    }
    return 0;
}
