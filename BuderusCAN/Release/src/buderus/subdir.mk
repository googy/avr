################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/buderus/messung.c \
../src/buderus/sensoren.c 

OBJS += \
./src/buderus/messung.o \
./src/buderus/sensoren.o 

C_DEPS += \
./src/buderus/messung.d \
./src/buderus/sensoren.d 


# Each subdirectory must supply rules for building sources it contributes
src/buderus/%.o: ../src/buderus/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -Os -fpack-struct -fshort-enums -ffunction-sections -fdata-sections -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega328p -DF_CPU=8000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


