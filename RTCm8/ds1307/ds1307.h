#ifndef DS1307_H
#define DS1307_H

#define DS1307_ADDR 0x68	//device address

extern void ds1307_init();
extern uint8_t ds1307_setdate(uint8_t year, uint8_t month, uint8_t day, uint8_t hour, uint8_t minute, uint8_t second);
extern void ds1307_getdate(const void* now_p);

uint8_t eepReadByte(uint8_t dev_address, uint16_t dat_address);
void eepWriteByte(uint8_t dev_address, uint16_t dat_address, uint8_t data);

#endif
