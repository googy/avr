#include "avr/io.h"
#include "avr/pgmspace.h"
#include "util/delay.h"

#include "ds1307.h"
#include "../i2chw/i2cmaster.h"
#include "../types.h"

/*
 * transform bcd value to deciaml
 */
static uint8_t bcd2dec(uint8_t val) {
	return ((val >> 4) * 10) + (val & 0xF);
}

/*
 * days per month
 */
const uint8_t ds1307_daysinmonth [] PROGMEM = { 31,28,31,30,31,30,31,31,30,31,30,31 };

void ds1307_init() {
	i2c_init();
	_delay_us(10);
}

/*
 * transform decimal value to bcd
 */
uint8_t ds1307_dec2bcd(uint8_t val) {
	return val + 6 * (val / 10);
}

/*
 * get number of days since 2000/01/01 (valid for 2001..2099)
 */
static uint16_t ds1307_date2days(uint8_t y, uint8_t m, uint8_t d) {
	uint16_t days = d;
	for (uint8_t i = 1; i < m; ++i)
		days += pgm_read_byte(ds1307_daysinmonth + i - 1);
	if (m > 2 && y % 4 == 0)
		++days;
	return days + 365 * y + (y + 3) / 4 - 1;
}

/*
 * get day of a week
 */
uint8_t ds1307_getdayofweek(uint8_t y, uint8_t m, uint8_t d) {
	uint16_t day = ds1307_date2days(y, m, d);
	return (day + 6) % 7;
}

/*
 * set date
 */
uint8_t ds1307_setdate(uint8_t year, uint8_t month, uint8_t day, uint8_t hour, uint8_t minute, uint8_t second) {
	//sanitize data
	if (second < 0 || second > 59 ||
		minute < 0 || minute > 59 ||
		hour < 0 || hour > 23 ||
		day < 1 || day > 31 ||
		month < 1 || month > 12 ||
		year < 0 || year > 99)
		return 8;

	//sanitize day based on month
	if(day > pgm_read_byte(ds1307_daysinmonth + month - 1))
		return 0;

	//get day of week
	uint8_t dayofweek = ds1307_getdayofweek(year, month, day);

	//write date
	i2c_start_wait((DS1307_ADDR << 1) | I2C_WRITE);
	i2c_write(0x00);//stop oscillator
	i2c_write(ds1307_dec2bcd(second));
	i2c_write(ds1307_dec2bcd(minute));
	i2c_write(ds1307_dec2bcd(hour));
	i2c_write(ds1307_dec2bcd(dayofweek));
	i2c_write(ds1307_dec2bcd(day));
	i2c_write(ds1307_dec2bcd(month));
	i2c_write(ds1307_dec2bcd(year));
	i2c_write(0x00); //start oscillator
	i2c_stop();

	return 1;
}

/*
 * get date
 */
void ds1307_getdate(const void* now_p) {
	struct timedate* now = (struct timedate*)now_p;

	i2c_start_wait((DS1307_ADDR << 1) | I2C_WRITE);		// signal write
	i2c_write(0x00);									// write pointer = 0x00, address of first read -> seconds
	//i2c_stop();
	i2c_rep_start((DS1307_ADDR << 1) | I2C_READ);		// read starting from pointer, pointer automatically increases
	now->second = bcd2dec(i2c_readAck());				// read second, bit 7 is oscillator status
	now->minute = bcd2dec(i2c_readAck());				// read minute, bit 7 unused
	now->hour = bcd2dec(i2c_readAck());					// read hour, works only for 24h format
	i2c_readAck();										// read weekday and skip, bit 3-7 unused
	now->day = bcd2dec(i2c_readAck());					// read day, bit 6-7 unused
	now->month = bcd2dec(i2c_readAck());				// read month, bit 5-7 unused
	now->year = bcd2dec(i2c_readNak());					// read year, NACK to stop read
	i2c_stop();
}
uint8_t eepReadByte(uint8_t dev_address, uint16_t dat_address) {
	i2c_start_wait((dev_address << 1) | I2C_WRITE);		// repeat sending address till device ready
	i2c_write(dat_address >> 8);						// send data address most significant byte
	i2c_write(dat_address);								// send data address least significant byte
	i2c_rep_start((dev_address << 1) | I2C_READ);		// start reading
	uint8_t cad = i2c_readNak();						// read byte and confirm with NACK, no other data
	i2c_stop();											// stop transfer
	return cad;
}

void eepWriteByte(uint8_t dev_address, uint16_t dat_address, uint8_t data) {
	i2c_start_wait((dev_address << 1) | I2C_WRITE);		// repeat sending address till device ready
	i2c_write(dat_address >> 8);						// send data address most significant byte
	i2c_write(dat_address);								// send data address least significant byte
	i2c_write(data);									// write byte
	i2c_stop();											// stop transfer
}
