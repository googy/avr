/*
ds1307 lib sample

copyright (c) Davide Gironi, 2013

Released under GPLv3.
Please refer to LICENSE file for licensing information.
*/

#include <stdlib.h>
#include <stdio.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "uart/uart.h"
#define UART_BAUD_RATE 2400

#include "ds1307/ds1307.h"

#include "types.h"

int main(void) {
	//init uart
	uart_init( UART_BAUD_SELECT(UART_BAUD_RATE,F_CPU) );

	//init ds1307
	ds1307_init();

	sei();

	uart_puts("startup...");

	//check set date
//	ds1307_setdate(14, 07, 12, 8, 11, 0);

	struct timedate now;

	//writeByte(0b01010000, 0, 8);
	uint16_t add = 0;

	for(;;) {
		//get date
		ds1307_getdate(&now);
		char buf[50];
		sprintf(buf, "%d/%d/%d %d:%d:%d", now.year, now.month, now.day, now.hour, now.minute, now.second);
		uart_puts(buf);	uart_puts("\r\n");

		_delay_ms(500);
//
//		if (add > 20) add = 0;
//		uint8_t bu[5] = {0, 0, 0, 0, 0};
//		uint8_t f = eepReadByte(0b01010000, add++);
//		itoa(f, bu, 10);
//		uart_puts(bu);
		_delay_ms(500);
	}
}
