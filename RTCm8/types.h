/*
 * types.h
 *
 *  Created on: 12.07.2014
 *      Author: googy_000
 */

#ifndef TYPES_H_
#define TYPES_H_

struct timedate {
	uint8_t second;
	uint8_t minute;
	uint8_t hour;
	uint8_t day;
	uint8_t month;
	uint8_t year;
};

#endif /* TYPES_H_ */
