//#include <avr/io.h>
#include <util/delay.h>
#include <stdlib.h>	//utoa
#include <avr/interrupt.h>

#include "ow.h"

// USART Baudrate berechnen
#define BAUD 9600UL	//Baudrate festlegen
#define UBRR_VAL ((F_CPU+BAUD*8)/(BAUD*16)-1)   // Baudratenregister berechnen
#define BAUD_REAL (F_CPU/(16*(UBRR_VAL+1)))     // Reale Baudrate
#define BAUD_ERROR ((BAUD_REAL*1000)/BAUD) // Fehler in Promille, 1000 = kein Fehler.
#if ((BAUD_ERROR<990) || (BAUD_ERROR>1010))	//Warnung falls Fehler zu gro�
#error Systematischer Fehler der Baudrate gr�sser 1% und damit zu hoch!	//Warnung
#endif
//-----------------------------USART-------------------------------------------------------
void USART_init() {
	UBRRH = UBRR_VAL >> 8; //Berechnete Baudrate shift nach rechts, folglich die oberen 8 Bits werden gesetzt
	UBRRL = UBRR_VAL & 0xFF; //bitweises UND mit 255, folglich die unteren Bits werden
	UCSRB |= (1 << TXEN); //UART auf Senden transmit einschtellen
	// | (1 << RXEN) f�r receive
	UCSRC |= (1 << URSEL) | (1 << UCSZ1) | (1 << UCSZ0); //URSEL siehe Datenblatt, UCSZ1/0 8-bit Zeichengr��e
}

void USART_SendByte(uint8_t Data) {
	while (!(UCSRA & (1 << UDRE))) { //warte bis vorheriges byte gesendet ist
	}
	UDR = Data; //sende
}

void USART_SendString(char *s) {
	while (*s) { //mache weiter solange kein 0-byte als Abschluss der Zeichenkette
		USART_SendByte(*s); //sende aktuelle Array Zelle
		s++; //schiebe den Zeiger eine Zelle weiter
	}
}
//-----------DCF---
uint8_t dcf_flag = 0;
volatile uint8_t ready_flag = 0;

uint8_t dcf[8];
volatile uint8_t dcf_copy[8];
uint8_t dcf_t[8];

uint8_t pos = 0;		// interrupt used iterator
uint8_t dcf_high = 0;	// interrupt used counting variable high cycles
uint8_t dcf_low = 0;	// interrupt used counting variable low cycles

//-----------------------------1-wire------------------------------------------------------
//---setup---
void wait_reset() {
	_delay_us(500);
}

void wait_recovery() {
	_delay_us(5);
}

void wait_write1() {
	_delay_us(5);
}

void wait_write0() {
	_delay_us(80);
}

void ow_rel() {
	OW_DIR &= ~(1 << OW);
	OW_OUT |= (1 << OW);
}

void ow_lo() {
	OW_OUT &= ~(1 << OW);
	OW_DIR |= (1 << OW);
}

#define IS_OW() (OW_PIN & (1 << OW))

//---functions---
void ow_init() {
	ow_rel();
}

void ow_reset() {
	ow_lo();
	wait_reset();
	ow_rel();
	wait_reset();
}

void ow_send(int data, uint8_t len) {
	for (int i = 0; i < len; i++) {
		if ((data & 1) > 0) { // send 1
			ow_lo();
			wait_write1();
			ow_rel();
			wait_write0();
		} else { // send 0
			ow_lo();
			wait_write0();
			ow_rel();
		}
		data >>= 1;
		wait_recovery();
	}
}

void ow_write(uint8_t data) {
	ow_send(data, 8);
}

uint64_t ow_read(uint8_t len) {
	uint64_t mask = 1;
	uint64_t result = 0;
	for (int i = 0; i < len; i++) {
		ow_lo();
		wait_recovery();
		ow_rel();
		_delay_us(8);
		//Sample
		if (IS_OW()) result |= mask;
		mask <<= 1;
		wait_write0();
	}
	return result;
}

void send_id(uint8_t *id) {
	for (uint8_t i = 0; i < 8; i++) {
		ow_write(*(id + i)); //0-8 mal den Zeiger verschieben und jeweils den Inhalt der Arrayzelle senden
	}
}

//-----------------------------------------------------------------------------------------
void printT(uint16_t t) {
	char s[10];
	uint16_t nk = 0;
	utoa((t >> 4), s, 10);
	USART_SendString(s);
	USART_SendByte('.');
	if (t & (1 << 3)) {
		nk = 50;
	}
	if (t & (1 << 2)) {
		nk += 25;
	}
	if (t & (1 << 1)) {
		nk += 12;
	}
	if (t & (1 << 0)) {
		nk += 6;
	}
	if (nk < 10) {
		USART_SendByte('0');
	}
	utoa(nk, s, 10);
	USART_SendString(s);
	USART_SendByte('\r');
	USART_SendByte('\n');
}

void timer_init(){
	// Initialisierung:
	TCCR1B = (1<<CS12) | (1<<WGM12);	// Prescaler von 256 | CTC-Modus
	OCR1A  = 0xF424;			// Vergleichswert 62500
	TIMSK |= (1<<OCIE1A);		// Interrupts aktivieren und damit Timer starten
	sei();
}

int main() {
	//uint8_t id1[8] = { 0x28, 0x94, 0x80, 0xAA, 0x03, 0x00, 0x00, 0xEE }; //ROM ID 1
	//uint8_t id2[8] = { 0x28, 0xFF, 0x8B, 0xAA, 0x03, 0x00, 0x00, 0xA5 }; //ROM ID 2 Wohnzimmer
//	uint16_t t = 0; //Temperatur Hilfsvariable


	DDRD |= (1<<DDD6);
	USART_init(); //Initialisierung USART
//	timer_init(); //Timer1 CTC mode
//	ow_init(); //Initialisierung 1-Wire

	TCCR0 = ((1<<CS02) | (1<<CS00)); // Prescaler 1024
	TIMSK = (1<<TOIE0);	//Time overflow interrupt aktivieren
	sei();// Global Interrupts aktivieren

	while (1) {
		// Messung starten
//		ow_reset(); //reset
//		ow_write(0xCC); //SKIP ROM [CCh]
//		ow_write(0x44); //CONVERT T [44h]

//		_delay_ms(1000); //min. 750ms f�r Messung mit 12Bit

//		USART_SendByte(0x0C); //clear screen

		// Sensor 1
//		ow_reset(); //reset
//		ow_write(0x55); //MATCH ROM [55h]
//		send_id(id1); //send ROM id
//		ow_write(0xBE); //READ SCRATCHPAD [BEh]
//		t = ow_read(16); //read 2 bytes
//		printT(t);

		if (ready_flag){
			ready_flag = 0;
			for (uint8_t i = 0; i< 8; i++) {
				dcf_t[i] = dcf_copy[i];
			}

			char g[5];
			//Stunde
			uint8_t stunde = (((dcf_t[29/8] >> (29%8)) & 1)) +
							(((dcf_t[30/8] >> (30%8)) & 1) << 1) +
							(((dcf_t[31/8] >> (31%8)) & 1) << 2) +
							(((dcf_t[32/8] >> (32%8)) & 1) << 3) +
							(((dcf_t[33/8] >> (33%8)) & 1) * 10) +
							(((dcf_t[34/8] >> (34%8)) & 1) * 20);
			utoa(stunde, g, 10);
			USART_SendString(g);

			USART_SendByte(':');
			//Minute
			uint8_t minute = (((dcf_t[21/8] >> (21%8)) & 1)) +
							(((dcf_t[22/8] >> (22%8)) & 1) << 1) +
							(((dcf_t[23/8] >> (23%8)) & 1) << 2) +
							(((dcf_t[24/8] >> (24%8)) & 1) << 3) +
							(((dcf_t[25/8] >> (25%8)) & 1) * 10) +
							(((dcf_t[26/8] >> (26%8)) & 1) * 20) +
							(((dcf_t[27/8] >> (27%8)) & 1) * 40);
			utoa(minute, g, 10);
			USART_SendString(g);

			USART_SendByte(' ');
			//Tag
			uint8_t tag = (((dcf_t[36/8] >> (36%8)) & 1)) +
						(((dcf_t[37/8] >> (37%8)) & 1) << 1) +
						(((dcf_t[38/8] >> (38%8)) & 1) << 2) +
						(((dcf_t[39/8] >> (39%8)) & 1) << 3) +
						(((dcf_t[40/8] >> (40%8)) & 1) * 10) +
						(((dcf_t[41/8] >> (41%8)) & 1) * 20);
			utoa(tag, g, 10);
			USART_SendString(g);

			USART_SendByte('.');

			//Monat
			uint8_t monat = (((dcf_t[45/8] >> (45%8)) & 1)) +
							(((dcf_t[46/8] >> (46%8)) & 1) << 1) +
							(((dcf_t[47/8] >> (47%8)) & 1) << 2) +
							(((dcf_t[48/8] >> (48%8)) & 1) << 3) +
							(((dcf_t[49/8] >> (49%8)) & 1) * 10);
			utoa(monat, g, 10);
			USART_SendString(g);

			USART_SendByte('.');
			//Jahr
			uint8_t jahr = (((dcf_t[50/8] >> (50%8)) & 1)) +
							(((dcf_t[51/8] >> (51%8)) & 1) << 1) +
							(((dcf_t[52/8] >> (52%8)) & 1) << 2) +
							(((dcf_t[53/8] >> (53%8)) & 1) << 3) +
							(((dcf_t[54/8] >> (54%8)) & 1) * 10) +
							(((dcf_t[55/8] >> (55%8)) & 1) * 20) +
							(((dcf_t[56/8] >> (56%8)) & 1) * 40) +
							(((dcf_t[57/8] >> (57%8)) & 1) * 80);
			utoa(jahr, g, 10);
			USART_SendString(g);
			USART_SendByte('\r');
			USART_SendByte('\n');

			//Wochentag
			uint8_t wtag = (((dcf_t[42/8] >> (42%8)) & 1)) +
							(((dcf_t[43/8] >> (43%8)) & 1) << 1) +
							(((dcf_t[44/8] >> (44%8)) & 1) << 2);
			utoa(wtag, g, 10);
			USART_SendString(g);
			USART_SendByte('\r');
			USART_SendByte('\n');USART_SendByte('\n');
		}
	}
}



// Interrupt alle 1sec 16MHz (Takt) / 256 (Prescaler) / 62500 (compare match)
//ISR (TIMER1_COMPA_vect){    // Timer/Counter1 Compare Match A interrupt
//}

// Interrupt alle 16,384ms 16MHz (Takt) / 1024 (Prescaler) / 256 (Overflow)
ISR (TIMER0_OVF_vect){							// Timer 0 Overflow interrupt
	// high Zyklus
	if (!(PIND & (1 << PIND4))){				// high pegel
		if (dcf_flag){							// flag gesetzt => low Zyklen beendet
			if (dcf_low > 100){					// gez�hlte low Zyklen > 100 => Minutenmarke
				pos = 0;						// beginne am Anfang des neuen Telegrams
				for (uint8_t i = 0; i <= 7; i++){
					dcf_copy[i] = dcf[i];		// kopiere Telegramm
				}
				ready_flag = 1;					// Telegramm kopiert
			}
			dcf_low = 0;						// setze gez�hlte low Zyklen zur�ck
			dcf_flag = 0;						// low Zyklen ausgewertet
		}
		dcf_high++;								// z�hle high Zyklus
	// low Zyklus
	} else {
		if (!dcf_flag){							// flag nicht gesetzt => high Zyklen beendet
			if (dcf_high > 10){					// => DCF 1
				dcf[pos>>3] |= (1<<(pos & 7));	// notiere Bit
				pos++;							// zum n�chsten Bit
			}else if (dcf_high > 5){			// => DCF 0
				dcf[pos>>3] &= ~(1<<(pos & 7));	// notiere Bit
				pos++;							// zum n�chsten Bit
			}
			if (pos > 40) {
				ready_flag = 0;					//ready flag entfernen, da gleich der Wert aktualisiert wird
			}
			dcf_high = 0;						// setze gez�hlte high Zyklen zur�ck
			dcf_flag = 1;						// high Zyklen ausgewertet
		}
		dcf_low++;								// z�hle low Zyklus
	}
}
