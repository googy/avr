/*
 * ow.h
 *
 *
 *      Author: cyblord
 */

#ifndef OW_H_
#define OW_H_

#define OW_OUT		PORTD
#define OW_DIR		DDRD
#define OW_PIN		PIND
#define OW			PD7

void ow_init();
void ow_reset();
void ow_write(uint8_t data);
uint64_t ow_read(uint8_t len);

#endif /* OW_H_ */
