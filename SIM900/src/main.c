/*
 * geschrieben f�r ein Arduino Pro mini 5V, 16MHz
 * keine Arduino IDE, AVR-GCC Compiler und usbasp als Programmer
 * zus�tzlich optional als Interface ein 0.91 Zoll SSD1306 128x32 OLED Display mit I2C angebunden
 * Interface zum Zuheizer siehe beigelegte Schaltug, wobei ich w�rde f�r R19 einen etwas h�heren Wert nehmen
 */
#include <avr/io.h>
#include <stdlib.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdio.h>
#include <inttypes.h>

#include "uart.h"

char buf[10];

//-----------------------------timebase----------------------------------------------------------------
volatile uint8_t time1 = 0;
// 16000000Hz / 1024 / 256 = 61Hz 61.03515625
ISR(TIMER2_OVF_vect) {
    time1++;             // tick 16,384ms, wrap around 4,194304s
    PORTB |= 1 << PB5;
}

void init_tick_timer() {
    TCCR2A = 0;     // normal mode, no output compare output
    TCCR2B = (1 << CS22) | (1 << CS21) | (1 << CS20);   // 1024 prescaler
    TIMSK2 = 1 << TOIE2;                                // enable Timer2 Overflow Interrupt
}
//-----------------------------------------------------------------------------------------------------




int main() {
    // initialisiere Zeitgeber f�r Fehlererkennung
    init_tick_timer();      // Timer0 overflow Interrupt dient als Zeitgeber
    sei();          // unmask Interrupts to Processor

    return 0;
}
