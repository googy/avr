/*
 * Strobo.c
 *
 * Created: 21.12.2014 01:00:38
 *  Author: googy
 */ 

#define F_CPU 4000000

#include <avr/io.h>
#include <util/delay.h>

uint8_t delay = 0;

int main(void) {
	DDRB |= (1 << PB3);
	DDRD &= ~(1 << PD5);	// Taster als Eingang
	PORTD |= (1 << PD5);	// Pullup am Taster
	
	uint8_t strobe = 0;
	
    while(1) {
		if (delay != 0) {
			delay--;
		}
		
		switch (strobe) {
		case 0:
			PORTB |= (1 << PB3);
			_delay_ms(50);
			PORTB &= ~(1 << PB3);
			_delay_ms(50);
			PORTB |= (1 << PB3);
			_delay_ms(50);
			PORTB &= ~(1 << PB3);
			_delay_ms(50);
			PORTB |= (1 << PB3);
			_delay_ms(50);
			PORTB &= ~(1 << PB3);
			_delay_ms(600);
			break;
		case 1:
			PORTB |= (1 << PB3);
			_delay_ms(50);
			PORTB &= ~(1 << PB3);
			_delay_ms(300);
			break;
		case 2:
			PORTB |= (1 << PB3);
			_delay_ms(30);
			PORTB &= ~(1 << PB3);
			_delay_ms(150);
			break;
		default:
			strobe = 0;
			break;
		}
		
		if (!(PIND & (1 << PD5)) && delay == 0) {
			delay = 5;
			strobe++;
		}
    }
}
