/*
 * main.c
 *
 *  Created on: 29.06.2014
 *      Author: googy_000
 */

#include <avr/io.h>
#include <stdlib.h>
#include <util/delay.h>
#include "uart.h"
#include "USI_TWI_Master.h"

#define DS1307		0x68

int main() {
	uart_init();

	USI_TWI_Master_Initialise();

	while (1) {
		uint8_t buf[4];
		buf[0] = (DS1307 << 1) | 1;	// read
		buf[1] = 0x06;
		USI_TWI_Start_Transceiver_With_Data(buf, 4);

		char cbuf[10];
		itoa(USI_TWI_Get_State_Info(), cbuf, 10);
		uart_puts(cbuf);
		uart_putc(' ');
		itoa(buf[1], cbuf, 10);
		uart_puts(cbuf);
		uart_putc(10);
		uart_putc(13);
		_delay_ms(1000);
	}
	return 0;
}
