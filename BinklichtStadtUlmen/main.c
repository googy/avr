#include <avr/io.h>
#include <util/delay.h>

void allOn() {
	PORTD &= ~(1 << PD5);
	PORTD &= ~(1 << PD4);
	PORTB &= ~(1 << PD0);
	PORTB &= ~(1 << PD1);
}

void allOff() {
	PORTD |= (1 << PD5);
	PORTD |= (1 << PD4);
	PORTB |= (1 << PD0);
	PORTB |= (1 << PD1);
}

void vlOn() {PORTD &= ~(1 << PD5);}
void vlOff() {PORTD |= (1 << PD5);}
void hlOn() {PORTD &= ~(1 << PD4);}
void hlOff() {PORTD |= (1 << PD4);}
void vrOn() {PORTB &= ~(1 << PB0);}
void vrOff() {PORTB |= (1 << PB0);}
void hrOn() {PORTB &= ~(1 << PB1);}
void hrOff() {PORTB |= (1 << PB1);}

void v1() {
	for (uint8_t i = 0; i < 5; i++) {
		allOn();
		_delay_ms(50);
		allOff();
		_delay_ms(50);
	}
	_delay_ms(500);
}

void v2() {
	for (uint8_t i = 0; i < 5; i++) {
		vlOn();
		hlOn();
		_delay_ms(50);
		vlOff();
		hlOff();
		_delay_ms(50);
	}
	for (uint8_t i = 0; i < 5; i++) {
		vrOn();
		hrOn();
		_delay_ms(50);
		vrOff();
		hrOff();
		_delay_ms(50);
	}
}

void v3() {
	for (uint8_t i = 0; i < 5; i++) {
		vlOn();
		vrOn();
		_delay_ms(50);
		vlOff();
		vrOff();
		_delay_ms(50);
	}
	_delay_ms(200);
	for (uint8_t i = 0; i < 5; i++) {
		hlOn();
		hrOn();
		_delay_ms(50);
		hlOff();
		hrOff();
		_delay_ms(50);
	}
	_delay_ms(200);
}

int main(void) {
	DDRD |= (1 << PD4) | (1 << PD5);
	DDRB |= (1 << PB0) | (1 << PB1);

	while (1) {
		v1();
	}
	return 0;
}

