/*
 * timer.c
 *
 *  Created on: 24.08.2013
 *      Author: googy
 */

#include <avr/interrupt.h>

#include "timer.h"
#include "tastatur.h"
#include "uart.h"

void init_timer0() { // ca. 33ms
	TCCR0A = 0;
	TCCR0B = (1 << CS02) | (0 << CS01) | (1 << CS00);
	TIMSK = (1 << TOIE0);
}

uint8_t ticks = 0;

ISR(TIMER0_OVF_vect) { // ca. 33ms
	ticks++;
	if (ticks >= 30) {
		ticks = 0;
		timer++;
	}
}
