/*
 * tastatur.c
 *
 *  Created on: 24.08.2013
 *      Author: googy
 */

#include <avr/io.h>
#include "uart.h"

void init_tastatur() {
	// init Spalten
	DDRD &= ~(1 << PD3) | (1 << PD4) | (1 << PD5);	// X1 X2 X3
	PORTD &= ~((1 << PD3) | (1 << PD4) | (1 << PD5));

	// init Zeilen (Eingänge)
	DDRB &= ~((1 << PB0) | (1 << PB1) | (1 << PB2) | (1 << PB3));	// Y1 Y2 Y3 Y4
	// pull ups deaktivieren, active high, externe pull-down Widerstände
	PORTB &= ~((1 << PB0) | (1 << PB1) | (1 << PB2) | (1 << PB3));
}
uint16_t read_keys() {
	uint16_t keys = 0;
	// 1.
	DDRD |= (1 << PD3);
	PORTD |= (1 << PD3);
	if (PORTD) {
		asm volatile("nop");
	}

	if (PINB & (1 << PINB0)) {
		keys |= (1 << 1);
	}
	if (PINB & (1 << PINB1)) {
		keys |= (1 << 4);
	}
	if (PINB & (1 << PINB2)) {
		keys |= (1 << 7);
	}
	if (PINB & (1 << PINB3)) {
		keys |= (1 << 10);
	}
	DDRD &= ~(1 << PD3);
	PORTD &= ~(1 << PD3);
	// 2.
	DDRD |= (1 << PD4);
	PORTD |= (1 << PD4);
	if (PORTD) {
		asm volatile("nop");
	}
	if (PINB & (1 << PINB0)) {
		keys |= (1 << 2);
	}
	if (PINB & (1 << PINB1)) {
		keys |= (1 << 5);
	}
	if (PINB & (1 << PINB2)) {
		keys |= (1 << 8);
	}
	if (PINB & (1 << PINB3)) {
		keys |= (1 << 0);
	}
	DDRD &= ~(1 << PD4);
	PORTD &= ~(1 << PD4);
	// 3.
	DDRD |= (1 << PD5);
	PORTD |= (1 << PD5);
	if (PORTD) {
		asm volatile("nop");
	}
	if (PINB & (1 << PINB0)) {
		keys |= (1 << 3);
	}
	if (PINB & (1 << PINB1)) {
		keys |= (1 << 6);
	}
	if (PINB & (1 << PINB2)) {
		keys |= (1 << 9);
	}
	if (PINB & (1 << PINB3)) {
		keys |= (1 << 11);
	}
	DDRD &= ~(1 << PD5);
	PORTD &= ~(1 << PD5);
	return keys;
}
