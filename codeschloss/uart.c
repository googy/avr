/*
 * uart.c
 *
 *  Created on: 23.08.2013
 *      Author: mikrocontroller.net
 */
#include <avr/io.h>

#include "uart.h"

#define BAUD 9600UL      // Baudrate

#define UBRR_VAL ((F_CPU+BAUD*8)/(BAUD*16)-1)	// clever runden
#define BAUD_REAL (F_CPU/(16*(UBRR_VAL+1)))	// Reale Baudrate
#define BAUD_ERROR ((BAUD_REAL*1000)/BAUD)	// Fehler in Promille, 1000 = kein Fehler.

#if ((BAUD_ERROR<990) || (BAUD_ERROR>1010))
	#error Systematischer Fehler der Baudrate gr�sser 1% und damit zu hoch!
#endif

void uart_init(){
	UBRRH = UBRR_VAL >> 8;
	UBRRL = UBRR_VAL & 0xFF;

	UCSRB = (1<<TXEN);
	UCSRC = (1<<UCSZ1)|(1<<UCSZ0);	// Asynchron 8N1
}

void uart_putc(unsigned char c) {
	while (!(UCSRA & (1<<UDRE))) {}
	UDR = c;
}

void uart_puts (char *s) {
	while (*s) {
		uart_putc(*s);
		s++;
	}
}

void uart_putd(uint8_t i) {
	uart_putc( i + '0' );
}

uint8_t uart_receive( void ) {
	while ( !(UCSRA & (1 << RXC)) ) {};
	return UDR;
}

void uart_flush( void ) {
	uint8_t dummy;
	while ( UCSRA & (1 << RXC) ) {
		dummy = UDR;
	}
}
