/*
 * main.h
 *
 *  Created on: 24.08.2013
 *      Author: googy
 */

#ifndef MAIN_H_
#define MAIN_H_

extern volatile uint8_t timer;
volatile uint8_t keys[4][3] = {{0,0,0},{0,0,0},{0,0,0},{0,0,0}};
volatile uint8_t k[12] = {0,0,0,0,0,0,0,0,0,0,0,0};

volatile uint16_t tasten = 0;

#endif /* MAIN_H_ */
