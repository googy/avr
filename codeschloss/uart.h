/*
 * uart.h
 *
 *  Created on: 23.08.2013
 *      Author: googy
 */

#ifndef UART_H_
#define UART_H_

void uart_init();
void uart_putc(unsigned char c);
void uart_puts (char *s);
void uart_putd(uint8_t i);
uint8_t uart_receive( void );
void uart_flush( void );

#endif /* UART_H_ */
