/*
 * timer.h
 *
 *  Created on: 24.08.2013
 *      Author: googy
 */

#ifndef TIMER_H_
#define TIMER_H_

volatile uint8_t timer;

extern volatile uint8_t keys[4][3];
extern volatile uint8_t k[12];

extern volatile uint16_t tasten;

void init_timer0();

#endif /* TIMER_H_ */
