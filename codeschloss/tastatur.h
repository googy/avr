/*
 * tastatur.h
 *
 *  Created on: 24.08.2013
 *      Author: googy
 */

#ifndef TASTATUR_H_
#define TASTATUR_H_

void init_tastatur();
uint16_t read_keys();

#endif /* TASTATUR_H_ */
