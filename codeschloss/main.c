/*
 * main.c
 *
 *  Created on: 02.04.2014
 *      Author: googy
 */

#include <avr/io.h>
#include <stdlib.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>

#include "main.h"
#include "uart.h"
#include "timer.h"
#include "tastatur.h"

enum key_state{
	UP,
	DEBOUNCE_DOWN,
	DOWN,
	DEBOUNCE_UP
};

uint8_t digits[20];

int main() {
	// init LED
	DDRA |= (1 << PA0);
	PORTA &= ~(1 << PA0);

	uart_init();
	init_tastatur();
	init_timer0();
	sei();

	uint8_t state = 0;
	uint8_t digit = 0;
	uint16_t keys = 0;

	enum key_state tas;
	uint16_t down = 0;
	char buf[18];

	uint16_t new_keys = 0;
	uint8_t i = 0;

	while (1) {
		keys = read_keys();
		new_keys = keys & ~down;
		if (new_keys) {
			i = 0;
			while (i < 11) {
				if (new_keys & (1 << i)) {
					if (digit < 20) {
						digits[digit] = i;
						digit++;
					}
					if (digit == 20) {
						digit = 0;
					}
					uart_putc(10);
					uart_putc(13);
					uart_putc('0' + i);
					down |= (1 << i);
				}
				i++;
			}
			if (new_keys & (1 << 11)) {
				uart_putc(10);
				uart_putc(13);
				for (uint8_t j = 0; j < 20; j++) {
					uart_putc('0' + digits[j]);
				}
			}
		}

		_delay_ms(10);
		i = 0;
		while (i < 11) {
			if (!(keys & (1 << i))) {
				down &= ~(1 << i);
			}
			i++;
		}


//		uint16_t keys = read_keys();
//		switch(state) {
//		case 0:	// wait key
//			uart_putc('0' + code[digit]);
//
//			if (!(keys - (1 << code[digit]))) {
//				uart_putc('0' + digit);
//				uart_putc(10);
//				uart_putc(13);
//				state = 1;
//				break;
//			}
//			if (keys) {
//				digit = 0;
//				break;
//			}
//			if ((digit > 0) && (timer > 5)) {
//				digit = 0;
//				break;
//			}
//			break;
//		case 1: // key OK
//			if (!(keys - (1 << code[digit]))) {
//				uart_puts("repeate");
//				uart_putc(10);
//				uart_putc(13);
//				break;
//			}
//			if (!keys) {
//				digit++;
//				uart_puts("next");
//				uart_putc('0' + digit);
//				uart_putc(10);
//				uart_putc(13);
//				//_delay_ms(2000);
//			} else {
//				digit = 0;
//				uart_puts("wrong");
//				uart_putc(10);
//				uart_putc(13);
//			}
//			timer = 0;
//			state = 0;
//			if (digit >= 7) {
//				PORTA |= (1 << PA0);
//				state = 2;
//				timer = 0;
//				uart_puts("done");
//				uart_putc(10);
//				uart_putc(13);
//			}
//			break;
//		case 2:
//			if (timer > 5) {
//				PORTA &= ~(1 << PA0);
//				digit = 0;
//				state = 0;
//				uart_puts("new");
//				uart_putc(10);
//				uart_putc(13);
//			}
//			uart_putc('0' + timer);
//			uart_putc(10);
//			uart_putc(13);
//			break;
//		}
	}
}
