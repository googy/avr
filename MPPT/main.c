#include <avr/io.h>
#include <stdlib.h>
#include <util/delay.h>
#include <stdio.h>

#include "uart.h"
#include "i2cmaster.h"
#include "ssd1306.h"

char buf[10];

// Diese Beispiel zeigt die Anwendung des ADC eines ATmega169
// unter Verwendung der internen Referenzspannung von nominell 1,1V.
// Zur Anpassung an andere AVR und/oder andere Referenzspannungen
// siehe Erl�uterungen in diesem Tutorial und im Datenblatt

/* ADC initialisieren */
void ADC_Init(void) {
  // die Versorgungsspannung AVcc als Referenz w�hlen:
  ADMUX = (1<<REFS0);
  // oder interne Referenzspannung als Referenz f�r den ADC w�hlen:
  // ADMUX = (1<<REFS1) | (1<<REFS0);

  // Bit ADFR ("free running") in ADCSRA steht beim Einschalten
  // schon auf 0, also single conversion
  ADCSRA = (1 << ADPS2) | (1<<ADPS1) | (1<<ADPS0);     // Frequenzvorteiler
  ADCSRA |= (1<<ADEN);                  // ADC aktivieren

  /* nach Aktivieren des ADC wird ein "Dummy-Readout" empfohlen, man liest
     also einen Wert und verwirft diesen, um den ADC "warmlaufen zu lassen" */

  ADCSRA |= (1<<ADSC);                  // eine ADC-Wandlung
  while (ADCSRA & (1<<ADSC) ) {         // auf Abschluss der Konvertierung warten
  }
  /* ADCW muss einmal gelesen werden, sonst wird Ergebnis der n�chsten
     Wandlung nicht �bernommen. */
  (void) ADCW;
}

/* ADC Einzelmessung */
uint16_t ADC_Read( uint8_t channel )
{
  // Kanal waehlen, ohne andere Bits zu beeinflu�en
  ADMUX = (ADMUX & ~(0x1F)) | (channel & 0x1F);
  ADCSRA |= (1<<ADSC);            // eine Wandlung "single conversion"
  while (ADCSRA & (1<<ADSC) ) {   // auf Abschluss der Konvertierung warten
  }
  return ADCW;                    // ADC auslesen und zur�ckgeben
}

/* ADC Mehrfachmessung mit Mittelwertbbildung */
/* beachte: Wertebereich der Summenvariablen */
uint16_t ADC_Read_Avg( uint8_t channel, uint8_t nsamples )
{
  uint32_t sum = 0;

  for (uint8_t i = 0; i < nsamples; ++i ) {
    sum += ADC_Read( channel );
  }

  return (uint16_t)( sum / nsamples );
}

#define MPPT_STEP 2
uint16_t volt;
uint16_t current;
uint32_t power;
uint32_t l_power;
uint8_t updown = 0;
void mppt_PO(void) {
    current = ADC_Read_Avg(6, 50) - 500;  // Kanal 2, Mittelwert aus 4 Messungen
    volt = ADC_Read_Avg(7, 50);
    power = volt * current;
    if(power < l_power)
        updown ^= 1;
    if(!updown) {
        OCR1A -= MPPT_STEP;
        uart_puts("down\n\r");
        if (OCR1A < 7) {
            OCR1A = 8;
            updown = 1;
        }
    } else {
        OCR1A += MPPT_STEP;
        uart_puts("up\n\r");
        if (OCR1A > 250) {
            OCR1A = 250;
            updown = 0;
        }
    }
    l_power = power;
}

int main() {
    uart_init();
    DDRB |= 1 << PB5;   // LED
    DDRB |= 1 << PB1;   // PWM

    PORTB &= ~(1 << PB0);   // disable IR2184

    TCCR1A = (1 << COM1A1) | (0 << COM1A0) | (0 << COM1B1) | (0 << COM1B0) | (0 << WGM11) | (1 << WGM10);
    TCCR1B = (0 << ICNC1) | (0 << ICES1) | (0 << WGM13) | (1 << WGM12) | (1 << CS10);
    TCCR1C = (0 << FOC1A) | (0 << FOC1B);
    OCR1A = 8;

    PORTB |= 1 << PB0;  // !SD, enable IR2184


    ADC_Init();

    uint16_t alt = 0;
    uint8_t inc = 1;
    while (1) {
        PORTB ^= (1 << PB5);
//        OCR1A++;
        uint8_t c = uart_receive_non_blocking();
        if (c == '+')
            uart_puts("startup\n\r");
        if (c == '-')
            uart_puts("poweroff\n\r");

        mppt_PO();
        uart_puts(itoa(OCR1A, buf, 10));
        uart_puts("   c: ");
        uart_puts(itoa(current, buf, 10));
        uart_puts("   v: ");
        uart_puts(itoa(volt, buf, 10));
        uart_puts("   p: ");
        uart_puts(ltoa(power, buf, 10));
        uart_puts("\n\r");
        _delay_ms(500);
    }

/*    while(1) {
        if(T0IF) {  // Timer 0 overflow interrupt flag
            T0IF = 0;
            if(but_cnt)
                but_cnt--;
            if(track && mppt_calc)
                mppt_calc--;
            if(second)
                second--;

            read_ADC();

            if(!track) {
                if(battery_state != FAULT) {
                    cc_cv_mode();
                    if(!cmode)
                        pid(vout, vref);
                    else
                        pid(iout, iref);
                    if(increment >= dmax)
                        track = TRACK_DELAY;
                }
            } else {
                if(mppt_calc < MPPT_AVERAGE) {
                    f_vin += vin;
                    f_iin += iin;
                }
                if(!mppt_calc) {
                    mppt_calc = MPPT_INTERVAL;
                    #ifdef MPPT_PO mppt_PO();
                    #endif
                    #ifdef MPPT_INCCOND mppt_INCCOND();
                    #endif
                }
                pid(vinref, vin);
                if(vout > vref || iout > iref) {
                    track--;
                    dmax = increment;
                    if(!track)
                        Init_State_Machine();
                } else
                    track = TRACK_DELAY;
            }
            if(!second) {
                second = SECOND_COUNT;
                if(!track)
                    Battery_State_Machine();
            }
        }
    }
    }
*/



    // initialisiere OLED Display
//    init_ssd1306();
//    clear();
//    uint16_t x = 0;
//    while (1) {
//        setCursor(0, 0);
//        itoa(x, buf, 10);
//        oled_print(buf);
//        x++;
//    }

    return 0;
}






/*
void mppt_PO(void) {
    power = (long) f_vin * (long) f_iin;
    if(power < l_power)
        updown ^= 1;
    if(!updown)
        vinref -= MPPT_STEP;
    else
        vinref += MPPT_STEP;
    l_power = power;
    f_vin = 0;
    f_iin = 0;
}*/


/*
void mppt_INCCOND(void) {
    long delta_i;
    long delta_v;
    long delta_p;
    power = (long) f_iin * f_vin;
    delta_p = power - l_power;
    delta_i = (long) f_iin - fl_iin;
    delta_v = (long) f_vin - fl_vin;
    if(delta_v) {
        ineq = delta_p / delta_v;
        if(ineq > 0)
            vinref += MPPT_STEP;
        else if(ineq < 0)
            vinref -= MPPT_STEP;
    } else {
        if(delta_i > 0)
            vinref += MPPT_STEP;
        else if(delta_i < 0) vinref -= MPPT_STEP;
    }
    fl_iin = f_iin;
    fl_vin = f_vin;
    l_power = power;
    f_iin = 0;
    f_vin = 0;
}*/
