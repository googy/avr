#include <avr/io.h>
#include <stdlib.h>
#include <util/delay.h>
#include <stdio.h>

#include "i2cmaster.h"
#include "ssd1306.h"

char buf[10];


int main() {
    // initialisiere OLED Display
    init_ssd1306();
    clear();
    uint16_t x = 0;
    while (1) {
        setCursor(0, 0);
        itoa(x, buf, 10);
        oled_print(buf);
        x++;
    }

    return 0;
}
