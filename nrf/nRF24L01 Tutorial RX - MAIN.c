#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdlib.h>
#include <string.h>
#include "spi.h"
#include "wl_module.h"
#include "nRF24L01.h"
#include "uart.h"

//Variablen
volatile uint8_t PTX;			//Global Variable
char itoabuffer[20];

/* TODO
 * reinitialize module if hanging after reset or programming
 * 1) use power down mode (PWR_UP = 0)
 * 2) clear data ready flag and data sent flag in status register
 * 3) flush tx/rx buffer
 * 4) write status register as 0x0e;
 *
 * YA LO SOLUCIONE : AGREGE ESTO A LA CONFIGURACION INICIAL
 * output_low(RF_CS); spi_write(0xE1); //FLUSH_TX output_high(RF_CS);
 * output_low(RF_CS); spi_write(0xE2); //FLUSH_RX output_high(RF_CS);
 * SUERTE
 */


int main(void) {
	uart_init();
	uint8_t payload[wl_module_PAYLOAD];		//holds the payload
	uint8_t nRF_status;						//STATUS information of nRF24L01+
	uint8_t zaehler = 0;

	wl_module_init();		//Init nRF Module
	_delay_ms(50);			//wait for Module
	sei();					//activate Interrupts
	wl_module_config();		//config nRF as RX Module, simple Version

	DDRD |= (1 << PD4);

	while(1) {
		while (!wl_module_data_ready());			//waits for RX_DR Flag in STATUS
		nRF_status = wl_module_get_data(payload);	//reads the incomming Data to Array payload
		zaehler = payload[0];
		if (zaehler == 179) {
			PORTD ^= (1 << PD4);
		}
//		itoa(zaehler, itoabuffer, 10);				//conversion into String
//		uart_puts(itoabuffer);
//		uart_putc(10);
//		uart_putc(13);
	}
}
