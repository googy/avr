/*
 * uart.c
 *
 *  Created on: 03.07.2013
 *      Author: googy
 */
#include <avr/io.h>
#include "uart.h"

void uart_init() {	// USART init
	UBRR0H = UBRR_VAL >> 8;
	UBRR0L = UBRR_VAL & 0xFF;

	UCSR0B |= (1 << TXEN0); // UART TX einschalten
	//UCSR0C = (1 << URSEL) | (1 << UCSZ1) | (1 << UCSZ0); // Asynchron 8N1
}

void uart_putc(unsigned char c) {
	while (!(UCSR0A & (1 << UDRE0))) {
	} /* warten bis Senden moeglich */
	UDR0 = c; /* sende Zeichen */
}

void uart_puts(char *s) {
	while (*s) { /* so lange *s != '\0' also ungleich dem "String-Endezeichen(Terminator)" */
		uart_putc(*s);
		s++;
	}
}
