#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdlib.h>
#include "spi.h"
#include "wl_module.h"
#include "nRF24L01.h"
#include "uart.h"

volatile uint8_t timercounter;

int main(void) {
	uint8_t payload[wl_module_PAYLOAD];		//Array for Payload
	uint8_t maincounter =0;
	uint8_t k;

	wl_module_init();	//initialise nRF24L01+ Module
	_delay_ms(50);		//wait for nRF24L01+ Module
	sei();

	wl_module_tx_config(wl_module_TX_NR_0);		//Config Module

	while(1) {
		_delay_ms(500);
		payload[0] = 179;
		wl_module_send(payload,wl_module_PAYLOAD);
	}
}

ISR(INT0_vect) {	// Interrupt handler
	uint8_t status;
	// Read wl_module status
	CSN_PORT &= ~(1 << CSN);			// Pull down chip select
	status = spi_fast_shift(NOP);		// Read status register
	CSN_PORT |= (1 << CSN);				// Pull up chip select

	if (status & (1<<TX_DS)) {			// IRQ: Package has been sent
		wl_module_config_register(STATUS, (1<<TX_DS));	//Clear Interrupt Bit
		PTX=0;
	}

	if (status & (1<<MAX_RT)) {			// IRQ: Package has not been sent, send again
		wl_module_config_register(STATUS, (1<<MAX_RT));	// Clear Interrupt Bit
		CSN_PORT |= (1 << CSN);			// Start transmission
		_delay_us(10);
		CE_PORT &= ~(1 << CE);
	}

	if (status & (1<<TX_FULL)) {		// TX_FIFO Full <-- this is not an IRQ
		CSN_PORT &= ~(1 << CSN);		// Pull down chip select
		spi_fast_shift(FLUSH_TX);		// Flush TX-FIFO
		CSN_PORT |= (1 << CSN);			// Pull up chip select
	}
}
