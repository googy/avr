#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include "spi.h"
#include "uart.h"
#include "nrf.h"

put init parameters in eep
put all setup parameters in eep
clear fifo

int main(void) {
	uint8_t payload[1];		//holds the payload
	uart_init();
	uart_puts("uart initialized\n\r");
	nrf_init();
	uart_puts("nrf initialized\n\r");

	DDRD |= (1 << PD5);

	char itoabuffer[20];

	uint8_t dat = 0;
	for (uint8_t i = 0x0; i < 0x18; i++) {
		wl_module_read_register(i, &dat, 1);
		itoa(i, itoabuffer, 16);
		uart_puts(itoabuffer);
		uart_puts("      ");
		itoa(dat, itoabuffer,2);
		uart_puts(itoabuffer);
		uart_putc(10);
		uart_putc(13);
	}

	//wl_module_config_register(0, 0b10);			// poweroff

	while(1) {
		while (!wl_module_data_ready());			//waits for RX_DR Flag in STATUS
		uart_puts("got data\n\r");
		wl_module_get_data(payload);				//reads the incomming Data to Array payload
		itoa(payload[0], itoabuffer, 10);			//conversion into String
		uart_puts(itoabuffer);
		uart_putc(10);
		uart_putc(13);
	}
}

ISR(INT0_vect) {
	//write 1 to status RX_DR to clear interrupt
	PORTD ^= (1 << PD5);
	// Empfang signalisieren, alle RX_PW_P0 Register abklappern und alle Daten abholen


//	uint8_t status;
//
//	// Read wl_module status
//	PORTB &= ~(1 << CSN);								// Pull down chip select
//	status = spi_fast_shift(NOP);						// Read status register
//	PORTB |= (1 << CSN);								// Pull up chip select
//
//	if (status & (1<<TX_DS))	{						// IRQ: Package has been sent
//		wl_module_config_register(STATUS, (1<<TX_DS));	//Clear Interrupt Bit
//		PTX = 0;
//	}
//
//	if (status & (1<<MAX_RT))	{						// IRQ: Package has not been sent, send again
//		wl_module_config_register(STATUS, (1<<MAX_RT));	// Clear Interrupt Bit
//		PORTB |= (1 << CE);								// Start transmission
//		_delay_us(10);
//		PORTB &= ~(1 << CE);
//	}
//
//	if (status & (1<<TX_FULL))	{						//TX_FIFO Full <-- this is not an IRQ
//		PORTB &= ~(1 << CSN);							// Pull down chip select
//		spi_fast_shift(FLUSH_TX);						// Flush TX-FIFO
//		PORTB &= ~(1 << CSN);							// Pull up chip select
//	}
}
