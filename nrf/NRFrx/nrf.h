/*
 * nrf.h
 *
 *  Created on: 02.10.2014
 *      Author: googy_000
 */

#ifndef NRF_H_
#define NRF_H_

#define CE  PB0
#define CSN PB1

struct {
	uint8_t x;
	uint8_t y;
} nrf_data;

void nrf_init();
void wl_module_config_register(uint8_t reg, uint8_t value);
uint8_t wl_module_data_ready();
uint8_t wl_module_get_data(uint8_t * data);
uint8_t getStatus();
void wl_module_read_register(uint8_t reg, uint8_t * value, uint8_t len);

#endif /* NRF_H_ */
