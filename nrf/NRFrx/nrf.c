/*
 * nrf.c
 *
 *  Created on: 02.10.2014
 *      Author: googy_000
 */
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "nrf.h"
#include "nRF24L01.h"
#include "spi.h"

struct nrfConfig {
	uint8_t config;
	uint8_t en_aa;
};

extern void nrf_init() {
	DDRB |= ((1<<CSN)|(1<<CE));		// config CE, CSN
	PORTB &= ~(1<<CE);
	PORTB |=  (1<<CSN);

	MCUCR = ((1<<ISC11)|(0<<ISC10)|(1<<ISC01)|(0<<ISC00));	// Set external interupt on falling edge
	GICR  = ((0<<INT1)|(1<<INT0));							// Activate INT0

	spi_init();				// max. 4MHz, 1MHz eingestellt
	_delay_ms(50);			// wait for Module
	sei();					// activate Interrupts

	wl_module_config_register(CONFIG, 0);			// poweroff
	_delay_ms(100);
	wl_module_config_register(EN_AA, 1);			// enable autoACK fpr pipe 0
	wl_module_config_register(EN_RXADDR, 1);		// nur pipe 0 aktivieren
	wl_module_config_register(SETUP_AW, 0b11);		// 5Byte adressbreite
	wl_module_config_register(SETUP_RETR, 0b11);	// 3 retransmits, 250us wait
	wl_module_config_register(RF_CH, 0x02);			// set Channel 2
	wl_module_config_register(RF_SETUP, (RF_SETUP_RF_PWR_0 | RF_SETUP_RF_DR_250));	// 0dBm, 250kbps
	wl_module_config_register(STATUS, 0b1110000);	// clear all interrupts
	wl_module_config_register(RX_PW_P0, 1);			// use pipe0 with 1Byte length
	wl_module_config_register(RX_PW_P1, 0x0);		// 1 Byte payload
	wl_module_config_register(RX_PW_P2, 0x0);		// pipe not used
	wl_module_config_register(RX_PW_P3, 0x0);		// pipe not used
	wl_module_config_register(RX_PW_P4, 0x0);		// pipe not used
	wl_module_config_register(RX_PW_P5, 0x0);		// pipe not used

	// Start receiver
	wl_module_config_register(CONFIG,
			(0 << MASK_RX_DR) |	// deactivate RX_DR on IRQ
			(1 << EN_CRC) |		// enable CRC
			(1 << CRCO) |		// 2Byte CRC
			(1 << PWR_UP) |		// power up module
			(1 << PRIM_RX));	// PRX, Receiver mode
	PORTB |=  (1<<CE);
}

extern void wl_module_config_register(uint8_t reg, uint8_t value){
	PORTB &= ~(1<<CSN);
	spi_fast_shift(W_REGISTER | (REGISTER_MASK & reg));
	spi_fast_shift(value);
	PORTB |=  (1<<CSN);
}

extern uint8_t wl_module_data_ready() {
	uint8_t status;
	PORTB &= ~(1<<CSN);
	status = spi_fast_shift(NOP);						// Read status register
	PORTB |=  (1<<CSN);
	return status & (1<<RX_DR);
}

extern uint8_t wl_module_get_data(uint8_t * data) {
	uint8_t status;
	PORTB &= ~(1<<CSN);									// Pull down chip select
	status = spi_fast_shift( R_RX_PAYLOAD );			// Send cmd to read rx payload
	spi_transfer_sync(data,data, 1);					// Read payload
	PORTB |=  (1<<CSN);									// Pull up chip select
	wl_module_config_register(STATUS,(1<<RX_DR));		// Reset status register
	return status;
}

extern uint8_t getStatus() {
	uint8_t status;
	PORTB &= ~(1 << CSN);
	status = spi_fast_shift(NOP);
	PORTB |= (1 << CSN);
	return status;
}

extern void wl_module_read_register(uint8_t reg, uint8_t * value, uint8_t len) {
	PORTB &= ~(1 << CSN);
	spi_fast_shift(R_REGISTER | (REGISTER_MASK & reg));
	spi_transfer_sync(value,value,len);
	PORTB |= (1 << CSN);
}
