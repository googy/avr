#include "spi.h"
#include <avr/io.h>
#include <avr/interrupt.h>

#define PORT_SPI	PORTB
#define DDR_SPI		DDRB
#define DD_MISO		DDB4
#define DD_MOSI		DDB3
#define DD_SS		DDB2
#define DD_SCK		DDB5

// Initialize pins for spi communication
void spi_init() {
	//TODO reset pullup on MISO
	// define spi pins as input
	DDR_SPI &= ~((1<<DD_MOSI)|(1<<DD_MISO)|(1<<DD_SS)|(1<<DD_SCK));
	// Define the following pins as output
	DDR_SPI |= ((1<<DD_MOSI)|(1<<DD_SS)|(1<<DD_SCK));

	// main clock 16MHz, divider = 16 (SPI2x = 0, SPR1 = 0, SPR0 = 1), result 1MHz SPI clock
	SPCR = ((1<<SPE)|				// SPI Enable
			(0<<SPIE)|				// SPI Interupt Enable
			(0<<DORD)|				// Data Order (0:MSB first / 1:LSB first)
			(1<<MSTR)|				// Master/Slave select
			(0<<SPR1)|(1<<SPR0)|	// SPI Clock Rate
			(0<<CPOL)|				// Clock Polarity (0:SCK low / 1:SCK hi when idle)
			(0<<CPHA));				// Clock Phase (0:leading / 1:trailing edge sampling)

	SPSR = (0<<SPI2X);				// Double Clock Rate
}

// Shift full array through target device
void spi_transfer_sync (uint8_t * dataout, uint8_t * datain, uint8_t len) {
	uint8_t i;
	for (i = 0; i < len; i++) {
		SPDR = dataout[i];				// send byte
		while((SPSR & (1<<SPIF))==0);	// wait byte is sent
		datain[i] = SPDR;				// read answer
	}
}

// Shift full array to target device without receiving any byte
void spi_transmit_sync (uint8_t * dataout, uint8_t len) {
	uint8_t i;
	for (i = 0; i < len; i++) {
		SPDR = dataout[i];				// send byte
		while((SPSR & (1<<SPIF))==0);	// wait byte is sent
	}
}

// Clocks only one byte to target device and returns the received one
uint8_t spi_fast_shift (uint8_t data) {
	SPDR = data;						// send byte
	while((SPSR & (1<<SPIF))==0);		// wait byte is sent
	return SPDR;
}
