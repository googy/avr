#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdlib.h>
#include "spi.h"
#include "nRF24L01.h"

// Pin definitions for chip select and chip enabled of the wl-module
#define CE  PB0
#define CSN PB1

// Flag which denotes transmitting mode
volatile uint8_t PTX;

void wl_module_config_register(uint8_t reg, uint8_t value) {	// Clocks only one byte into the given wl-module register
	PORTB &= ~(1 << CSN);
	spi_fast_shift(W_REGISTER | (REGISTER_MASK & reg));
	spi_fast_shift(value);
	PORTB |= (1 << CSN);
}

void wl_module_write_register(uint8_t reg, uint8_t * value, uint8_t len) {
// Writes an array of bytes into inte the wl-module registers.
	PORTB &= ~(1 << CSN);
	spi_fast_shift(W_REGISTER | (REGISTER_MASK & reg));
	spi_transmit_sync(value,len);
	PORTB |= (1 << CSN);
}

void wl_module_send(uint8_t * value, uint8_t len) {
// Sends a data package to the default address. Be sure to send the correct
// amount of bytes as configured as payload on the receiver.
	while (PTX) {}					// Wait until last paket is send
	PORTB &= ~(1 << CE);
	PTX = 1;						// Set to transmitter mode
	wl_module_config_register(CONFIG, ( (1<<MASK_RX_DR) | (1<<EN_CRC) | (1<<CRCO) ) | ( (1<<PWR_UP) | (0<<PRIM_RX) ) );	// Power up

	PORTB &= ~(1<<CSN);                    // Pull down chip select
    spi_fast_shift( FLUSH_TX );     // Write cmd to flush tx fifo
    PORTB |=  (1<<CSN);                    // Pull up chip select

    PORTB &= ~(1<<CSN);                    // Pull down chip select
    spi_fast_shift( W_TX_PAYLOAD ); // Write cmd to write payload
    spi_transmit_sync(value,len);   // Write payload
    PORTB |=  (1<<CSN);                    // Pull up chip select

    PORTB |=  (1<<CE);                     // Start transmission
	_delay_us(10);						// Gr�nes Modul funktioniert nicht mit 10�s delay
	PORTB &= ~(1<<CE);
}

int main(void) {
	DDRD |= (1 << PD5) | (1 << PD6) | (1 << PD7);

	// Define CSN and CE as Output and set them to default
	DDRB |= ((1<<CSN)|(1<<CE));
	PORTB &= ~(1 << CE);
	PORTB |= (1 << CSN);

	// Initialize external interrupt 0 (PD2)
	MCUCR = ((1<<ISC11)|(0<<ISC10)|(1<<ISC01)|(0<<ISC00));	// Set external interupt on falling edge
	GICR  = ((0<<INT1)|(1<<INT0));							// Activate INT0

	spi_init();
	_delay_ms(50);		//wait for nRF24L01+ Module
	sei();

	wl_module_config_register(CONFIG, 0);	// PWR_UP bit auf 0 setzen, damit der Chip resettet wird
	// sonst l�uft dieser nach dem Programmieren nicht wieder an
	// PWR_UP -> Power Up bit im CONFIG Register schaltet das Modul ein und aus

	wl_module_config_register(RF_CH, 0x02);	// Set RF channel
	wl_module_config_register(RF_SETUP,(RF_SETUP_RF_PWR_0 | RF_SETUP_RF_DR_250));	// Set data speed & Output Power
	wl_module_config_register(CONFIG, ( (1<<MASK_RX_DR) | (1<<EN_CRC) | (1<<CRCO) ));	//Config the CONFIG Register (Mask IRQ, CRC, etc)
	wl_module_config_register(SETUP_RETR,(SETUP_RETR_ARD_750 | SETUP_RETR_ARC_15));
	//set the TX address for the pipe with the same number as the iteration
	//setup TX address as default RX address for pipe 0 (E7:E7:E7:E7:E7)
	uint8_t tx_addr[5] = {
			RX_ADDR_P0_B0_DEFAULT_VAL,
			RX_ADDR_P0_B0_DEFAULT_VAL,
			RX_ADDR_P0_B0_DEFAULT_VAL,
			RX_ADDR_P0_B0_DEFAULT_VAL,
			RX_ADDR_P0_B0_DEFAULT_VAL};
	// im Empf�nger ist diese Adresse die default adresse f�r pipe0.
	// M�chte man eine andere benutzen so muss man diese auf beiden Seiten �ndern.
	// Die Pakete landen beim Empf�nger in der Pipe 0, da dessen Adresse als Ziel eingestellt ist.
	// TX_ADDR muss gleich RX_ADDR_P0 sein, falls es ein Sender ist, AutoACK und EnhancedShockBurst eingeschaltet ist.

	// set TADDR
	wl_module_write_register(TX_ADDR, tx_addr,5);

	// set RADDR
	PORTB &= ~(1 << CE);
	wl_module_write_register(RX_ADDR_P0,tx_addr,5);
	PORTB |= (1 << CE);

	PTX = 0;
	wl_module_config_register(CONFIG, ( (1<<MASK_RX_DR) | (1<<EN_CRC) | (1<<CRCO) ) | ( (1<<PWR_UP) | (0<<PRIM_RX) ) );

	while(1) {
		_delay_ms(1000);
		PORTD |= (1 << PD5);
		wl_module_send(&PORTD, 1);
		PORTD ^= (1 << PD6);
	}
}

ISR(INT0_vect) {
	uint8_t status;

	// Read wl_module status
	PORTB &= ~(1 << CSN);								// Pull down chip select
	status = spi_fast_shift(NOP);						// Read status register
	PORTB |= (1 << CSN);								// Pull up chip select

	if (status & (1<<TX_DS))	{						// IRQ: Package has been sent
		wl_module_config_register(STATUS, (1<<TX_DS));	//Clear Interrupt Bit
		PTX = 0;
	}

	if (status & (1<<MAX_RT))	{						// IRQ: Package has not been sent, send again
		wl_module_config_register(STATUS, (1<<MAX_RT));	// Clear Interrupt Bit
		PORTB |= (1 << CE);								// Start transmission
		_delay_us(10);
		PORTB &= ~(1 << CE);
	}

	if (status & (1<<TX_FULL))	{						//TX_FIFO Full <-- this is not an IRQ
		PORTB &= ~(1 << CSN);							// Pull down chip select
		spi_fast_shift(FLUSH_TX);						// Flush TX-FIFO
		PORTB &= ~(1 << CSN);							// Pull up chip select
	}
}
