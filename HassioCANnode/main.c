
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>

#include <util/delay.h>
#include <stdio.h>
#include <inttypes.h>

#include <stdlib.h>	// itoa

#include "mcp2515.h"
#include "global.h"
#include "defaults.h"

#include "uart.h"
#include "timebase.h"

tCAN message, rMessage;

char buf[10];

//    runter
//    18
//    25

//    rauf
//    9
//    28

uint8_t ist = 0;
uint8_t soll = 0;

uint8_t state = 0;
uint8_t move = 0;
uint32_t move_timer = 0;
void ccc() {
    switch (state) {
        case 0:         // idle
            if (ist != soll) {
                stoptimer_init(&move_timer);
                state = 1;
                if (ist > soll) {   // oeffne
                    move = ist - soll;
                    PORTC &= ~((1 << PC0) | (1 << PC1));
                    PORTC |= (1 << PC1);
                    uart_puts("open\n\r");
                    break;
                }
                if (ist < soll) {
                    move = soll - ist;
                    PORTC &= ~((1 << PC0) | (1 << PC1));
                    PORTC |= (1 << PC0);
                    uart_puts("close\n\r");
                    break;
                }
            }
            break;
        case 1:         // moving
            if (stoptimer_expired(&move_timer, ((uint16_t) move) * 200)) {
                ist = soll;
                PORTC &= ~((1 << PC0) | (1 << PC1));
                uart_puts("done\n\r");
                state = 0;
            }
            break;
        default:
            break;
    }
}

int main(void) {

    PORTC &= ~((1 << PC0) | (1 << PC1) | (1 << PC2));
    DDRC |= (1 << PC0) | (1 << PC1) | (1 << PC2);

    timer0_init();
    uart_init();

    sei();	// f�r MCP Interrupts

    DDRB |= 1 << PB2;		// !!!!!!!!!!!!!!!!!!!!! SS to output, sonst keine SPI Kommunikation

    /* NiRen MCP2515_CAN modul von alice aus China
     * Reset per pullup an VCC
     * 5V
     * CS an B1
     * INT an D2
     * 8MHz
     * 125kbps
     */



    // Versuche den MCP2515 zu initilaisieren
    if (!mcp2515_init()) {
        uart_puts("MCP2515 FAIL\n\r");
        return 0;
    } else {
        uart_puts("MCP2515 OK\n\r");
    }

    uint32_t timecount = 0;


    // fahre in definierte Position
//    PORTC |= 1 << PC1;
//    stoptimer_init(&timecount);
//    while (!stoptimer_expired(&timecount, 30000));
    ist = 0;
    uart_puts("vollstaendig geoeffnet\n\r");



    stoptimer_init(&timecount);
    while (1) {
        _delay_ms(2000);
        // prepare message - Abgastemperatur
        message.id = 0x123;
        message.header.rtr = 0;
        message.header.length = 8;
        message.data[0] = 0;
        message.data[1] = 0;
        message.data[2] = 0;
        message.data[3] = 0;
        message.data[4] = 0;
        message.data[5] = 0;
        message.data[6] = 0;
        message.data[7] = 0;

        // send message
        if (mcp2515_send_message(&message)) {
            uart_puts("Nachricht konnte gesendet werden\n\r");
        } else {
            uart_puts("ERROR Nachricht konnte nicht gesendet werden\n\r");
        }

        // check for incoming messages
        if (mcp2515_check_message()) {		// Nachricht vorhanden
            if (mcp2515_get_message(&rMessage)) {		// Nachricht erfolgreich angekommen
                uart_puts("ID: ");
                itoa((rMessage.id), buf, 16);
                uart_puts(buf);
                uart_puts("   DLC: ");
                itoa((rMessage.header.length), buf, 10);
                uart_puts(buf);
                uart_puts("   Data: ");
                for (uint8_t i = 0; i < rMessage.header.length; i++) {
                    itoa((rMessage.data[i]), buf, 16);
                    uart_puts(buf);
                    uart_putc(' ');
                }
                uart_puts("\n\r");

                if (rMessage.id == 0x17) {
                    soll = rMessage.data[0];
                }
            } else {
                uart_puts("FAIL\n\r");
            }
        } else {
            //uart_puts("keine Nachrichterhalten");
        }
        ccc();
    }

    return 0;
}
