/*
 * tastatur.h
 *
 *  Created on: 21.09.2014
 *      Author: googy_000
 */

#ifndef TASTATUR_H_
#define TASTATUR_H_

#define C1	PB0
#define C2	PB1
#define C3	PB2

#define R1	PC0
#define R2	PC1
#define R3	PC2
#define R4	PC3

struct ringbuffer {
	uint8_t head;
	uint8_t tail;
	uint8_t size;
	uint8_t data[10];
};

void tastatur_init();
uint16_t getKeys();
uint8_t getKey();
void entprellen(uint16_t keys);

#endif /* TASTATUR_H_ */
