/*
 * main.c
 *
 *  Created on: 31.01.2015
 *      Author: googy_000
 */

#include <avr/io.h>
#include <util/delay.h>
#include <stdlib.h>
#include "uart.h"
#include "tastatur.h"

int main() {
	uart_init();
//	// deaktiviere Spalten
//	DDRB &= ~((1 << PB1) | (1 << PB2));	// als Eingang
//	PORTB |= (1 << PB1) | (1 << PB2);	// pull-ups
//
//	// Spalte aktivieren
//	DDRB = (1 << PB0);	// als Ausgang
//	PORTB &= ~(1 << PB0);	// low
//
//	// setup Zeilen
//	DDRC &= ~((1 << PC0) | (1 << PC1) | (1 << PC2) | (1 << PC3));	// als Eingang
//	PORTC |= (1 << PC0) | (1 << PC1) | (1 << PC2) | (1 << PC3);	// pull up

	uint8_t state = 0;
	//uint16_t keys = 0;
	uint8_t code[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	uint8_t dcode[] = {3, 9, 11, 1, 2, 10};
	uint8_t cl = 6;
	uint8_t dig = 0;
	uint8_t key = 0;

	char buf[10];

	tastatur_init();

	while (1) {
		entprellen(getKeys());
		switch (state) {
		case 0:// werte auf eingabe
			// falls l�nger als 3 sec in state 0 -> rerset code
			key = getKey();
			if (key) {
				state = 1;
			}
			break;
		case 1:
			if ((key == 12) || (dig > 8)) {
				state = 2;
				break;
			}
			code[dig] = key;
			dig++;
			state = 0;
			break;
		case 2:
			for (uint8_t i = 0; i < dig; i++) {	// gebe eingabe aus
				itoa(code[i], buf, 10);
				uart_puts(buf);
				uart_putc(' ');
			}
			uart_putc(10);
			uart_putc(13);
			if(dig == cl) {// l�nge passt, pr�fe eingabe
				for (uint8_t i = 0; i < cl; i++) {
					if (code[i] == dcode[i]) {
						uart_putc('1');
					} else {
						uart_putc('0');
					}
				}
			}
			uart_putc(10);
			uart_putc(13);
			dig = 0;
			state = 0;
			break;
		default:
			break;
		}
	}
}
