/*
 * tastatur.c
 *
 *  Created on: 21.09.2014
 *      Author: googy_000
 */
#include <avr/io.h>
#include "tastatur.h"

static uint8_t timeouts[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
static uint16_t entprellt = 0;
static struct ringbuffer rbuf;

uint8_t add(uint8_t key) {
	uint8_t next_head = (rbuf.head + 1) % rbuf.size;
	if (next_head != rbuf.tail) {	// buffer not full
		rbuf.data[rbuf.head] = key;
		rbuf.head = next_head;
		return 1;
	} else {
		return 0;
	}
}

uint8_t getKey() {
	if (rbuf.tail != rbuf.head) {
		uint8_t t = rbuf.data[rbuf.tail];
		rbuf.tail = (rbuf.tail + 1) % rbuf.size;
		return t;
	}
	return 0;
}

void tastatur_init() {
	// cols
	DDRB |= (1 << C1) | (1 << C2) | (1 << C3);		// als Ausgang
	PORTB |= (1 << C1) | (1 << C2) | (1 << C3);		// auf HIGH

	// rows
	DDRC &= ~((1 << R1) | (1 << R2) | (1 << R3) | (1 << R4));	// als Eingang
	PORTC |= (1 << R1) | (1 << R2) | (1 << R3) | (1 << R4);	// pull up

	rbuf.head = 0;
	rbuf.tail = 0;
	rbuf.size = 0;
}

uint16_t getKeys() {
	uint16_t k = 0;
	PORTB &= ~(1 << C1);
	asm volatile ("nop");
	k |= ~PINC & 0b1111;
	PORTB |= (1 << C1);

	PORTB &= ~(1 << C2);
	asm volatile ("nop");
	k |= (~PINC & 0b1111) << 4;
	PORTB |= (1 << C2);

	PORTB &= ~(1 << C3);
	asm volatile ("nop");
	k |= (~PINC & 0b1111) << 8;
	PORTB |= (1 << C3);
	return k;
}

void entprellen(uint16_t keys) {// Eingaben entprellen
	for (uint8_t i = 0; i < 12; i++) {
		if (keys & (1 << i)) {	// Taste bet�tigt
			if (timeouts[i] < 0xFF) {
				timeouts[i]++;
				if (timeouts[i] >= 0xFF) {	// > redundant
					if (!(entprellt & (1 << i))) {
						entprellt |= (1 << i);
						add(i + 1);
					}
					timeouts[i] = 0xFF;	// redundant
				}
			}
		} else {	// Taste nicht bet�tigt
			if (timeouts[i]) {
				timeouts[i]--;
				if (!timeouts[i]) {
					entprellt &= ~(1 << i);
				}
			}
		}
	}
}
