#include <avr/io.h>
#include <util/delay.h>
#include <stdlib.h>
#include <avr/interrupt.h>
#include "hd44780.h"
#include "ow.h"

#define STROBE PD3
#define DATA PD4
#define CLOCK PD5

#define S0 8
#define S1 9
#define S2 10
#define T5 11
#define COMP PD6

uint16_t shift = 0;

// Messergebnis ADC
volatile uint16_t mval = 0;
volatile uint8_t flag = 0;


void shift_init() {
	DDRD |= (1 << STROBE) | (1 << DATA) | (1 << CLOCK);
	PORTD &= ~((1 << STROBE) | (1 << DATA) | (1 << CLOCK));
}

void shift_set(uint16_t v){
	PORTD &= ~((1 << DATA) | (1 << CLOCK) | (1 << STROBE));
	for (uint8_t i = 0; i < 16; i++){
		PORTD &= ~((1 << DATA) | (1 << CLOCK));		// aufr�umen
		if (1 & (v >> (15 - i))) {
			PORTD |= (1 << DATA);					// setze 1
		}
		PORTD |= (1 << CLOCK);						// einshiften
	}
	PORTD &= ~((1 << DATA) | (1 << CLOCK));			// aufr�umen

	PORTD |= (1 << STROBE);
	PORTD &= ~(1 << STROBE);
}

void icp_init() {
	// Timer1 initialisieren:
	TCNT1L = 0;
	TCNT1H = 0;
	 TCCR1A = 0;                      // normal mode, keine PWM Ausg�nge
	 TCCR1B = (1 << ICNC1) + (1<<CS10) + (1<<CS12)   //noise canceler, prescaler 1024 64�s
	          + (0 << ICES1);            // fallende Flanke ausw�hlen
	 TIFR = 0;
	 TIMSK |= (1<<TICIE1);   // Input-capture interrupt aktivieren, Mega32: TIMSK
}

// Timer f�r ADC Messung
void timer0_init_int_128us(){
	TCCR0 = 0;
	TCCR0 = (1 << CS01);	// prescaler 8 >> 0,5us
	TCNT0 = 0;				// Timer zur�cksetzen
	TIMSK |= (1 << TOIE0);	// Overflow Interrupt enable	128us Overflow
}

// ADC Messung
void meassure(){
	shift |= (1 << T5);
	shift_set(shift);		// Kondensator Entladetransistor T5 einschalten
	mval = 0;				// Z�hler initialisieren
	_delay_ms(100);			// warten bis Kondensator weitgehend entladen ist
	shift &= ~(1 << T5);
	timer0_init_int_128us();
	shift_set(shift);		// Kondensator laden
	_delay_ms(100);			// warte auf Messung, feste Wartezeit, damit bei einer endlosen Messung kein deadlock
	TCCR0 = 0;				// Timer anhalten
	//TIMSK &= ~(TOIE0);		// overflow Interupt deaktivieren
}

uint8_t convert_mt(uint16_t mes){
	uint32_t h = 14000000/mes;		// Vergleichswert Messung
	uint32_t j = 1;					// Vergleichswert Berechnung
	uint8_t temper = 0;
	while (h > j){					// N�herung des Logarithmus
		j *= 103;
		if (j > 1000000){
			j /= 100;
		}
		temper++;
	}
	return temper - 1;
}

void printT(uint16_t t) {
	char s[10];
	uint16_t nk = 0;
	utoa((t >> 4), s, 10);
	hd44780_puts(s);
	hd44780_puts(".");
	if (t & (1 << 3)) {
		nk = 50;
	}
	if (t & (1 << 2)) {
		nk += 25;
	}
	if (t & (1 << 1)) {
		nk += 12;
	}
	if (t & (1 << 0)) {
		nk += 6;
	}
	if (nk < 10) {
		hd44780_puts("0");
	}
	utoa(nk, s, 10);
	hd44780_puts(s);
}


int main() {
	shift_init();
	// ADC eingang
	DDRD &= ~(1 << COMP);

	// Init LCD
	_delay_ms(30);
	hd44780_init();
	hd44780_mode(1,0,0);
	hd44780_clear();

	sei();


	char test[12];

	while (1) {
		shift &= ~((1 << S0) | (1 << S1) | (1 << S2));
		shift_set(shift);

		shift |= (1 << T5);
		shift_set(shift);		// Kondensator Entladetransistor T5 einschalten
		mval = 0;				// Z�hler initialisieren
		_delay_ms(100);			// warten bis Kondensator weitgehend entladen ist

		shift &= ~(1 << T5);
		shift_set(shift);		// Kondensator laden
		icp_init();

		while (!flag) {
			_delay_ms(100);
		}
		flag = 0;
		ultoa(mval, test, 10);


			ow_reset();				//reset
			//ow_write(0x55);			//MATCH ROM [55h]
			//send_id(id1);			//send ROM id
			ow_write(0xCC);		//SKIP ROM [CCh]
			ow_write(0xBE);			//READ SCRATCHPAD [BEh]

			uint16_t tt = ow_read(16);//read 2 bytes

			ow_reset();				//reset
			ow_write(0xCC);			//SKIP ROM [CCh]
			ow_write(0x44);			//CONVERT T [44h]


		hd44780_clear();
		printT(tt);
		hd44780_puts(" ");
		hd44780_puts(test);

		hd44780_puts(" ");
		ultoa(convert_mt(mval), test, 10);
		hd44780_puts(test);
		_delay_ms(1000);
	}
}

ISR(TIMER1_CAPT_vect)      //  Flanke an ICP pin
{
	TCCR1B = 0;		// timer stoppen

	mval = ICR1L;         // low Byte zuerst, high Byte wird gepuffert
    mval |= (ICR1H << 8);
    ICR1L = 0;
    ICR1H = 0;
    flag = 0xFF;
    TIMSK &= ~(1<<TICIE1);
}


