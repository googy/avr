/*
 * main.c
 *
 *  Created on: 23.08.2013
 *      Author: googy
 */
#include <avr/io.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>

#include "main.h"
#include "uart.h"
#include "timer.h"
#include "tastatur.h"

uint8_t getPressedKey() {
	uint8_t r = 255;
	for (uint8_t i = 0; i < 12 && ~r; i++) {
		if (k[i]) {
			return i;
		}
	}
	return r;
}

uint8_t code[6] = {7, 3, 9, 1, 4, 6};

int main() {
	// init LED
	DDRA |= (1 << PA0);
	PORTA &= ~(1 << PA0);

	uart_init();
	init_tastatur();
	init_timer0();
	sei();

	uint8_t state = 0;

	while (1) {
		if (getPressedKey() == 7) {
			PORTA |= (1 << PA0);
		} else {
			PORTA &= ~(1 << PA0);
		}
		//		if (keys[1][1]) {
		//			PORTA |= (1 << PA0);
		//		} else {
		//			PORTA &= ~(1 << PA0);
		//		}
	}
}
