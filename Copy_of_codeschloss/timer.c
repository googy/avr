/*
 * timer.c
 *
 *  Created on: 24.08.2013
 *      Author: googy
 */

#include <avr/interrupt.h>

#include "timer.h"
#include "tastatur.h"

void init_timer0() {
	TCCR0A = 0;
	TCCR0B = (1 << CS02) | (0 << CS01) | (1 << CS00);
	TIMSK = (1 << TOIE0);
}

ISR(TIMER0_OVF_vect) {
	for (uint8_t i = 0; i < 12; i++) {
		uint8_t port = ((i % 3) + 3);
		PORTD |= (1 << port);
		if (PINB & (1 << (i / 3))) {
			k[i] = 0xFF;
		} else {
			k[i] = 0x00;
		}
		PORTD &= ~(1 << ((i % 3) + 3));
	}
//	for (uint8_t x = PD3; x <= PD5; x++) {
//		PORTD |= (1 << x);
//		for (uint8_t y = PB0; y <= PB3; y++) {
//			if (PINB & (1 << y)) {
//				keys[y][x - PD3] = 0xFF;
//			} else {
//				keys[y][x - PD3] = 0x00;
//			}
//		}
//		PORTD &= ~(1 << x);
//	}
}
