/*
 * tastatur.c
 *
 *  Created on: 24.08.2013
 *      Author: googy
 */

#include <avr/io.h>

void init_tastatur() {
	// init Spalten
	DDRD |= (1 << PD3) | (1 << PD4) | (1 << PD5);	// X1 X2 X3
	PORTD &= ~((1 << PD3) | (1 << PD4) | (1 << PD5));

	// init Zeilen
	DDRB &= ~((1 << PB0) | (1 << PB1) | (1 << PB2) | (1 << PB3));	// Y1 Y2 Y3 Y4
	PORTB &= ((1 << PB0) | (1 << PB1) | (1 << PB2) | (1 << PB3));	// pull ups deaktivieren, active high
	//PORTB |= (1 << PB0) | (1 << PB1) | (1 << PB2) | (1 << PB3);		// pull ups
}
