################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/main.c \
../src/ssd1306.c \
../src/twimaster.c \
../src/uart.c \
../src/wbus.c 

OBJS += \
./src/main.o \
./src/ssd1306.o \
./src/twimaster.o \
./src/uart.o \
./src/wbus.o 

C_DEPS += \
./src/main.d \
./src/ssd1306.d \
./src/twimaster.d \
./src/uart.d \
./src/wbus.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -Os -fpack-struct -fshort-enums -ffunction-sections -fdata-sections -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega328p -DF_CPU=16000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


