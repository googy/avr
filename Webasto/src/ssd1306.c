/*
 * ssd1306.c
 *
 *  Created on: 10.05.2015
 *      Author: googy_000
 */
#include <util/delay.h>
#include <avr/pgmspace.h>
#include "ssd1306.h"
#include "i2cmaster.h"
//#include "fonts.h"


/*
void sendCommand(uint8_t command) {
	i2c_start(DevSSD1306);
	i2c_write(0x00);
	i2c_write(command);
	i2c_stop();
}

void init_ssd1306() {
	i2c_init();

	i2c_start(DevSSD1306 + I2C_WRITE);	// set device address and write mode

	// Init sequence for 128x64 OLED module
//	i2c_write(0xAE);                    // Display Off
//
//    i2c_write(0xAE);                    // Display
//	i2c_write(0x00 | 0x0);            // low col = 0
//	i2c_write(0x10 | 0x0);           // hi col = 0
//	i2c_write(0x40 | 0x0);            // line #0
//
//	i2c_write(0x81);                   // Set Contrast 0x81
//	i2c_write(0xCF);
//	// flips display
//	i2c_write(0xA1);                    // Segremap - 0xA1
//	i2c_write(0xC8);                    // COMSCAN DEC 0xC8 C0
//	i2c_write(0xA6);                    // Normal Display 0xA6 (Invert A7)
//
//	i2c_write(0xA4);                // DISPLAY ALL ON RESUME - 0xA4
//	i2c_write(0xA8);                    // Set Multiplex 0xA8
//	i2c_write(0x3F);                    // 1/64 Duty Cycle
//
//	i2c_write(0xD3);                    // Set Display Offset 0xD3
//	i2c_write(0x0);                     // no offset
//
//	i2c_write(0xD5);                    // Set Display Clk Div 0xD5
//	i2c_write(0x80);                    // Recommnedi2c_write(pgm_read_byte(&(font[((*s - 25) * 5) + c])));ed resistor ratio 0x80
//
//	i2c_write(0xD9);                  // Set Precharge 0xd9
//	i2c_write(0xF1);
//
//	i2c_write(0xDA);                    // Set COM Pins0xDA
//	i2c_write(0x12);
//
//	i2c_write(0xDB);                 // Set VCOM Detect - 0xDB
//	i2c_write(0x40);
//
//	i2c_write(0x20);                    // Set Memory Addressing Mode
//	i2c_write(0x00);                    // 0x00 - Horizontal
//
//	i2c_write(0x40 | 0x0);              // Set start line at line 0 - 0x40
//
//	i2c_write(0x8D);                    // Charge Pump -0x8D
//	i2c_write(0x14);
//
//	i2c_write(0xA4);              //--turn on all pixels - A5. Regular mode A4
//	i2c_write(0xAF);                //--turn on oled panel - AF
//
//	i2c_write(0x21);        // column address
//	i2c_write(0);           // column start address (0 = reset)
//	i2c_write(127);         // column end addres (127 = reset)
//
//	i2c_write(0x22);        // page address
//	i2c_write(0);           // page start address (0 = reset);
//	i2c_write(3);           // page end address



	// Init sequence
	sendCommand(SSD1306_DISPLAYOFF);                    // 0xAE
	sendCommand(SSD1306_SETDISPLAYCLOCKDIV);            // 0xD5
	sendCommand(0x80); // the suggested ratio 0x80

	sendCommand(SSD1306_SETMULTIPLEX);                  // 0xA8
	sendCommand(SSD1306_LCDHEIGHT - 1);

	sendCommand(SSD1306_SETDISPLAYOFFSET);              // 0xD3
	sendCommand(0x0);                                   // no offset
	sendCommand(SSD1306_SETSTARTLINE | 0x0);            // line #0
	sendCommand(SSD1306_CHARGEPUMP);                    // 0x8D

	sendCommand(0x14);
	sendCommand(SSD1306_MEMORYMODE);                    // 0x20
	sendCommand(0x00);                                  // 0x0 act like ks0108
	sendCommand(SSD1306_SEGREMAP | 0x1);
	sendCommand(SSD1306_COMSCANDEC);


	sendCommand(SSD1306_SETCOMPINS);                    // 0xDA
	sendCommand(0x02);
	sendCommand(SSD1306_SETCONTRAST);                   // 0x81
	sendCommand(0x8F);


	sendCommand(SSD1306_SETPRECHARGE);                  // 0xd9

	sendCommand(0xF1);
	sendCommand(SSD1306_SETVCOMDETECT);                 // 0xDB
	sendCommand(0x40);
	sendCommand(SSD1306_DISPLAYALLON_RESUME);           // 0xA4
	sendCommand(SSD1306_NORMALDISPLAY);                 // 0xA6

	sendCommand(SSD1306_DEACTIVATE_SCROLL);

	sendCommand(SSD1306_DISPLAYON);//--turn on oled panel

	i2c_stop();
}

void clear() {
    // write whole display
    sendCommand(SSD1306_COLUMNADDR);
    sendCommand(0);   // Column start address (0 = reset)
    sendCommand(SSD1306_LCDWIDTH-1); // Column end address (127 = reset)

    sendCommand(SSD1306_PAGEADDR);
    sendCommand(0); // Page start address (0 = reset)
    sendCommand(3); // Page end address
	uint8_t twbrbackup = TWBR;
	TWBR = 12;
	for (uint16_t i = 0; i < ((128 * 32) / 8); i++) {
		unsigned char ret = i2c_start(DevSSD1306 + I2C_WRITE); // set device address and write mode
		if (ret) {
			// failed to issue start condition, possibly no device found
			i2c_stop();
		} else {
			i2c_write(0x40);      // set display RAM display start line register
			for (uint8_t x = 0; x < 16; x++) {
				i2c_write(0x00);
				i++;
			}
			i--;
			i2c_stop();
			TWBR = twbrbackup;
		}
	}
}

void setCursor(uint8_t page, uint8_t column) {
    sendCommand(SSD1306_COLUMNADDR);
    sendCommand(column);   // Column start address (0 = reset)
    sendCommand(SSD1306_LCDWIDTH-1); // Column end address (127 = reset)

    sendCommand(SSD1306_PAGEADDR);
    sendCommand(page); // Page start address (0 = reset)
    sendCommand(3); // Page end address
}

void write_bytes(uint8_t* data, uint8_t bytes) {
	i2c_start(DevSSD1306);
	i2c_write(0x40);		//
	for (uint8_t packet_byte = 0; packet_byte < bytes; packet_byte++) {
		if (packet_byte % 5 == 0) {
			i2c_write(0x00);
		}
		i2c_write(pgm_read_byte(&(font[packet_byte + 495])));
	}
	i2c_stop();
}

void oled_print(char *s) {
	while (*s) { // so lange *s != '\0' also ungleich dem "String-Endezeichen(Terminator)"
		i2c_start(DevSSD1306);
		i2c_write(0x40);
		for (uint8_t c = 0; c < 5; c++) {
			i2c_write(pgm_read_byte(&(font[((*s - 25) * 5) + c])));
		}
		i2c_write(0x00);
		s++;
		i2c_stop();
	}
}


//void oled_digit(const uint8_t* adr, const uint8_t x) {
//    uint8_t width = 22;
//    // stelle den zu schreibenden Bereich ein, so m�ssen wir nicht den cursor immer in die n�chste
//    // Zeile bewegen, denn sobald der eingestellte Bereich in x Richtung �berschritten wird, springt
//    // der Cursor in die n�chste Zeile
//    sendCommand(SSD1306_COLUMNADDR);
//    sendCommand(x);   // start x
//    sendCommand(x + width - 1); // end x
//
//    // Da die Zahlen, den gesamten Bereich in der H�he einnehmen, setzen wird vertikal alle 4 Zeilen,
//    // bei Displays mit 64 Pixel H�he, w�re es sinnvoll diesen Wert anpassbar zu machen
//    sendCommand(SSD1306_PAGEADDR);
//    sendCommand(0); // Page start address (0 = reset)
//    sendCommand(3); // Page end address
//
//    // sichere die Frequenz vom I2C Bus, denn es k�nnten andere Clients mit anderer Frequenz am
//    // selben Bus h�ngen
//    uint8_t twbrbackup = TWBR;
//    TWBR = 12;      // Frequenz ?????
//    const uint8_t bytes = width * 4;    // Anzahl zu schreibender Bytes/Zeilen, Zahl 4 Bytes hoch
//
//    // um die Performance zu verbessern, werden die Bytes in 16 Byte Sch�ben geschrieben
//    for (uint16_t i = 0; i < bytes;) {
//        i2c_start(DevSSD1306 + I2C_WRITE); // starte neue �bertragung
//        i2c_write(0x40);      // set display RAM display start line register
//        uint8_t start = i;
//        for (; (i < (start + 16)) && (i < bytes); i++) {
//            i2c_write(pgm_read_byte(&(vWTextBold_33ptBitmaps[i])));
//        }
//        i2c_stop();
//    }
//    TWBR = twbrbackup;
//}/**/








//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
#define PIN_DC  PB1
#define PIN_CS  PB2


uint8_t shiftOut(uint8_t data) {
    SPDR = data;
    while (!(SPSR & (1 << SPIF))) {
        ;
    }
    return SPDR;
}

void oledWriteCmd(uint8_t command) {
    PORTB &= ~(1 << PIN_DC);
    PORTB &= ~(1 << PIN_CS);
    shiftOut(command);
    PORTB |= (1 << PIN_CS);
}

void oledWriteData(uint8_t data) {
    PORTB |= (1 << PIN_DC);
    PORTB &= ~(1 << PIN_CS);
    shiftOut(data);
    PORTB |= (1 << PIN_CS);
}

void oledWriteCharacter(char character) {
    for (int i=0; i<5; i++) oledWriteData(pgm_read_byte(&(font[((character - 25) * 5) + i])));
    oledWriteData(0x00);
}

void oledWriteString(char *characters) {
    while (*characters) oledWriteCharacter(*characters++);
}


void setup() {
    DDRB |= (1 << PB5);     // SCK
    DDRB |= (1 << PB3);     // MOSI
    DDRB |= (1 << PB2);     // SS (select slave) output
    PORTB |= (1 << PB2);    // deselect slave (active low)

    // enable SPI interface, Master mode, Fosc/8 -> 1MHz
    SPCR = (1 << SPE) | (1 << MSTR) | (0 << SPR1) | (1 << SPR0);
    SPSR = 1 << SPI2X;

    DDRD |= (1 << PD7);
    DDRB |= (1 << PIN_DC);

    PORTD &= ~(1 << PD7);
    _delay_ms(1);
    PORTD |= (1 << PD7);
    _delay_ms(1);


    oledWriteCmd(0x8d); //enable charge pump
    oledWriteCmd(0x14);
    _delay_ms(1);
    oledWriteCmd(0xaf); //set display on

    // oledWriteCmd(0xa8); //set MUX ratio
    // oledWriteCmd(0x3f);
    oledWriteCmd(0xd3); //set display offset
    oledWriteCmd(0x00);

    oledWriteCmd(0x40); //set display start line
    oledWriteCmd(0xa1); //set segment re-map (horizontal flip) - reset value is 0xa0 (or 0xa1)
    oledWriteCmd(0xc8); //set COM output scan direction (vertical flip) - reset value is 0xc0 (or 0xc8)
    oledWriteCmd(0xda); //set COM pins hardware configuration
    oledWriteCmd(0x12); //reset value is 0x12
    oledWriteCmd(0x81); //set contrast (2-byte)
    oledWriteCmd(0xFF);
    // oledWriteCmd(0xa4); //disable entire display on
    // oledWriteCmd(0xa6); //set normal display
    // oledWriteCmd(0xd5); //set oscillator frequency
    // oledWriteCmd(0x80);
    // oledWriteCmd(0xdb); //vcomh deselect level (brightness)
    // oledWriteCmd(0x20);

    oledWriteCmd(0x20); //set horizontal addressing mode for screen clear
    oledWriteCmd(0x00);

    oledWriteCmd(0x21); //set column start and end address
    oledWriteCmd(0x00); //set column start address
    oledWriteCmd(0x7f); //set column end address

    oledWriteCmd(0x22); //set row start and end address
    oledWriteCmd(0x00); //set row start address
    oledWriteCmd(0x07); //set row end address


    for (uint16_t i=0; i<1024; i++) oledWriteData(0x00); // clear oled screen

    oledWriteCmd(0x20); //set page addressing mode
    oledWriteCmd(0x00);

    oledWriteCmd(0xb7); //set page start address to page 7
    oledWriteCmd(0x00); //set lower column start address
    oledWriteCmd(0x10); //set upper column start address


//  oledWriteCmd(0x27); //set right horizontal scroll
//  oledWriteCmd(0x0);  //dummy byte
//  oledWriteCmd(0x0);  //page start address
//  oledWriteCmd(0x7);  //scroll speed
//  oledWriteCmd(0x7);  //page end address
//  oledWriteCmd(0x0);  //dummy byte
//  oledWriteCmd(0xff); //dummy byte

//  oledWriteCmd(0x2f); //start scrolling

    oledWriteCmd(0xb0); //set page start address to page 0
}



//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////

// set cursor, sets position where the data will be written to display
void oled_set_start_addr(uint8_t page, uint8_t column) {
    // Set Page Start Address for Page Addressing Mode
    // vertical position of cursor
    oledWriteCmd(0xB0 | page);

    // Set Lower Column Start Address for Page Addressing Mode
    // (horizontal position of cursor)
    oledWriteCmd(0x00 | (column & 0xF));    // set lower nibble
    // Set Higher Column Start Address for Page Addressing Mode
    oledWriteCmd(0x10 | (column >> 4));     // set upper nibble
}

void oled_clear_line(uint8_t line) {
    oled_set_start_addr(line, 0);
    for (uint16_t i = 0; i < 128; i++) {
        oledWriteData(0x00);
    }
}

void oled_clear_display() {
    oled_set_start_addr(0, 0);
    for (uint16_t i = 0; i < (128 * 8); i++) {
        oledWriteData(0x00);
    }
}
