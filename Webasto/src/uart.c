/*
 * uart.c
 *
 *  Created on: 03.07.2013
 *      Author: googy
 */
#include <avr/io.h>
#include "uart.h"

#include <util/delay.h>
#include <avr/interrupt.h>

void uart_init() {	// USART init
	UBRR0H = UBRR_VAL >> 8;
	UBRR0L = UBRR_VAL & 0xFF;

	UCSR0B |= (1 << TXEN0) | (1 << RXEN0); // UART TX einschalten
	UCSR0C = (1 << UCSZ01) | (1 << UCSZ00) | (1 << UPM01); // Asynchron 8E1

	uart_flush();   // Flush Receive-Buffer (entfernen evtl. vorhandener ungültiger Werte)
}

void uart_putc(unsigned char c) {
	while (!(UCSR0A & (1 << UDRE0))) {
	} /* warten bis Senden moeglich */
	UDR0 = c; /* sende Zeichen */
}

void uart_puts(char *s) {
	while (*s) { /* so lange *s != '\0' also ungleich dem "String-Endezeichen(Terminator)" */
		uart_putc(*s);
		s++;
	}
}

void uart_putb(char *s, uint8_t length) {
    for (uint8_t i = 0; i < length; i++) {
        uart_putc(*s);
        s++;
    }
}

uint8_t uart_receive(void) {
	while ( !(UCSR0A & (1 << RXC0)) ) {};
	return UDR0;
}

void uart_flush(void) {
	uint8_t dummy = 0;
	while ( UCSR0A & (1 << RXC0) ) {
		dummy = UDR0;
	}
}

void soft_uart_init(void) {

}

void soft_uart_tx(uint8_t byte) {
    cli();
    uint8_t mask = 1;
    /* Start bit */
    PORTC &= ~(1 << PC5);
    _delay_us(103);
    while(mask) {
        if (mask & byte)
            PORTC |= (1 << PC5);
        else
            PORTC &= ~(1 << PC5);
        _delay_us(103);
        mask <<= 1;
    }
    /* Stop bit */
    PORTC |= (1 << PC5);
    _delay_us(103);
    sei();
}

void soft_uart_puts(char *s) {
    while (*s) { /* so lange *s != '\0' also ungleich dem "String-Endezeichen(Terminator)" */
        soft_uart_tx(*s);
        s++;
    }
}
