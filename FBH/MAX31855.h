/*
 * MAX31855.h
 *
 *  Created on: 28.05.2015
 *      Author: googy_000
 */

#ifndef MAX31855_H_
#define MAX31855_H_

enum error {
	OK,
	open_cirquit,
	short_to_GND,
	short_to_VCC,
};

struct data {
	enum	error err;
	uint8_t	fault;
	int8_t	intern;
	int16_t	sensor;
	uint8_t	b1;
	uint8_t b2;
	uint8_t b3;
	uint8_t b4;
} max31855;

void max31855initSW();
void max31855initHW();
void readTempSW();
void readTempHW();

#endif /* MAX31855_H_ */
