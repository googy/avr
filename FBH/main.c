#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>

#include <util/delay.h>
#include <stdio.h>
#include <inttypes.h>

#include <stdlib.h>	// itoa

#include "mcp2515.h"
#include "global.h"
#include "defaults.h"

#include "uart.h"

#include "DS18B20/ow.h"

#include "timebase.h"

#include "pid.h"

uint8_t broadcast = 1;		// broadcast
tCAN message, rMessage;

char buf[10];

static uint32_t delay_timecounter = 0;

uint8_t turned = 0;
uint8_t lastdir = 0;

struct temperature{
    uint8_t degrees;
    uint16_t millis;
};

uint8_t out_id[8] = {0x28, 0x6D, 0x5F, 0xAA, 0x03, 0x00, 0x00, 0x3B};
struct temperature out;
//uint8_t in_id[8] =  {0x28, 0x8F, 0x87, 0xAA, 0x03, 0x00, 0x00, 0x1C};
//struct temperature in;

int main(void) {

    // timebase
    timer0_init();
    sei();

    uart_init();

    // sensor init
    ow_init();
    ow_start();     // first measurement
    _delay_ms(1000);
    uint16_t out_t = ow_temp_id(out_id);
    out.degrees = (out_t & 0b11111110000) >> 4;
    out.millis = out_t & 0x0F;
    uint8_t ow_state = 0;
    uint32_t ow_timecounter = 0;

    // logik init
    uint8_t on = 0;
    uint8_t target = 26;
    stoptimer_init(&delay_timecounter);
    // wait zwischen Messzyklen
    uint16_t turntime = 0;
    uint8_t state = 0;

    // Relais
    PORTC &= ~((1 << PC0) | (1 << PC1) | (1 << PC2));
    DDRC |= (1 << PC0) | (1 << PC1) | (1 << PC2);
    PORTC &= ~(1 << PC2);        // Pumpe ausschalten

    DDRB |= 1 << PB2;		// !!!!!!!!!!!!!!!!!!!!! SS to output, sonst keine SPI Kommunikation



    // Versuche den MCP2515 zu initilaisieren
    if (!mcp2515_init()) {
        uart_puts("MCP2515 FAIL\n\r");
        return 0;
    } else {
        uart_puts("MCP2515 OK\n\r");
    }
    uint32_t can_timecounter = 0;
    stoptimer_init(&can_timecounter);

// PID init
/*    uint32_t pid_timecounter = 0;
    stoptimer_init(&pid_timecounter);
    struct PID_DATA pidData;
    pidData.maxError = 0;
    int16_t referenceValue, measurementValue, inputValue;
    pid_Init(1 * SCALING_FACTOR, 0.1 * SCALING_FACTOR, 0.1 * SCALING_FACTOR, &pidData);*/

    while (1) {
        // DS18B20 OneWire Temperatursensor
        switch (ow_state) {
        case 0:
            stoptimer_init(&ow_timecounter);
            ow_state = 1;
            ow_start();
            break;
        case 1:
            if (stoptimer_expired(&ow_timecounter, 750)) {   // laut Datenblatt, bei 12-Bit max. 750ms
                uint16_t out_t = ow_temp_id(out_id);
                out.degrees = (out_t & 0b11111110000) >> 4;
                out.millis = out_t & 0x0F;
                ow_state = 0;
            }
            break;
        }

        switch (state) {
        case 0:
            if (on) {
                PORTC |= (1 << PC2);
                state = 1;
            }
            break;
        case 1:         // idle, warte bis Periode abgelaufen ist, verhindert das oszillieren, das System tr�ge
            if (!on) {state = 0; PORTC &= ~(1 << PC2); break;}
            stoptimer_init(&delay_timecounter);
            state = 2;
            break;
        case 2:         // test
            if (!on) {state = 0; PORTC &= ~(1 << PC2); break;}
            if (stoptimer_expired(&delay_timecounter, 15000)) {

                // Run PID calculations once every PID timer timeout
/*                referenceValue = target;
                measurementValue = out.degrees;
                inputValue = pid_Controller(referenceValue, measurementValue, &pidData);
                stoptimer_init(&pid_timecounter);*/

                uart_puts("ist: ");
                uart_puts(itoa(out.degrees, buf, 10));
                uart_puts("   soll: ");
                uart_puts(itoa(target, buf, 10));
                uart_puts("\n\r");

                turntime = 0;
                stoptimer_init(&delay_timecounter);

                // solltemperatur erreicht, nichts tun
                if (out.degrees == target) {
                    PORTC &= ~((1 << PC1) | (1 << PC0));
                    state = 1;
                    break;
                }


                // zu kalt
                if (out.degrees < target) {
                    // Endanschlag
                    if (lastdir == 1) {     // Drehrichtung gewechselt
                        turned = 0;
                    }
                    uint8_t difference = target - out.degrees;
                    turned = (difference < (255 - turned)) ? (turned + difference) : 255;
                    lastdir = 2;

                    // Drehung
                    if (turned < 150) {     // Endanschlag noch nicht erreicht?
                        turntime = difference * 1000;   // Drehzeit in ms
                        turntime = turntime > 0x7FFF ? 0x7FFF : turntime;   // wegen overflow Drehzeit auf 63s begrenzen
                        PORTC &= ~(1 << PC1);
                        PORTC |= (1 << PC0);    // waermer
                        state = 3;  // go Drehzustand
                    } else {
                        state = 1;  // am Endanschlag, nicht weiter drehen
                    }
                }

                // zu warm
                if (out.degrees > target) {
                    // Endanschlag
                    if (lastdir == 2) {     // Drehrichtung gewechselt
                        turned = 0;
                    }
                    uint8_t difference = out.degrees - target;
                    turned = (difference < (255 - turned)) ? (turned + difference) : 255;
                    lastdir = 1;

                    // Drehung
                    if (turned < 150) {
                        turntime = difference * 1000;
                        turntime = turntime > 0x7FFF ? 0x7FFF : turntime;   // auf 63 begrenzen
                        PORTC &= ~(1 << PC0);
                        PORTC |= (1 << PC1);    // kaelter
                        state = 3;  // drehen
                    } else {
                        state = 1;
                    }
                }
                uart_puts("turntime: ");
                uart_puts(itoa(turntime, buf, 10));
                uart_puts("\n\r");

                uart_puts("turned: ");
                uart_puts(itoa(turned, buf, 10));
                uart_puts("\n\n\r");
            }
            break;
        case 3:         // turn
            if (stoptimer_expired(&delay_timecounter, turntime)) {
                PORTC &= ~((1 << PC1) | (1 << PC0));
                stoptimer_init(&delay_timecounter);
                state = 1;
            }
            break;
	    }

        if (stoptimer_expired(&can_timecounter, 5000)) {
            // aktives Versenden der Werte aktiviert
            if (broadcast) {
                // prepare message - Abgastemperatur
                message.id = 0x7FF;
                message.header.rtr = 0;
                message.header.length = 8;
                message.data[0] = out.degrees;
                message.data[1] = out.millis >> 8;
                message.data[2] = out.millis && 0xFF;
                message.data[3] = target;
                message.data[4] = 0;
                message.data[5] = 0;
                message.data[6] = 0;
                message.data[7] = 0;

                // send message
                if (mcp2515_send_message(&message)) {
                    //uart_puts("Nachricht konnte gesendet werden\n\r");
                } else {
                    uart_puts("ERROR Nachricht konnte nicht gesendet werden\n\r");
                }
            }
            stoptimer_init(&can_timecounter);
        }

		// check for incoming messages
		if (mcp2515_check_message()) {		// Nachricht vorhanden
			if (mcp2515_get_message(&rMessage)) {		// Nachricht erfolgreich angekommen
				if (rMessage.header.rtr) {
					// send data
				}

//				if (id = x) {		// set broadcast
//					broadcast = data;
//				}

				if (rMessage.id == 0x7FF && rMessage.header.length == 8) {
				    on = rMessage.data[0];
				    target = rMessage.data[1];
				}
//				uart_puts("ID: ");
//				itoa((rMessage.id), buf, 16);
//				uart_puts(buf);
//				uart_puts("   DLC: ");
//				itoa((rMessage.header.length), buf, 10);
//				uart_puts(buf);
//				uart_puts("   Data: ");
//				for (uint8_t i = 0; i < rMessage.header.length; i++) {
//					itoa(rMessage.data[i], buf, 16);
//					uart_puts(buf);
//					uart_putc(' ');
//				}
//				uart_puts("\n\r");
			}
			else {
				uart_puts("FAIL\n\r");
			}
		}

	}

	return 0;
}
