#ifndef OW_H_
#define OW_H_

#define OW_OUT		PORTC
#define OW_DIR		DDRC
#define OW_PIN		PINC
#define OW			PC3

void ow_init();
void ow_reset();
void ow_start();
uint16_t ow_temp_id(uint8_t * id);
uint8_t read_ow_scratch_id(uint8_t id);
uint8_t get_temp_deg();
uint16_t get_temp_millis();
uint8_t crc8( uint8_t *data);

/* Liest die ROM ID des Sensors aus und gibt diese zur�ck,
 * funktioniert wenn lediglich nur ein Sensor am Bus h�ngt
 * Nimmt einen Zeiger auf ein min. 8 Byte langes Array um das Ergebnis dort abzulegen
 */
void ow_read_single_id(uint8_t* id);

/* converts DS18B20 returned lower scratchpad byte to 1/10000 degree
 * takes lower nibble of the lower byte returned by sensor
 */
uint16_t temp_to_millis(uint8_t value);

#endif
