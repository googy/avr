################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../DS18B20/ds18b20.c \
../DS18B20/ow.c 

OBJS += \
./DS18B20/ds18b20.o \
./DS18B20/ow.o 

C_DEPS += \
./DS18B20/ds18b20.d \
./DS18B20/ow.d 


# Each subdirectory must supply rules for building sources it contributes
DS18B20/%.o: ../DS18B20/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -Os -fpack-struct -fshort-enums -ffunction-sections -fdata-sections -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega328p -DF_CPU=8000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


