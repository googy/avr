#include <avr/io.h>
#include <util/delay.h>
#include <stdlib.h>
#include <avr/interrupt.h>

#include "main.h"

void uart_init() {
	/* Baudrate einstellen */
	UBRRH = UBRR_VAL >> 8;
	UBRRL = UBRR_VAL & 0xFF;
	UCSRB |= (1 << TXEN);  // UART TX einschalten
	UCSRC = (1 << URSEL) | (1 << UCSZ1) | (1 << UCSZ0);  // Asynchron 8N1
}

void uart_putc(unsigned char c) {
	while (!(UCSRA & (1 << UDRE))) {
	} /* warten bis Senden moeglich */
	UDR = c; /* sende Zeichen */
}

void uart_puts(char *s) {
	while (*s) { /* so lange *s != '\0' also ungleich dem "String-Endezeichen(Terminator)" */
		uart_putc(*s);
		s++;
	}
}

/* ADC initialisieren */
void ADC_Init(void) {
  // die Versorgungsspannung AVcc als Refernz w�hlen:
  ADMUX = (1<<REFS0);
  // oder interne Referenzspannung als Referenz f�r den ADC w�hlen:
  // ADMUX = (1<<REFS1) | (1<<REFS0);

  // Bit ADFR ("free running") in ADCSRA steht beim Einschalten
  // schon auf 0, also single conversion
  ADCSRA = (1<<ADPS1) | (1<<ADPS0);     // Frequenzvorteiler
  ADCSRA |= (1<<ADEN);                  // ADC aktivieren

  /* nach Aktivieren des ADC wird ein "Dummy-Readout" empfohlen, man liest
     also einen Wert und verwirft diesen, um den ADC "warmlaufen zu lassen" */

  ADCSRA |= (1<<ADSC);                  // eine ADC-Wandlung
  while (ADCSRA & (1<<ADSC) ) {         // auf Abschluss der Konvertierung warten
  }
  /* ADCW muss einmal gelesen werden, sonst wird Ergebnis der n�chsten
     Wandlung nicht �bernommen. */
  (void) ADCW;
}

/* ADC Einzelmessung */
uint16_t ADC_Read( uint8_t channel )
{
  // Kanal waehlen, ohne andere Bits zu beeinflu�en
  ADMUX = (ADMUX & ~(0x1F)) | (channel & 0x1F);
  ADCSRA |= (1<<ADSC);            // eine Wandlung "single conversion"
  while (ADCSRA & (1<<ADSC) ) {   // auf Abschluss der Konvertierung warten
  }
  return ADCW;                    // ADC auslesen und zur�ckgeben
}

void timer0_init() {	// 8-Bit
	TCCR0 |= (1 << CS02) | (1 << CS00);	// 1024 Prescaler -> 2000000/1024
	TIMSK |= (1 << TOIE0);	// timer overflow interrupt enable
	// 2000000/1024/256 -> 131ms interrupt cycle
}

int main(){
	DDRD |= (1 << PD6);	// PD6 als Ausgang
	DDRD &= ~(1 << PD4);	// PD4 als Eingang
	PORTD |= (1 << PD4);	// PD4 PullUp

	uart_init();
	ADC_Init();
	timer0_init();
	sei();

	char buf[6];
	while(1){
		uart_putc(10);	// line feed
		//uart_putc(12);	// form feed
		uart_putc(13);	// carriage return
		uint16_t adcval = ADC_Read(6);	// Kanal 6 lesen
		ltoa(adcval,buf,10);	// Ergebnis in String wandeln
		uart_puts(buf);	// String ausgeben

//		if (adcval > maxADCvalue) {
//			PORTD |= (1 << PD6);	// Relais ein
//		}
//		if (adcval < minADCvalue) {
//			PORTD &= ~(1 << PD6);	// Relais aus
//		}

		switch (machineState) {
		case NORMAL:
			// on enter restore power to pandaboard.
			PORTD |= (1 << PD6);	// Relais ein
			// if voltage < low_level: switch to state triggered.
			if (adcval < minADCvalue) {
				machineSubState = ENTER;
				machineState = TRIGGERED;
			}
			break;
		case TRIGGERED:
			switch (machineSubState) {
			case ENTER:
				// on enter start timer.
				cli();
				timer = 0;
				sei();
				machineSubState = RUN;
				break;
			case RUN:
				// if voltage > low_level: switch to state normal.
				if (adcval > minADCvalue) {
					machineSubState = ENTER;
					machineState = NORMAL;
				}
				// if timer elapsed: switch to state shutdown.
				if (timer > 70) {
					machineSubState = ENTER;
					machineState = SHUTDOWN;
				}
				break;
			}
			break;
		case SHUTDOWN:
			switch (machineSubState) {
			case ENTER:
				// TODO on enter set signal for shutdown.
				// on enter start timer.
				cli();
				timer = 0;
				sei();
				machineSubState = RUN;
				break;
			case RUN:
				// if timer elapsed: switch to state off.
				if (timer > 70) {
					machineSubState = ENTER;
					machineState = OFF;
				}
				break;
			}
			break;
		case OFF:
			switch (machineSubState) {
			case ENTER:
				// on enter drop power to pandaboard.
				PORTD &= ~(1 << PD6);	// Relais aus
				machineSubState = RUN;
				break;
			case RUN:
				// if voltage > high_level: switch to state normal
				if (adcval > maxADCvalue) {
					machineSubState = ENTER;
					machineState = NORMAL;
				}
				break;
			}
			break;
		default:
			// TODO panic
			break;
		}
	}
	return 1;
}

// timer0 overflow ISR, 131ms
ISR (TIMER0_OVF_vect) {
	timer++;
}
