/*
 * main.h
 *
 *  Created on: 23.11.2013
 *      Author: googy
 */

#ifndef MAIN_H_
#define MAIN_H_

/* Berechnung ADC Werte */
// Spannungsteiler
#define R1 10000
#define R2 4700
// Spannungen
#define VREF 4.96
#define VMIN 11.5
#define VMAX 12.0

const uint16_t minADCvalue = (R2/(R1+R2)*VMIN)/(VREF/1024.0)+0.5;
const uint16_t maxADCvalue = (R2/(R1+R2)*VMAX)/(VREF/1024.0)+0.5;
//uint16_t minADCvalue = (VMIN+0.38)/0.0155 + 0.5;


#ifndef F_CPU
#error F_CPU unkown
#endif

#define BAUD 9600UL      // Baudrate
// Berechnungen
#define UBRR_VAL ((F_CPU+BAUD*8)/(BAUD*16)-1)   // clever runden
#define BAUD_REAL (F_CPU/(16*(UBRR_VAL+1)))     // Reale Baudrate
#define BAUD_ERROR ((BAUD_REAL*1000)/BAUD) // Fehler in Promille, 1000 = kein Fehler.
#if ((BAUD_ERROR<990) || (BAUD_ERROR>1010))
#error Systematischer Fehler der Baudrate gr�sser 1% und damit zu hoch!
#endif

enum boolean {
	FALSE,
	TRUE
};

enum state {
	DEFAULT,
	NORMAL,
	TRIGGERED,
	SHUTDOWN,
	OFF
} machineState = NORMAL;

enum subState {
	ENTER,
	RUN
} machineSubState = ENTER;

volatile uint8_t timer = 0;

#endif /* MAIN_H_ */
