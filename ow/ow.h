#ifndef OW_H_
#define OW_H_

#define OW_OUT		PORTB
#define OW_DIR		DDRB
#define OW_PIN		PINB
#define OW			PB3

void ow_init();
void ow_reset();
void ow_write(uint8_t data);
uint64_t ow_read(uint8_t len);
void send_id(uint8_t *id);

#endif /* OW_H_ */
