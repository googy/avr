#include <avr/io.h>
#include <util/delay.h>

#include "ow.h"

//---setup---
void wait_reset() {
	_delay_us(500);
}

void wait_recovery() {
	_delay_us(5);
}

void wait_write1() {
	_delay_us(5);
}

void wait_write0() {
	_delay_us(80);
}

void ow_rel() {
	OW_DIR &= ~(1 << OW);	// OW als Eingang
	OW_OUT |= (1 << OW);	// pullup ein
}

void ow_lo() {
	OW_OUT &= ~(1 << OW);	// pullup aus
	OW_DIR |= (1 << OW);	// OW als Ausgang
}

#define IS_OW() (OW_PIN & (1 << OW))

//---functions---
void ow_init() {
	ow_rel();
}

void ow_reset() {
	ow_lo();
	wait_reset();
	ow_rel();
	wait_reset();
}

void ow_send(int data, uint8_t len) {
	for (int i = 0; i < len; i++) {
		if ((data & 1) > 0) { // send 1
			ow_lo();
			wait_write1();
			ow_rel();
			wait_write0();
		} else { // send 0
			ow_lo();
			wait_write0();
			ow_rel();
		}
		data >>= 1;
		wait_recovery();
	}
}

void ow_write(uint8_t data) {
	ow_send(data, 8);
}

uint64_t ow_read(uint8_t len) {
	uint64_t mask = 1;
	uint64_t result = 0;
	for (int i = 0; i < len; i++) {
		ow_lo();
		wait_recovery();
		ow_rel();
		_delay_us(8);
		//Sample
		if (IS_OW()) result |= mask;
		mask <<= 1;
		wait_write0();
	}
	return result;
}

// ID senden
void send_id(uint8_t *id) {
	for (uint8_t i = 0; i < 8; i++) {
		ow_write(*(id + i)); //0-8 mal den Zeiger verschieben und jeweils den Inhalt der Arrayzelle senden
	}
}
