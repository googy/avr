#include <avr/io.h>
#include <util/delay.h>
#include <stdlib.h>
#include "hd44780.h"
#include "ow.h"

void printT(uint16_t t) {
	char s[10];
	uint16_t nk = 0;
	utoa((t >> 4), s, 10);
	hd44780_puts(s);
	hd44780_puts(".");
	if (t & (1 << 3)) {
		nk = 50;
	}
	if (t & (1 << 2)) {
		nk += 25;
	}
	if (t & (1 << 1)) {
		nk += 12;
	}
	if (t & (1 << 0)) {
		nk += 6;
	}
	if (nk < 10) {
		hd44780_puts("0");
	}
	utoa(nk, s, 10);
	hd44780_puts(s);
}

int main() {
	uint8_t id1[8] = { 0x28, 0x94, 0x80, 0xAA, 0x03, 0x00, 0x00, 0xEE }; //ROM ID 1 experimentell
//	uint8_t id2[8] = { 0x28, 0xFF, 0x8B, 0xAA, 0x03, 0x00, 0x00, 0xA5 }; //ROM ID 2 Wohnzimmer

	ow_init();

	// Init LCD
	_delay_ms(30);
	hd44780_init();
	hd44780_mode(1,1,0);
	hd44780_clear();

	while (1) {
		ow_reset();				//reset
		ow_write(0xCC);			//SKIP ROM [CCh]
		ow_write(0x44);			//CONVERT T [44h]
		_delay_ms(1000);
hd44780_clear();	// clear screen
		ow_reset();				//reset
		//ow_write(0x55);			//MATCH ROM [55h]
		//send_id(id1);			//send ROM id
		ow_write(0xCC);		//SKIP ROM [CCh]
		ow_write(0xBE);			//READ SCRATCHPAD [BEh]


		printT(ow_read(16));		//read 2 bytes

		_delay_ms(1000);
	}
}
