/*
 * MAX31855.c
 *
 *  Created on: 28.05.2015
 *      Author: googy_000
 */


//BIT			NAME									DESCRIPTION
//D[31:18]	14-Bit Thermocouple Temperature Data	These bits contain the signed 14-bit thermocouple temperature value. See Table 4.
//D17			Reserved								This bit always reads 0.
//D16			Fault									This bit reads at 1 when any of the SCV, SCG, or OC faults are active. Default value is 0.
//D[15:4]		12-Bit Internal Temperature Data		These bits contain the signed 12-bit value of the reference junction temperature. See Table 5.
//D3			Reserved								This bit always reads 0.
//D2			SCV Fault								This bit is a 1 when the thermocouple is short-circuited to VCC. Default value is 0.
//D1			SCG Fault								This bit is a 1 when the thermocouple is short-circuited to GND. Default value is 0.
//D0			OC Fault								This bit is a 1 when the thermocouple is open (no connections). Default value is 0.

#include <avr/io.h>
#include <util/delay.h>
#include "MAX31855.h"

/*
 * MAX31855.c
 *
 *  Created on: 28.05.2015
 *      Author: googy_000
 */


//BIT			NAME									DESCRIPTION
//D[31:18]	14-Bit Thermocouple Temperature Data	These bits contain the signed 14-bit thermocouple temperature value. See Table 4.
//D17			Reserved								This bit always reads 0.
//D16			Fault									This bit reads at 1 when any of the SCV, SCG, or OC faults are active. Default value is 0.
//D[15:4]		12-Bit Internal Temperature Data		These bits contain the signed 12-bit value of the reference junction temperature. See Table 5.
//D3			Reserved								This bit always reads 0.
//D2			SCV Fault								This bit is a 1 when the thermocouple is short-circuited to VCC. Default value is 0.
//D1			SCG Fault								This bit is a 1 when the thermocouple is short-circuited to GND. Default value is 0.
//D0			OC Fault								This bit is a 1 when the thermocouple is open (no connections). Default value is 0.

#include <avr/io.h>
#include <util/delay.h>
#include "MAX31855.h"

void max31855initSW() {
	DDRD |= (1 << PD6);		// SCK	als Ausgang
	PORTD &= ~(1 << PD6);	// SCK default low

	DDRD &= ~(1 << PD5);	// MISO als Eingang
	PORTD &= ~(1 << PD5);	// evtl. MISO pull-up abschalten

	DDRD |= (1 << PD7);		// CS als Ausgang
	PORTD |= (1 << PD7);	// CS aus -> high

	_delay_ms(1);
}

void max31855initHW() {
	DDRB |= (1 << PB3)		// MOSI output, here not needed
			| (1 << PB5)	// SCK output
			| (1 << PB2)	// SS output, to prevent automatic Slave Mode switch
			;

	DDRB &= ~((1 << PB4)	// MISO input, automatically overriden by SPI, so not needed
			);

	DDRD |= (1 << PD7);		// CS output
	PORTD |= (1 << PD7);	// CS off -> high

	SPCR = (0 << SPIE)		// no SPI interrupt
			| (1 << SPE)	// enable SPI
			| (0 << DORD)	// 0: MSB first, 1: LSB first
			| (1 << MSTR)	// 0: Slave Mode, 1: Master Mode
			| (0 << CPOL)	// SPI Mode 0
			| (0 << CPHA)	// SPI Mode 0
			| (0 << SPR1)	// Prescaler = 4, max. 5MHz
			| (0 << SPR0)	// Prescaler = 4
			;

	SPSR = (0 << SPI2X);	// no double speed

	_delay_ms(1);
}

uint8_t SPItransferByte(uint8_t dataByte) {
	SPDR = dataByte;
	while (!(SPSR & (1 << SPIF)));
	return SPDR;
}

void readTempHW() {
	PORTD &= ~(1 << PD7);	// CS, select chip
	_delay_ms(1);			// tCSS, CS fall to SCK rise, min. 100ns

	max31855.b4 = SPItransferByte(0x00);
	max31855.b3 = SPItransferByte(0x00);
	max31855.b2 = SPItransferByte(0x00);
	max31855.b1 = SPItransferByte(0<00);

	PORTD |= (1 << PD7);	// reset CS

	max31855.err = OK;
	if (max31855.b1 & (1 << 0)) max31855.err = open_cirquit;
	if (max31855.b1 & (1 << 1)) max31855.err = short_to_GND;
	if (max31855.b1 & (1 << 2)) max31855.err = short_to_VCC;
	if (!(max31855.b3 & (1 << 0))) {
		max31855.intern = max31855.b2;
		max31855.sensor = max31855.b3 >> 4;
		max31855.sensor |= max31855.b4 << 4;
	} else {
		max31855.intern = 0;
		max31855.sensor = 0;
		max31855.fault = 1;
	}
}

void readTempSW() {
	uint16_t low = 0;
	uint16_t high = 0;

	PORTD &= ~(1 << PD7);	// CS, select chip
	_delay_ms(1);			// tCSS, CS fall to SCK rise, min. 100ns

	for (uint8_t bit = 15; bit < 16; bit--) {
		PORTD |= (1 << PD6);
		_delay_us(1);
		if (PIND & (1 << PD5)) {
			high |= (1 << bit);
		}
		_delay_ms(1);
		PORTD &= ~(1 << PD6);
		_delay_ms(1);
	}
	for (uint8_t bit = 15; bit < 16; bit--) {
		PORTD |= (1 << PD6);
		_delay_us(1);
		if (PIND & (1 << PD5)) {
			low |= (1 << bit);
		}
		_delay_ms(1);
		PORTD &= ~(1 << PD6);
		_delay_ms(1);
	}

	PORTD |= (1 << PD7);	// reset CS

	max31855.err = OK;
	if (low & (1 << 0)) max31855.err = open_cirquit;
	if (low & (1 << 1)) max31855.err = short_to_GND;
	if (low & (1 << 2)) max31855.err = short_to_VCC;
	if (!(high & (1 << 0))) {
		max31855.intern = low >> 8;
		max31855.sensor = high >> 4;
	} else {
		max31855.intern = 0;
		max31855.sensor = 0;
		max31855.fault = 1;
	}
}
