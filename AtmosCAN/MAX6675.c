/*
 * MAX6675.c
 *
 *  Created on: 03.06.2020
 *      Author: googy
 */

#include <avr/io.h>
#include <util/delay.h>
#include "MAX6675.h"


void max6675initSW() {
	DDRD |= (1 << PD6);		// SCK	als Ausgang
	PORTD &= ~(1 << PD6);	// SCK default low

	DDRD &= ~(1 << PD5);	// MISO als Eingang
	PORTD &= ~(1 << PD5);	// evtl. MISO pull-up abschalten

	DDRD |= (1 << PD7);		// CS als Ausgang
	PORTD |= (1 << PD7);	// CS aus -> high

	_delay_ms(1);
}

uint16_t max6675readTempSW() {
	uint16_t scratchpad = 0;

	PORTD &= ~(1 << PD7);	// CS, select chip
	_delay_ms(1);			// tCSS, CS fall to SCK rise, min. 100ns

	for (uint8_t bit = 15; bit < 16; bit--) {
		PORTD |= (1 << PD6);
		_delay_us(1);
		if (PIND & (1 << PD5)) {
			scratchpad |= (1 << bit);
		}
		_delay_ms(1);
		PORTD &= ~(1 << PD6);
		_delay_ms(1);
	}

	PORTD |= (1 << PD7);	// reset CS

	return scratchpad;
}
