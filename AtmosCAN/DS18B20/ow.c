#include <avr/io.h>
#include <util/delay.h>

#include "ow.h"

static uint8_t scratchpad[9];

//---setup---
void wait_reset() {
	_delay_us(500);
}

void wait_recovery() {
	_delay_us(5);
}

void wait_write_1() {
	_delay_us(5);
}

void wait_write_0() {
	_delay_us(80);
}

void ow_rel() {
	OW_DIR &= ~(1 << OW);	// OW als Eingang
	OW_OUT |= (1 << OW);	// pullup ein
}

void ow_lo() {
	OW_OUT &= ~(1 << OW);	// pullup aus
	OW_DIR |= (1 << OW);	// OW als Ausgang
}

#define IS_OW() (OW_PIN & (1 << OW))

//---functions---

#define CRC8POLY    0x18              //0x18 = X^8+X^5+X^4+X^0

uint8_t crc8( uint8_t *data) {
	uint8_t  crc;
	uint8_t  bit_counter;
	uint8_t  b;
	uint8_t  feedback_bit;

	crc = 0;

	for (uint8_t loop_count = 0; loop_count != 8; loop_count++) {
		b = data[loop_count];

		bit_counter = 8;
		do {
			feedback_bit = (crc ^ b) & 0x01;

			if ( feedback_bit == 0x01 ) {
				crc = crc ^ CRC8POLY;
			}
			crc = (crc >> 1) & 0x7F;
			if ( feedback_bit == 0x01 ) {
				crc = crc | 0x80;
			}

			b = b >> 1;
			bit_counter--;

		} while (bit_counter > 0);
	}
	return crc;
}

void ow_init() {
	ow_rel();
}

void ow_reset() {
	ow_lo();
	wait_reset();
	ow_rel();
	wait_reset();
}

void ow_send(int data, uint8_t len) {
	for (int i = 0; i < len; i++) {
		if ((data & 1) > 0) { // send 1
			ow_lo();
			wait_write_1();
			ow_rel();
			wait_write_0();
		} else { // send 0
			ow_lo();
			wait_write_0();
			ow_rel();
		}
		data >>= 1;
		wait_recovery();
	}
}

void ow_write(uint8_t data) {
	ow_send(data, 8);
}

uint64_t ow_read(uint8_t len) {
	uint64_t mask = 1;
	uint64_t result = 0;
	for (int i = 0; i < len; i++) {
		ow_lo();
		wait_recovery();
		ow_rel();
		_delay_us(8);
		//Sample
		if (IS_OW()) result |= mask;
		mask <<= 1;
		wait_write_0();
	}
	return result;
}

uint8_t ow_read_byte() {
	uint8_t mask = 1;
	uint8_t result = 0;
	for (int i = 0; i < 8; i++) {
		ow_lo();
		wait_recovery();
		ow_rel();
		_delay_us(8);
		//Sample
		if (IS_OW()) result |= mask;
		mask <<= 1;
		wait_write_0();
	}
	return result;
}

void ow_start() {
	ow_reset();				//reset
	ow_write(0xCC);			//SKIP ROM [CCh]
	ow_write(0x44);			//CONVERT T [44h]
}

uint16_t ow_temp_id(uint8_t * id) {
	ow_reset();				//reset
	ow_write(0x55);			//MATCH ROM [55h]
	//SEND ROM ID
	for (uint8_t i = 0; i < 8; i++) {
		ow_write(id[i]);
	}
	ow_write(0xBE);			//READ SCRATCHPAD [BEh]
	for (uint8_t i = 0; i < 9; i++) {
		scratchpad[i] = ow_read_byte();
	}
	ow_reset();				//nothing more to read, stop
//	if (crc8(&scratchpad[0]) != scratchpad[8]) {
//		return 0b0000011111110000;	// entspricht 127C, Fehler
//	}
	return scratchpad[1] << 8 | scratchpad[0];
}

//uint8_t check_scratch_crc() {
//	//uint8_t crc = scratchpd[8];
//
//	return 1;
//}
//
//uint8_t read_ow_scratch_id(uint8_t id) {
//	ow_reset();				//reset
//	ow_write(0x55);			//MATCH ROM [55h]
//	//SEND ROM ID
//	for (uint8_t i = 0; i < 8; i++) {
//		ow_write(eeprom_read_byte((uint8_t *)(id + i)));	// ID aus dem EEPROM lesen
//	}
//	ow_write(0xBE);			//READ SCRATCHPAD [BEh]
//	for (uint8_t i = 0; i < 9; i++) {
//		scratchpad[i] = ow_read_byte();
//	}
//	ow_reset();				//nothing more to read, stop
//	return check_scratch_crc();
//}
//
//uint8_t get_temp_deg() {
//	return (scratchpad[1] << 4) | (scratchpad[0] >> 4);
//}

uint16_t get_temp_millis() {
	uint16_t erg = 0;
	if (scratchpad[0] & 0b1000) {
		erg += 5000;
	}
	if (scratchpad[0] & 0b100) {
		erg += 2500;
	}
	if (scratchpad[0] & 0b10) {
		erg += 1250;
	}
	if (scratchpad[0] & 0b1) {
	erg += 625;
	}
	return erg;
}

uint16_t temp_to_millis(uint8_t value) {
	uint16_t erg = 0;
	if (value & 0b1000) {
		erg += 5000;
	}
	if (value & 0b100) {
		erg += 2500;
	}
	if (value & 0b10) {
		erg += 1250;
	}
	if (value & 0b1) {
	erg += 625;
	}
	return erg;
}

void ow_read_single_id(uint8_t* id) {
	ow_reset();				// reset
	ow_write(0x33);			// READ ROM [33h]
	for (uint8_t i = 0; i < 8; i++) {
		id[i] = ow_read_byte();
	}
	ow_reset();
}













//
//
//
//
//
////-----------------------------------------
//// Read temperature
////-----------------------------------------
//float  ds1820_read_temp(uint8_t used_pin) {
//	uint8_t error,i;
//	uint16_t j=0;
//	uint8_t scratchpad[9];
//	float temp=0;
//	scratchpad[0]=0;
//	scratchpad[1]=0;
//	scratchpad[2]=0;
//	scratchpad[3]=0;
//	scratchpad[4]=0;
//	scratchpad[5]=0;
//	scratchpad[6]=0;
//	scratchpad[7]=0;
//	scratchpad[8]=0;
//	error=ds1820_reset(used_pin);									//1. Reset
//	if (error==0){
//		ds1820_wr_byte(0xCC,used_pin);  							//2. skip ROM
//		ds1820_wr_byte(0x44,used_pin);  							//3. ask for temperature conversion
//		while (ds1820_re_byte(used_pin)==0xFF && j<1000){			//4. wait until conversion is finished
//			_delay_us(1);
//			j++;
//		}
//		error=ds1820_reset(used_pin);								//5. Reset
//		ds1820_wr_byte(0xCC,used_pin);  							//6. skip ROM
//		ds1820_wr_byte(0xBE,used_pin);  							//7. Read entire scratchpad 9 bytes
//
//		for (i=0; i<9; i++)     									//8. Get scratchpad byte by byte
//		{
//			scratchpad[i]=ds1820_re_byte(used_pin); 					//9. read one DS18S20 byte
//		}
//	}
//	if(scratchpad[1]==0x00 && scratchpad[7]!=0){					//Value pos.
//		scratchpad[0]=scratchpad[0] >> 1;
//		temp=(scratchpad[0]-0.25f+(((float)scratchpad[7]-(float)scratchpad[6])/(float)scratchpad[7]));
//		temp = (floor(temp*10.0+0.5)/10);							//Round value .x
//
//	}
//	if(scratchpad[1]!=0x00){										//Value negative
//		uint8_t tmp;
//		tmp =scratchpad[0];											//Save Kommabit
//		tmp= ~ tmp;
//		tmp= tmp >> 1;
//		temp = (-1)*(tmp+1);
//		if ((scratchpad[0]&0b00000001)==1){
//			temp=temp+0.5;
//		}
//
//	}
//
//	return temp;
//}
//
//
//
//
//uchar w1_rom_search( uchar diff, uchar idata *id ) {
//  uchar i, j, next_diff;
//  bit b;
//
//  if( w1_reset() )
//    return PRESENCE_ERR;			// error, no device found
//  w1_byte_wr( SEARCH_ROM );			// ROM search command
//  next_diff = LAST_DEVICE;			// unchanged on last device
//  i = 8 * 8;					// 8 bytes
//  do{
//    j = 8;					// 8 bits
//    do{
//      b = w1_bit_io( 1 );			// read bit
//      if( w1_bit_io( 1 ) ){			// read complement bit
//	if( b )					// 11
//	  return DATA_ERR;			// data error
//      }else{
//	if( !b ){				// 00 = 2 devices
//	  if( diff > i ||
//	    ((*id & 1) && diff != i) ){
//	    b = 1;				// now 1
//	    next_diff = i;			// next pass 0
//	  }
//	}
//      }
//      w1_bit_io( b );     			// write bit
//      *id >>= 1;
//      if( b )					// store bit
//	*id |= 0x80;
//      i--;
//    }while( --j );
//    id++;					// next byte
//  }while( i );
//  return next_diff;				// to continue search
//}





































//// Perform a search. If this function returns a '1' then it has
//// enumerated the next device and you may retrieve the ROM from the
//// OneWire::address variable. If there are no devices, no further
//// devices, or something horrible happens in the middle of the
//// enumeration then a 0 is returned.  If a new device is found then
//// its address is copied to newAddr.  Use OneWire::reset_search() to
//// start over.
////
//// --- Replaced by the one from the Dallas Semiconductor web site ---
////--------------------------------------------------------------------------
//// Perform the 1-Wire Search Algorithm on the 1-Wire bus using the existing
//// search state.
//// Return TRUE  : device found, ROM number in ROM_NO buffer
////        FALSE : device not found, end of search
////
//uint8_t OneWire::search(uint8_t *newAddr, bool search_mode /* = true */)
//{
//   uint8_t id_bit_number;
//   uint8_t last_zero, rom_byte_number, search_result;
//   uint8_t id_bit, cmp_id_bit;
//
//   unsigned char rom_byte_mask, search_direction;
//
//   // initialize for search
//   id_bit_number = 1;
//   last_zero = 0;
//   rom_byte_number = 0;
//   rom_byte_mask = 1;
//   search_result = 0;
//
//   // if the last call was not the last one
//   if (!LastDeviceFlag)
//   {
//      // 1-Wire reset
//      if (!reset())
//      {
//         // reset the search
//         LastDiscrepancy = 0;
//         LastDeviceFlag = FALSE;
//         LastFamilyDiscrepancy = 0;
//         return FALSE;
//      }
//
//      // issue the search command
//      if (search_mode == true) {
//        write(0xF0);   // NORMAL SEARCH
//      } else {
//        write(0xEC);   // CONDITIONAL SEARCH
//      }
//
//      // loop to do the search
//      do
//      {
//         // read a bit and its complement
//         id_bit = read_bit();
//         cmp_id_bit = read_bit();
//
//         // check for no devices on 1-wire
//         if ((id_bit == 1) && (cmp_id_bit == 1))
//            break;
//         else
//         {
//            // all devices coupled have 0 or 1
//            if (id_bit != cmp_id_bit)
//               search_direction = id_bit;  // bit write value for search
//            else
//            {
//               // if this discrepancy if before the Last Discrepancy
//               // on a previous next then pick the same as last time
//               if (id_bit_number < LastDiscrepancy)
//                  search_direction = ((ROM_NO[rom_byte_number] & rom_byte_mask) > 0);
//               else
//                  // if equal to last pick 1, if not then pick 0
//                  search_direction = (id_bit_number == LastDiscrepancy);
//
//               // if 0 was picked then record its position in LastZero
//               if (search_direction == 0)
//               {
//                  last_zero = id_bit_number;
//
//                  // check for Last discrepancy in family
//                  if (last_zero < 9)
//                     LastFamilyDiscrepancy = last_zero;
//               }
//            }
//
//            // set or clear the bit in the ROM byte rom_byte_number
//            // with mask rom_byte_mask
//            if (search_direction == 1)
//              ROM_NO[rom_byte_number] |= rom_byte_mask;
//            else
//              ROM_NO[rom_byte_number] &= ~rom_byte_mask;
//
//            // serial number search direction write bit
//            write_bit(search_direction);
//
//            // increment the byte counter id_bit_number
//            // and shift the mask rom_byte_mask
//            id_bit_number++;
//            rom_byte_mask <<= 1;
//
//            // if the mask is 0 then go to new SerialNum byte rom_byte_number and reset mask
//            if (rom_byte_mask == 0)
//            {
//                rom_byte_number++;
//                rom_byte_mask = 1;
//            }
//         }
//      }
//      while(rom_byte_number < 8);  // loop until through all ROM bytes 0-7
//
//      // if the search was successful then
//      if (!(id_bit_number < 65))
//      {
//         // search successful so set LastDiscrepancy,LastDeviceFlag,search_result
//         LastDiscrepancy = last_zero;
//
//         // check for last device
//         if (LastDiscrepancy == 0)
//            LastDeviceFlag = TRUE;
//
//         search_result = TRUE;
//      }
//   }
//
//   // if no device found then reset counters so next 'search' will be like a first
//   if (!search_result || !ROM_NO[0])
//   {
//      LastDiscrepancy = 0;
//      LastDeviceFlag = FALSE;
//      LastFamilyDiscrepancy = 0;
//      search_result = FALSE;
//   } else {
//      for (int i = 0; i < 8; i++) newAddr[i] = ROM_NO[i];
//   }
//   return search_result;
//  }
