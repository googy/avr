/*
 * timebase.c
 *
 *  Created on: 24.01.2020
 *      Author: googy
 */

#include <avr/io.h>
#include <avr/interrupt.h>

//-----------------------------timebase----------------------------------------------------------------
volatile uint8_t time1 = 0;
volatile uint32_t ticks = 0;
volatile uint16_t minute_counter = 0;
volatile uint16_t minutes = 0;
// 8000000Hz / 1024 / 256 = 30,5Hz 30,517578125
ISR(TIMER2_OVF_vect) {
    time1++;             // tick 32,768ms, wrap around 8,388608s
    ticks++;             //
    minute_counter++;
    if (minute_counter > 1831) {
        minutes++;
        minute_counter = 0;
    }
}

void init_tick_timer() {
    TCCR2A = 0;     // normal mode, no output compare output
    TCCR2B = (1 << CS22) | (1 << CS21) | (1 << CS20);   // 1024 prescaler
    TIMSK2 = 1 << TOIE2;                                // enable Timer2 Overflow Interrupt
}

// speichere aktuelle Zeit in Ticks im Counter
void timer_init(uint32_t* counter) {
    cli();
    *counter = ticks;
    sei();
}

// wenn die aktuelle Zeit den gespeicherten Startwert + offset �bersteigt return true
uint8_t timer_expired(uint32_t* counter, uint16_t ms_time) {
    uint8_t ergebnis = 0;
    cli();
    if (ticks > (*counter + (ms_time >> 5))) ergebnis = 1;
    sei();
    return ergebnis;
}
//-----------------------------------------------------------------------------------------------------
