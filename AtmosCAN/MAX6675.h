/*
 * MAX31855.h
 *
 *  Created on: 03.06.2020
 *      Author: googy
 */

#ifndef MAX6675_H_
#define MAX6675_H_

void max6675initSW();
uint16_t max6675readTempSW();

#endif /* MAX6675_H_ */
