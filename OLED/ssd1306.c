/*
 * ssd1306.c
 *
 *  Created on: 10.05.2015
 *      Author: googy_000
 */

#include "i2cmaster.h"
#include <avr/pgmspace.h>
#include "ssd1306.h"


void sendCommand(uint8_t command) {
	i2c_start(DevSSD1306);
	i2c_write(0x00);
	i2c_write(command);
	i2c_stop();
}

void init_ssd1306() {
	i2c_init();

	i2c_start(DevSSD1306 + I2C_WRITE);	// set device address and write mode

	// Init sequence for 128x64 OLED module
	i2c_write(0xAE);                    // Display Off

	i2c_write(0x00 | 0x0);            // low col = 0
	i2c_write(0x10 | 0x0);           // hi col = 0
	i2c_write(0x40 | 0x0);            // line #0

	i2c_write(0x81);                   // Set Contrast 0x81
	i2c_write(0xCF);
	// flips display
	i2c_write(0xA1);                    // Segremap - 0xA1
	i2c_write(0xC8);                    // COMSCAN DEC 0xC8 C0
	i2c_write(0xA6);                    // Normal Display 0xA6 (Invert A7)

	i2c_write(0xA4);                // DISPLAY ALL ON RESUME - 0xA4
	i2c_write(0xA8);                    // Set Multiplex 0xA8
	i2c_write(0x3F);                    // 1/64 Duty Cycle

	i2c_write(0xD3);                    // Set Display Offset 0xD3
	i2c_write(0x0);                     // no offset

	i2c_write(0xD5);                    // Set Display Clk Div 0xD5
	i2c_write(0x80);                    // Recommneded resistor ratio 0x80

	i2c_write(0xD9);                  // Set Precharge 0xd9
	i2c_write(0xF1);

	i2c_write(0xDA);                    // Set COM Pins0xDA
	i2c_write(0x12);

	i2c_write(0xDB);                 // Set VCOM Detect - 0xDB
	i2c_write(0x40);

	i2c_write(0x20);                    // Set Memory Addressing Mode
	i2c_write(0x00);                    // 0x00 - Horizontal

	i2c_write(0x40 | 0x0);              // Set start line at line 0 - 0x40

	i2c_write(0x8D);                    // Charge Pump -0x8D
	i2c_write(0x14);

	i2c_write(0xA4);              //--turn on all pixels - A5. Regular mode A4
	i2c_write(0xAF);                //--turn on oled panel - AF

	i2c_write(0x21);        // column address
	i2c_write(0);           // column start address (0 = reset)
	i2c_write(127);         // column end addres (127 = reset)

	i2c_write(0x22);        // page address
	i2c_write(0);           // page start address (0 = reset);
	i2c_write(7);           // page end address

	i2c_stop();
}

void clear() {
	uint8_t twbrbackup = TWBR;
	TWBR = 12;
	for (uint16_t i = 0; i < ((128 * 64) / 8); i++) {
		unsigned char ret = i2c_start(DevSSD1306 + I2C_WRITE); // set device address and write mode
		if (ret) {
			/* failed to issue start condition, possibly no device found */
			i2c_stop();
		} else {
			i2c_write(0x40);      // set display RAM display start line register
			for (uint8_t x = 0; x < 16; x++) {
				i2c_write(0x00);
				i++;
			}
			i--;
			i2c_stop();
			TWBR = twbrbackup;
		}
	}
}

void setCursor(uint8_t page, uint8_t column) {
	i2c_start(DevSSD1306);
	i2c_write(0x21);		// command to set column address
	i2c_write(column);		// set column start address (0 = reset)
	i2c_write(127);			// set column end address (127 = reset)

	i2c_write(0x22);		// command to set page address
	i2c_write(page);		// set page start address (0 = reset);
	i2c_write(7);			// set page end address
	i2c_stop();
}

void write_bytes(uint8_t* data, uint8_t bytes) {
	i2c_start(DevSSD1306);
	i2c_write(0x40);		//
	for (uint8_t packet_byte = 0; packet_byte < bytes; packet_byte++) {
		if (packet_byte % 5 == 0) {
			i2c_write(0x00);
		}
		i2c_write(pgm_read_byte(&(font[packet_byte + 495])));
	}
	i2c_stop();
}

void oled_print(char *s) {
	while (*s) { /* so lange *s != '\0' also ungleich dem "String-Endezeichen(Terminator)" */
		i2c_start(DevSSD1306);
		i2c_write(0x40);
		for (uint8_t c = 0; c < 5; c++) {
			i2c_write(pgm_read_byte(&(font[((*s - 25) * 5) + c])));
		}
		i2c_write(0x00);
		s++;
		i2c_stop();
	}
}
