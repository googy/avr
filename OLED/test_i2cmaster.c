#include <avr/io.h>
#include <util/delay.h>
#include <stdlib.h>
#include "ssd1306.h"

uint8_t t = 0;
char buf[10];

int main(void) {
	init_ssd1306();
	clear();
	for (;;) {
		itoa(t, buf, 10);
		setCursor(0, 0);
		oled_print("0001");
		_delay_ms(1000);
		t++;
	}
}
