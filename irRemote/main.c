/*
 * main.c
 *
 *  Created on: 20.03.2013
 *      Author: googy
 */
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <util/delay.h>

#define BAUD 9600UL          // Baudrate

// Berechnungen
#define UBRR_VAL ((F_CPU+BAUD*8)/(BAUD*16)-1)   // clever runden
#define BAUD_REAL (F_CPU/(16*(UBRR_VAL+1)))     // Reale Baudrate
#define BAUD_ERROR ((BAUD_REAL*1000)/BAUD)      // Fehler in Promille, 1000 = kein Fehler.

#if ((BAUD_ERROR<990) || (BAUD_ERROR>1010))
  #error Systematischer Fehler der Baudrate gr�sser 1% und damit zu hoch!
#endif

volatile uint8_t i = 0;
volatile uint8_t xh = 0;
volatile uint8_t xl = 0;
volatile uint8_t l[60];
volatile uint8_t h[60];

void uart_init(void)
{
	UBRRH = UBRR_VAL >> 8;
	UBRRL = UBRR_VAL & 0xFF;		// set Baudrate
	UCSRB |= (1<<TXEN);				// enable transmitter
	// Frame Format: Asynchron 8N1
	UCSRC = (1<<UCSZ1)|(1<<UCSZ0);
}

int uart_putc(unsigned char c)
{
	while (!(UCSRA & (1<<UDRE))) {}		/* warten bis Senden moeglich */
    UDR = c;                      /* sende Zeichen */
    return 0;
}

void uart_puts (char *s) {
    while (*s) {	/* so lange *s != '\0' also ungleich dem "String-Endezeichen(Terminator)" */
        uart_putc(*s);
        s++;
    }
}

void uart_put_int(uint8_t i) {
	char s[4];
	uart_puts(itoa( i, s, 10 ));	// 10 fuer radix -> Dezimalsystem
}

void int0_init() {	// INT0 als pin change interrupt
	MCUCR |= (1<<ISC00);	// interrupt on any state change on INT0
	GIMSK |= (1<<INT0);		// enable INT0
}

int main() {
	DDRD |= (1 << PD5);
	DDRD &= ~(1<<PD2);

	uart_init();
	int0_init();

	// setup Timer
	TCCR0B = (1<<CS02);			// clock to timer without prescaler
	TIMSK = (1<<TOIE0);			// enable overflow interrupt

	sei();

	while (1) {
		uart_put_int(xl);
		uart_putc('_');
		uart_put_int(xh);
		uart_putc(10);
		uart_putc(13);
		if (xl > 50 || xh > 50) {
			cli();
			cli();
			for (uint8_t j = 0; j < 60; j++) {
				uart_putc('h');
				uart_put_int(h[j]);
				uart_putc(10);
				uart_putc(13);

				uart_putc('l');
				uart_put_int(l[j]);
				uart_putc(10);
				uart_putc(13);
			}
			xl = 0;
			xh = 0;
			uart_putc(10);
			uart_putc(10);
			uart_putc('x');
			uart_putc(10);
			uart_putc(13);
			cli();
		}

		_delay_ms(10000);
//		uart_putc('l');
//		uart_put_int(i);
//		uart_putc(10);
//		uart_putc(13);
//		if (PIND & (1 << PD2)) {
//			PORTD &= ~(1 << PD5);
//		} else {
//			PORTD |= (1 << PD5);
//		}
	}
}

ISR(INT0_vect) {
	if (PIND & (1 << PD2)) {
		h[xh] = i;
		xh++;
		i = 0;
	} else {
		l[xl] = i;
		xl++;
		i = 0;
	}
}

ISR(TIMER0_OVF_vect) {
	i++;
}
