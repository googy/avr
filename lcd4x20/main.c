//
// Anpassungen im makefile:
//    ATMega8 => MCU=atmega8 im makefile einstellen
//    lcd-routines.c in SRC = ... Zeile anh�ngen
//
#include <avr/io.h>
#include "lcd-routines.h"

int main(void)
{
	DDRD |= (1 << PD5);
	PORTD &= ~(1 << PD5);

  // Initialisierung des LCD
  // Nach der Initialisierung m�ssen auf dem LCD vorhandene schwarze Balken
  // verschwunden sein
  lcd_init();

  // Text in einzelnen Zeichen ausgeben
//  lcd_data( '' );

  // Die Ausgabemarke in die 2te Zeile setzen
  lcd_setcursor( 0, 1 );

  // erneut Text ausgeben, aber diesmal komfortabler als String
  lcd_string("Zeile 1");

  lcd_setcursor( 0, 2 );
  lcd_string("Zeile 2");
  lcd_setcursor( 0, 3 );
  lcd_string("Zeile 3");
  lcd_setcursor( 0, 4 );
  lcd_string("Zeile 4");

  while(1)
  {
  }

  return 0;
}
